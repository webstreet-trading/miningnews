<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   SM_Infusionsoft
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

global $wpdb;

if ( is_multisite() ) {

	$blogs = $wpdb->get_results( "SELECT blog_id FROM {$wpdb->blogs}", ARRAY_A );
		delete_option('sm_infusionsoft_settings');
	if ( $blogs ) {

	 	foreach ( $blogs as $blog ) {
			switch_to_blog( $blog['blog_id'] );
			delete_option('sm_infusionsoft_settings');
			restore_current_blog();
		}
	}

} else {
	delete_option('sm_infusionsoft_settings');
}