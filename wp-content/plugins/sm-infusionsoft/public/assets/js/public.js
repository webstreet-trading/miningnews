(function ( $ ) {
	"use strict";

	$(function () {
	
		$(document).ready(function() {

			$('form.infusionsoft-subscribe-form').submit(function() {
				$('form.infusionsoft-subscribe-form .error').remove();
				var hasError = false;
				$('.requiredField').each(function() {
					if($.trim($(this).val()) === '') {
						var labelText = $(this).prev('label').text();
						$(this).parent().append('<span class="error">' + objectL10n.error_empty + ' ' +labelText+'.</span>');
						$(this).addClass('inputError');
						hasError = true;
					}
					else if($(this).hasClass('email')) {
						var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
						if(!emailReg.test($.trim($(this).val()))) {
							var labelText = $(this).prev('label').text();
							$(this).parent().append('<span class="error">' + objectL10n.error_invalid + ' '+labelText+'.</span>');
							$(this).addClass('inputError');
							hasError = true;
						}
					}
				});

				if(!hasError) {
					var formInput = $(this).serialize();
					$('form.infusionsoft-subscribe-form').slideUp("fast", function(){
						$(this).before('<p class="loading">' + objectL10n.submitting + '</p>');
					});
					$.post(objectL10n.ajaxurl, formInput, function(result){
						$('.sm-infusionsoft .loading').remove();
						$("form.infusionsoft-subscribe-form").find('.info').hide();
						$("form.infusionsoft-subscribe-form").find('.alert').hide();
						$("form.infusionsoft-subscribe-form").slideDown("fast", function() {
							if (result === 'success') {
								// reset form
								subscribe_reset();
								$(this).find('.info').show();
							}
							else {
								$(this).find('.alert').show();
							}
						});
					});
				}

				return false;

			});

			function subscribe_reset() {
				$(".sm-infusionsoft .loading").remove();
				$(".sm-infusionsoft .tick").remove();
				$(".sm-infusionsoft form.infusionsoft-subscribe-form").show();
				$(".sm-infusionsoft form.infusionsoft-subscribe-form").find("input[type=text], textarea").val('');
			}

		});

	});

}(jQuery));