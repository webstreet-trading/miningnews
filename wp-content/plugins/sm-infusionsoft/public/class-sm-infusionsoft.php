<?php
/**
 * 3SMedia Infusionsoft.
 *
 * @package   SM_Infusionsoft
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

/**
 * Plugin class. This class should ideally be used to work with the
 * public-facing side of the WordPress site.
 *
 * If you're interested in introducing administrative or dashboard
 * functionality, then refer to `class-sm-infusionsoft-admin.php`
 *
 * @package SM_Infusionsoft
 * @author  Charles Coleman <echcoleman@gmail.com>
 */
class SM_Infusionsoft {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	const VERSION = '1.0.0';

	/**
	 * Unique identifier for your plugin.
	 *
	 *
	 * The variable name is used as the text domain when internationalizing strings
	 * of text. Its value should match the Text Domain file header in the main
	 * plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'sm-infusionsoft';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;
    
    /**
	 * Api instance
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $api = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
		
		// shortcode
		add_shortcode( 'infusionsoft_signup', array( $this, 'shortcode' ) );
		
		// handle ajax enquire forms
		add_action( 'wp_ajax_nopriv_sm_infusionsoft', array( $this, 'ajax_signup' ) );
		add_action( 'wp_ajax_sm_infusionsoft', array( $this, 'ajax_signup' ) );

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );

	}
    
    /**
	 * Setup api
	 *
	 * @since    1.0.0
	 */
    protected function setup_api() {
        
        // require api
        require_once ( plugin_dir_path( dirname( __FILE__ ) ) . '/includes/infusionsoft-sdk/isdk.php' );

        // get settings
        $settings = get_option( 'sm_infusionsoft_settings' );
        
        // setup infusionsoft api
        $this->api = new iSDK();
        $this->api->cfgCon( $settings['infu_service_url'], $settings['infu_service_key'] );
    }

	/**
	 * Return the plugin slug.
	 *
	 * @since    1.0.0
	 *
	 * @return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, basename( plugin_dir_path( dirname( __FILE__ ) ) ) . '/languages/' );

	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', plugins_url( 'assets/css/public.css', __FILE__ ), array(), self::VERSION );
	}

	/**
	 * Register public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function register_scripts() {
		wp_register_script( $this->plugin_slug . '-plugin-script', plugins_url( 'assets/js/public.js', __FILE__ ), array( 'jquery' ), self::VERSION );
	}
	
	/**
	 * Ajax signup
	 * 
	 * @since     1.0.0
	 */
	public function ajax_signup() {
		if ( $this->process_signup( $_POST ) ) {
			echo 'success';
		}
		else {
			echo 'error';
		}
		exit;
	}
	
	/**
	 * Process submitted enquiry
	 * 
	 * @param array $form_data Enquiry data
	 * @return boolean Return true on success, false on failure
	 * 
	 * @since     1.0.0
	 */
	public function process_signup( $form_data ) {
	
		//Check to see if the honeypot captcha field was filled in
		if( trim( $form_data['fieldcheck'] ) !== '' ) {
			return false;
		}
		else {

			//Check to make sure that the name field is not empty
			if( trim( $form_data['name'] ) === '' || trim( $form_data['lastname'] ) === '' ) {
				// disable for now
				//return false;
			}
			else {
				$name = trim( $form_data['name'] );
				$lastname = trim( $form_data['lastname'] );
			}

			//Check to make sure sure that a valid email address is submitted
			if( trim($form_data['email']) === '' )  {
				return false;
			}
			else if ( !eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($form_data['email'])) ) {
				return false;
			}
			else {
				$email = trim( $form_data['email'] );
			}
            
            // get settings
            $settings = get_option( 'sm_infusionsoft_settings' );
            
             $tags = array();
            // if only one tag available, always set as checked
            if ( count( $settings['tag_list'] ) === 1 ) {
                $tag = current( $settings['tag_list'] );
                $tags[] = $tag['tag_id'];
            }
            else {
                // check tags
                foreach ( $form_data['tag_opt'] as $tag_pos ) {
                    if ( isset( $settings['tag_list'][$tag_pos] ) ) {
                        $tags[] = $settings['tag_list'][$tag_pos]['tag_id'];
                    }
                }
            }
            if ( empty( $tags ) ) {
                return false;
            }
            
            $this->setup_api();
			
			// contact data
			$contact = array(
                'FirstName' => $name,
                'LastName' => $lastname,
                'Email' => $email,
            );

            // check if contact exits
            $returnFields = array( 'Id', 'FirstName', 'LastName' );
            $data = $this->api->findByEmail( $email, $returnFields );
            if ( isset( $data[0] ) && !empty( $data ) ) {
                $contact_id = $data[0]['Id'];
            }
            else {
                // if not exists, add contact
                $contact_id = $this->api->addCon( $contact );
            }
            
            // apply tag(s)
            $result = true;
            foreach ( $tags as $tag_id ) {
                if ( !$this->process_tag( $contact_id, $tag_id ) ) {
                    $result = false;
                }
            }
            return $result;
			
		}
		return false;
	}
    
    /**
	 * Process tag for contact
	 * 
	 * @param int $contact_id
     * @param int $tag_id
	 * @return boolean True on success and false on failure
	 * 
	 * @since     1.0.0
	 */
    protected function process_tag( $contact_id, $tag_id ) {
        
        // check if tag already assigned to contact
        $query = array(
            'Contact.Id' => $contact_id,
            'GroupId' => $tag_id,
        );
        $matched = $this->api->dsCount( 'ContactGroupAssign', $query );
        if ( $matched ) {
            return true;
        }
        else {

            // add tag to contact
            $result = $this->api->grpAssign( $contact_id, $tag_id );
            if ( $result ) {
                return true;
            }
            else {
                return false;
            }
        }
        
        return true;
    }
	
	/**
	 * Process submitted enquiry
	 * 
	 * @param array $atts
	 * @return string Shortcode HTML
	 * 
	 * @since     1.0.0
	 */
	public function shortcode( $atts = array() ) {
		
		wp_enqueue_script( $this->plugin_slug . '-plugin-script' );
		wp_localize_script( $this->plugin_slug . '-plugin-script', 'objectL10n', array(
			'error_empty' => __( 'You forgot to enter your', $this->plugin_slug ),
			'error_invalid' => __( 'You entered an invalid', $this->plugin_slug ),
			'submitting' => __( 'Submitting...', $this->plugin_slug ),
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			
		) );
		
		ob_start();
		
		include ( $this->_get_template( 'shortcode' ) );
		
		$content = ob_get_clean();
		
		return $content;
	}

	/**
	 * Locate template file in appropriate hierarchy: 1) child theme, 2) parent template,
	 * 3) plugin resources.
	 * 
	 * @param string $template Template name
	 * @return string Template path
	 */
	protected function _get_template( $template ) {
		
		// whether or not .php was added
		$template_slug = preg_replace( '/.php$/', '', $template );
		$template = $template_slug . '.php';

		$theme_file = locate_template( array( $this->plugin_slug . '/' . $template ) );
		if ( $theme_file ) {
			$file = $theme_file;
		}
		else {
			$file = 'views/' . $template;
		}		
		
		return apply_filters( $this->plugin_slug . '_template_' . $template, $file );
		
	}
}
