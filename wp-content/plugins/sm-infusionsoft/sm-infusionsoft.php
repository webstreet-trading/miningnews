<?php
/**
 * 3SMedia Infusionsoft Signup Forms
 *
 * Sidebar widget to allow users to signup directly into Infusionsoft Newsletter list
 *
 * @package   SM_Infusionsoft
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 *
 * @wordpress-plugin
 * Plugin Name:       3SMedia Infusionsoft Signup Forms
 * Plugin URI:        http://www.3smedia.co.za/
 * Description:       Sidebar widget to allow users to signup directly into Infusionsoft Newsletter list
 * Version:           1.0.0
 * Author:            Charles Coleman
 * Text Domain:       sm-infusionsoft
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class-sm-infusionsoft.php' );

add_action( 'plugins_loaded', array( 'SM_Infusionsoft', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Widgets
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-sm-infusionsoft-signup.php' );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-sm-infusionsoft-admin.php' );
	add_action( 'plugins_loaded', array( 'SM_Infusionsoft_Admin', 'get_instance' ) );

}

if (!function_exists('pr')) {
	
/**
 * Helper function to output data
 * 
 * @param mixed $var Variable to output
 */
function pr($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

}

if (!function_exists('prd')) {

/**
 * Helper function to output data and exit
 * 
 * @param mixed $var Variable to output
 */
function prd($var) {
    pr($var);
    exit;
}

}