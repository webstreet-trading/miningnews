<?php
/**
 * 3SMedia Infusionsoft
 *
 * @package   SM_Infusionsoft_Admin
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 Charles Coleman
 */

/**
 * SM_Infusionsoft_Admin_Settings class
 *
 * @package SM_Infusionsoft_Admin
 * @author  Charles Coleman <echcoleman@gmail.com>
 */
class SM_Infusionsoft_Admin_Settings {
	
	/**
	 * Saved settings.
	 *
	 * @since    1.0.0
	 *
	 * @var      array
	 */
	protected $setting;
	
	/**
	 * Construct settings
	 *
	 * @since     1.0.0
	 */
	public function __construct() {
		
		/*
		 * Call $plugin_slug from public plugin class.
		 */
		$plugin = SM_Infusionsoft::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();
		
		// get saved settings options
		$this->setting = self::get_settings();
		
		// setup settings
		$this->setup_settings();
		
		// register the checkbox
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}
	
	/**
	 * Return saved settings
	 *
	 * @since     1.0.0
	 *
	 * @return    array    Settings
	 */
	public static function get_settings() {
		return get_option( 'sm_infusionsoft_settings', '' );
	}
		
	/**
	 * Setup the settings
	 * 
	 * @since     1.0.0
	 */
	public function setup_settings() {
		
		register_setting( 'sm_infusionsoft_settings', 'sm_infusionsoft_settings', array( $this, 'validate_settings' ) );
		
		// get settings
		$settings = $this->settings();
		
		// setup sections and settings
		foreach ( $settings as $section => $section_data ) {
			
			// add section
			$section = $this->plugin_slug . '_' . $section;
			add_settings_section(
				$section,
				$section_data['title'],
				array( $this, 'callback_blackhole' ),
				$this->plugin_slug
			);
			
			// add section settings
			foreach ( $section_data['settings'] as $setting ) {
				
				if ( !isset( $setting['id'] ) ) {
					continue;
				}
				$options = array_merge(array('section' => 'sm_infusionsoft_settings'), $setting);
				
				add_settings_field(
					$setting['id'],
					$setting['title'],
					array( $this, 'setting_callback' ),
					$this->plugin_slug,
					$section,
					$options
				);
			}
		}
	}
	
	/**
	 * Setup the settings
	 * 
	 * @since    1.0.0
	 * 
	 * @return   array    Settings page information
	 */
	protected function settings() {
		
		return array(
			'general' => array(
				'title' => __( 'General', $this->plugin_slug ),
				'settings' => array(
					array(
						'title' => __( 'Infusion API Service URL', $this->plugin_slug ),
						'id' => 'infu_service_url',
						'type' => 'text',
						'desc' => __( 'Infusionsoft unique URL parameter https://xxxx.infusionsoft.com', $this->plugin_slug ),
					),
					array(
						'title' => __( 'Infusion API key', $this->plugin_slug ),
						'id' => 'infu_service_key',
						'type' => 'text',
						'desc' => __( 'Infusionsoft API Key to use for the API calls', $this->plugin_slug ),
					),
					array(
						'title' => __( 'Newsletter Tag ID(s)', $this->plugin_slug ),
						'id' => 'tag_list',
						'type' => 'tag-list',
						'desc' => __( 'The tag ID contacts with be tagged with', $this->plugin_slug ),
					),
				)
			)
		);
	}
	
	/**
	 * Setting callback to ouput setting field
	 * 
	 * @since    1.0.0
	 * 
	 * @param    array    $field    Setting field data
	 */
	public function setting_callback( $field ) {
		
		if ( !isset( $field['type'] ) ) {
			return;
		}
		
		// field defaults
		$field_defaults = array(
			'id' => '',
			'title' => '',
			'class' => '',
			'css' => '',
			'default' => '',
		);
		$field = array_merge( $field_defaults, $field );
		
		// set description
		if ( isset( $field['desc'] ) ) {
			$description = '<span class="description">' . wp_kses_post( $field['desc'] ) . '</span>';
		}
		else {
			$description = '';
		}
		
		// set name
		$name = $field['section'] . '[' . $field['id'] . ']';
		
		// get current value
		$value = $this->get_value( $field['id'], $field['default'] );
		
		switch ( $field['type'] ) {
			case 'text':
				echo "<input name='{$name}' id='{$field['id']}' type='text' value='{$value}' class='{$field['class']}' style='{$field['css']}' />";
				echo $description;
				
				break;

			case 'textarea':
				$defaults = array(
					'cols' => 40,
					'rows' => 6,
				);
				$field = array_merge($defaults, $field);
				echo "<textarea name='{$name}' id='{$field['id']}' cols='{$field['cols']}' rows='{$field['rows']}' class='{$field['class']}' style='{$field['css']}'>{$value}</textarea>";
				echo $description;
				
				break;
			
			case 'select':
			case 'multiselect':
				
				?>

				<select
					name="<?php echo $name; ?>" id="<?php echo $field['id']; ?>"
					class="<?php echo $field['class']; ?>" style="<?php echo $field['css']; ?>"
					<?php if ( $field['type'] == 'multiselect' ) echo 'multiple="multiple"'; ?>
				>
					<?php
					foreach ( $field['options'] as $key => $val ) {
						if ( is_array( $value ) ) {
							$selected = selected( in_array( key, $value ), $key, false );
						}
						else {
							$selected = selected( $value, $key, false );
						}
						?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php echo $selected; ?>><?php echo $val ?></option>
						<?php
					}
					?>
					
				</select>
				<?php echo $description; ?>

				<?php
				
				break;
                
            case 'tag-list':
                
                ?>
                <div id="tag-list">
                    
                    <?php
                    // create input template
                    ob_start(); ?>
                    <div class="tag-item">
                        <input name="<?php echo $name; ?>[__pos__][tag_id]" type="text" value="__tag_id__" class="tag-id" placeholder="Tag Id" />
                        <input name="<?php echo $name; ?>[__pos__][label]" type="text" value="__label__" class="tag-label" placeholder="Label" />
                    </div>
                    <?php
                    $template = str_replace( "\r", '', str_replace( "\n", '', ob_get_clean() ) );
                    
                    // input inputs
                    $counter = 1;
                    if ( !empty( $value ) ) {
                        foreach ( $value as $tag_item ) {
                            $output = str_replace( '__pos__', $counter, $template );
                            $output = str_replace( '__tag_id__', $tag_item['tag_id'], $output );
                            echo str_replace( '__label__', $tag_item['label'], $output );
                            $counter++;
                        }
                    }
                    else {
                        $output = str_replace( '__pos__', $counter, $template );
                        $output = str_replace( '__tag_id__', '', $output );
                        echo str_replace( '__label__', '', $output );
                    }
                    ?>
                    
                </div>

                <p>
                    <p class="description"><?php _e( 'Input format: Tag Id => Label', $this->plugin_slug ); ?></p>
                    <button id="sm-tag-item-add" type="button" id="sm-tag-item-add" class="button"><?php _e( 'Add Tag', $this->plugin_slug ); ?></button>
                </p>
                <input type="hidden" name="sm-tag-item-template" id="sm-tag-item-template" value='<?php echo addslashes( $template ) ?>' />
                <input type="hidden" name="sm-tag-list-count" id="sm-tag-list-count" value="<?php echo $counter ?>" />

                <?php
                
                break;
		}
		
	}
	
	/**
	 * Validate Settings
	 *
	 * @since    1.0.0
	 *
	 * @param    string   $setting    Setting field id
	 * @param    string   $default    Default value for field
	 * 
	 * @return   string   Setting value
	 */
	public function get_value( $setting, $default = '' ) {
		
		// get setting if exists
		if ( isset( $this->setting[$setting] ) ) {
			return $this->setting[$setting];
		}
		else {
			return $default;
		}
	}
	
	/**
	 * Validate Settings
	 *
	 * @since    1.0.0
	 *
	 * @param    array    $input    Settings input
	 * 
	 * @return   boolean  Return early if no settings page is registered.
	 */
	public function validate_settings( $input ) {
        
        // validate tag list
        $settings = $this->settings();
        foreach ( $settings as $section => $section_data ) {
            foreach ( $section_data['settings'] as $setting ) {
                if ( $setting['type'] == 'tag-list' && isset( $input[ $setting['id'] ]) ) {
                    foreach ( $input[ $setting['id'] ] as $k => $values ) {
                        if ( !trim( $values['tag_id'] ) ) {
                            unset( $input[ $setting['id'] ][ $k ] );
                        }
                    }
                }
            }
        }
		
		// no checking on settings
		return $input;
	}
	
	/**
	 * Callback with no output
	 *
	 * @since    1.0.0
	 */
	public function callback_blackhole() {}
}