(function ( $ ) {
	"use strict";

	$(function () {
		
		$().ready(function(){
			
			// add tag list item
			$("#sm-tag-item-add").on( 'click', function(){
				
				// create element from template
				var tag_item = $( $("#sm-tag-item-template").val().replace(/\\/g, '') );
                
                // get new count
                var count = parseInt( $("#sm-tag-list-count").val(), 10 ) + 1;
				
                // replace value placeholders
                var html = tag_item.html().replace( /<[^<>]+>/g, function( html ) {
                    html = html.replace( /__pos__|%i%/g, count );
                    html = html.replace( /__tag_id__|%i%/g, '' );
                    html = html.replace( /__label__|%i%/g, '' );
					return html;
				} );
                tag_item.html(html);
				
				// add tag_item
				$("#tag-list").append(tag_item);
                
                // update count
                $("#sm-tag-list-count").val(count)
			});
			
		});

	});

}(jQuery));