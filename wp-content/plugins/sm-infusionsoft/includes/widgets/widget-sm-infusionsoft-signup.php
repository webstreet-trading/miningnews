<?php
/**
 * 3SMedia Infusionsoft signup widget
 */
class SM_Widget_Infusionsoft_Signup extends WP_Widget {

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {
		
		// Call $plugin_slug from public plugin class.
		$plugin = SM_Infusionsoft::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// setup widget
		parent::__construct(
			'sm-infusionsoft-signup',
			__( 'Infusionsoft Newsletter Signup', $this->plugin_slug ),
			array(
				'classname'		=>	'widget-sm-infusionsoft-signup',
				'description'	=>	__( 'Sidebar widget signup form for Infusionsoft Newsletter list.', $this->plugin_slug )
			)
		);

	} // end constructor

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param	array	args		The array of form elements
	 * @param	array	instance	The current instance of the widget
	 */
	public function widget( $args, $instance ) {
		
		wp_enqueue_script( $this->plugin_slug . '-plugin-script' );
		wp_localize_script( $this->plugin_slug . '-plugin-script', 'objectL10n', array(
			'error_empty' => __( 'You forgot to enter your', $this->plugin_slug ),
			'error_invalid' => __( 'You entered an invalid', $this->plugin_slug ),
			'submitting' => __( 'Submitting...', $this->plugin_slug ),
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			
		) );
		
		extract( $args, EXTR_SKIP );
		
		// set title
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		if ( !$title ) {
			$title = __( 'Subscribe', $this->plugin_slug );
		}
		
		echo $before_widget;
		
		echo $before_title;
		echo $title;
		echo $after_title;

		include ( 'views/infusionsoft-signup.php' );
		
		echo $after_widget;

	} // end widget

	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param	array	new_instance	The new instance of values to be generated via the update.
	 * @param	array	old_instance	The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param	array	instance	The array of keys and values for the widget.
	 */
	public function form( $instance ) {

    	// default widget values
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => __( 'Subscribe', $this->plugin_slug )
			)
		);
		
		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" />
        </p>
		<?php

	} // end form

} // end class

add_action( 'widgets_init', create_function( '', 'register_widget("SM_Widget_Infusionsoft_Signup");' ) );
