<?php
/**
 * 3SMedia Infusionsoft signup form
 *
 * @package   SM_Infusionsoft
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

$emailSent = false;
$hasError = false;

//If the form is submitted - fallback to handle non-ajax requests
if( isset( $_POST['submitted'] ) && isset( $_POST['action'] ) && $_POST['action'] == 'sm_infusionsoft' ) {

	$infusionsoft = SM_Infusionsoft::get_instance();
	if ($infusionsoft->process_signup($_POST)) {
		$emailSent = true;
	}
	else {
		$hasError = true;
	}

}

// get settings
$settings = get_option( 'sm_infusionsoft_settings' );

?>

<div class="sm-infusionsoft et_pb_contact_form_container">
	<div class="et_pb_contact">
		<form action="<?php bloginfo('url'); ?>" class="infusionsoft-subscribe-form et_pb_contact_form clearfix" class="email-form" method="post">

			<div class="et-pb-contact-message">
				<p class="info" style="display: <?php echo ($emailSent)? 'block;':'none'; ?>"><?php _e( 'You have been successfully subscribed.', $this->plugin_slug ); ?></p>
				<p class="alert" style="display: <?php echo ($hasError)? 'block;':'none'; ?>"><?php _e( 'There was an error(s) submitting the form. Please ensure all fields and validation are filled in correctly', $this->plugin_slug ); ?></p>
			</div>
			<input type="hidden" name="action" value="sm_infusionsoft" />

			<p class="et_pb_contact_field et_pb_contact_field_last">
				<label class="et_pb_contact_form_label" for="infusionsoft-name"><?php _e('First Name', $this->plugin_slug); ?></label>
				<input type="text" name="name" id="infusionsoft-name" placeholder="<?php _e('First Name', $this->plugin_slug); ?>" value="<?php if(isset($_POST['name'])) echo $_POST['name'];?>" class="input txt requiredField" />
			</p>

			<p class="et_pb_contact_field et_pb_contact_field_last">
				<label class="et_pb_contact_form_label" for="infusionsoft-lastname"><?php _e('Last Name', $this->plugin_slug); ?></label>
				<input type="text" name="lastname" id="infusionsoft-lastname" placeholder="<?php _e('Last Name', $this->plugin_slug); ?>" value="<?php if(isset($_POST['lastname'])) echo $_POST['lastname'];?>" class="input xt requiredField" />
			</p>

			<p class="et_pb_contact_field et_pb_contact_field_last">
				<label class="et_pb_contact_form_label" for="infusionsoft-email"><?php _e('Email', $this->plugin_slug); ?></label>
				<input type="text" name="email" id="infusionsoft-email" placeholder="<?php _e('Email', $this->plugin_slug); ?>" value="<?php if(isset($_POST['email']))  echo $_POST['email'];?>" class="input txt requiredField email" />
			</p>

			<?php if ( count( $settings['tag_list'] ) > 1 ): ?>
			<p class="infusionsoft-tags et_pb_contact_field et_pb_contact_field_last">
				<?php foreach ( $settings['tag_list'] as $k => $tag_item ): ?>
				<input type="checkbox" checked="checked" id="tag_item_<?php echo $k; ?>" name="tag_opt[]" value="<?php echo $k; ?>">
				<label class="et_pb_contact_form_label" for="tag_item_<?php echo $k; ?>"><?php echo $tag_item['label']; ?></label>
				<?php endforeach; ?>
			</p>
			<?php endif; ?>

			<p class="fieldcheck et_pb_contact_field et_pb_contact_field_last">
				<label for="fieldcheck" class="fieldcheck et_pb_contact_form_label"><?php _e('If you want to submit this form, do not enter anything in this field', $this->plugin_slug); ?></label>
				<input type="text" name="fieldcheck" id="fieldcheck" class="fieldcheck input" placeholder="<?php _e('If you want to submit this form, do not enter anything in this field', $this->plugin_slug) ?>" value="<?php if(isset($_POST['fieldcheck'])) echo $_POST['fieldcheck'];?>" />
			</p>

			<div class="buttons et_contact_bottom_container">
				<input type="hidden" name="submitted" id="infusionsoft-submitted" value="true" />
				<input class="submit button et_pb_contact_submit et_pb_button tab_desktop_btn" type="submit" value="<?php _e('Submit', $this->plugin_slug); ?>" />
			</div>

		</form>
	</div>

</div>
