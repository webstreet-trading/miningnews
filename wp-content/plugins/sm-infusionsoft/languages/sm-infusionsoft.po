# Copyright (C) 2014 3SMedia (Pty) Ltd
# This file is distributed under the same license as the SM_Infusionsoft package.
msgid ""
msgstr ""
"Project-Id-Version: 3SMedia Infusionsoft Signup 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-06-02 13:54+0200\n"
"PO-Revision-Date: 2014-06-02 13:58+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: __;_e;_n;_x;esc_html_e;esc_html__;esc_attr_e;"
"esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2\n"
"X-Poedit-Basepath: ../\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SearchPath-0: .\n"

#: admin/class-sm-infusionsoft-admin.php:140
msgid "3SMedia Infusionsoft Settings"
msgstr ""

#: admin/class-sm-infusionsoft-admin.php:141
msgid "3SMedia Infusionsoft"
msgstr ""

#: admin/class-sm-infusionsoft-admin.php:167
msgid "Settings"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:118
msgid "General"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:121
msgid "Username"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:124
msgid "Infusionsoft username that corresponds with API Key"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:127
msgid "API Key"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:130
msgid "Infusionsoft API Key to use for the API calls"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:133
msgid "List ID"
msgstr ""

#: admin/includes/class-sm-infusionsoft-settings.php:136
msgid "The ID of the list users will be added to when they subscribe"
msgstr ""

#: admin/views/admin.php:23
msgid "Save Changes"
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:24
msgid "Infusionsoft Newsletter Signup"
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:27
msgid "Sidebar widget signup form for Infusionsoft Newsletter list."
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:47
msgid "You forgot to enter your"
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:48
msgid "You entered an invalid"
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:49
msgid "Submitting..."
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:59
#: includes/widgets/widget-sm-infusionsoft-signup.php:102
msgid "Subscribe"
msgstr ""

#: includes/widgets/widget-sm-infusionsoft-signup.php:108
msgid "Title:"
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:37 public/views/public.php:40
msgid "You have been successfully subscribed."
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:38 public/views/public.php:41
msgid ""
"There was an error(s) submitting the form. Please ensure all fields and "
"validation are filled in correctly"
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:41
msgid "First Name"
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:44
msgid "Last Name"
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:48 public/views/public.php:48
msgid "Email"
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:51 public/views/public.php:55
msgid "If you want to submit this form, do not enter anything in this field"
msgstr ""

#: includes/widgets/views/infusionsoft-signup.php:52 public/views/public.php:56
msgid "Submit"
msgstr ""

#: public/views/public.php:44
msgid "Name"
msgstr ""

#: public/views/public.php:52
msgid "Message"
msgstr ""
