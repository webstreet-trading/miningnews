<?php
/**
 * 3SMedia
 *
 * 3SMedia related functionality including: Metal of the Month, Top Contributors and Magazine Widget
 *
 * @package   SMedia
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 *
 * @wordpress-plugin
 * Plugin Name:       3SMedia
 * Plugin URI:        http://www.3smedia.co.za/
 * Description:       3SMedia related functionality including: Metal of the Month, Top Contributors and Magazine Widget
 * Version:           1.0.0
 * Author:            Charles Coleman
 * Text Domain:       smedia-locale
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

/*
 * @TODO:
 *
 * - replace `class-smedia.php` with the name of the plugin's class file
 *
 */
require_once( plugin_dir_path( __FILE__ ) . 'public/class-smedia.php' );

/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 *
 * @TODO:
 *
 * - replace SMedia with the name of the class defined in
 *   `class-smedia.php`
 */
register_activation_hook( __FILE__, array( 'SMedia', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'SMedia', 'deactivate' ) );

/*
 * @TODO:
 *
 * - replace SMedia with the name of the class defined in
 *   `class-smedia.php`
 */
add_action( 'plugins_loaded', array( 'SMedia', 'get_instance' ) );

require_once( plugin_dir_path( __FILE__ ) . 'includes/class-sm-top-contributors.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/class-sm-takeover.php' );
add_action( 'plugins_loaded', array( 'SM_Top_Contributors', 'get_instance' ) );

// widgets
require_once( plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-sm-top-contributors.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-sm-magazines.php' );
require_once( plugin_dir_path( __FILE__ ) . 'includes/widgets/widget-sm-totm.php' );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * @TODO:
 *
 * - replace `class-smedia-admin.php` with the name of the plugin's admin file
 * - replace SMedia_Admin with the name of the class defined in
 *   `class-smedia-admin.php`
 *
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-smedia-admin.php' );
	add_action( 'plugins_loaded', array( 'SMedia_Admin', 'get_instance' ) );

}
