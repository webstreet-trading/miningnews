<?php
/**
 * Page Takeover
 */
class SM_Takeover {
    
    /**
	 * Initialize
	 */
	public function __construct() {
        
        add_action( 'plugins_loaded', array( $this, 'hooks' ), 1 );
		
		return $this;
	}
    
    /**
	 * Run hooks
	 */
    public function hooks() {
        
        // run public hook
        $this->public_hooks();
        
        // run admin hook
        if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
            $this->admin_hooks();
        }
        
    }
    
    /**
	 * Setup public hooks
	 */
	public function public_hooks() {
		
		// Define custom functionality.
		add_action( 'wp', array($this, 'load' ) );
	}
	
	/**
	 * Setup admin hooks
	 */
	public function admin_hooks() {
		
		// set metaboxes for custom meta boxes
		add_filter( 'cmb2_meta_boxes', array($this, 'set_meta_boxes') );
	}
	
	/**
	 * On page load determine if takeover should be displayed
	 */
	public function load() {
		if ( is_singular() ) {
			global $post;
			
			$this->setup_takeover( $post->ID );
		}
	}
	
	/**
	 * Setup takeover (if available) for page
	 * 
	 * @param int $post_id Page to check for takeover information
	 * @return boolean Return true if takeover has been setup, otherwise return false
	 */
	protected function setup_takeover( $post_id ) {
		
		// check if post has active takeover
		if ( get_post_meta( $post_id, 'takeover_active', true ) ) {
            add_action( 'wp_footer', array( $this, 'takeover_display' ) );
			add_action( 'wp_head',
				// custom callback for takeover page
				function() {
					$this->add_body_style();
				}
			);
		}
	}
    
    public function takeover_display() {
        global $post;
        
        if ( get_post_meta( $post->ID, 'takeover_active', true ) ) {
            
            $bgcolor = get_post_meta( $post->ID, 'takeover_bgcolor', true );
            $image_url = get_post_meta( $post->ID, 'takeover_image', true );
            $image_link = get_post_meta( $post->ID, 'takeover_link', true );
            
            $style = array();
            if ( $bgcolor ) {
                $style[] = "background-color: {$bgcolor} !important";
            }
            if ( $image_url ) {
                $style[] = "background-image: url({$image_url}) !important";
                $style[] = 'background-position: top center';
                $style[] = 'background-repeat: no-repeat';
            }
?>
<?php if ( $image_link ): ?>
<a style="display: block" target="_blank" href="<?php echo $image_link; ?>">
<?php endif; ?>
    <div id="sm-takeover" style="<?php echo implode( '; ', $style ); ?>;position:fixed;overflow:hidden;z-index: 1; width:100%;margin-left:auto;margin-right:auto;left:0;right:0;top:0px;height:2000px;"></div>
<?php if ( $image_link ): ?>
</a>
<?php endif; ?>
<?php
        }
    }

	/**
	 * Add takeover styling to body
     * 
	 * @param int $post_id Page to check for takeover information
	 */
	public function add_body_style() {
?>
<style>
	#page-wrapper { z-index: 2; position: relative; }
</style>
<?php
	}
    
    /**
	 * Register meta boxes
	 * 
	 * @param array $meta_boxes Existing metaboxes
	 * @return array Array of meta boxes
	 */
	public function set_meta_boxes($meta_boxes = array()) {
		if (!is_array($meta_boxes)) {
			$meta_boxes = array();
		}
		$prefix = 'takeover';
		
		// takeovers are available on all pages so get custom post types
		$post_types = get_post_types();
		
		$meta_boxes[] = array(
			'id' => 'takeover',
			'title' => __('Takeover settings', 'takeover'),
			'object_types' => $post_types, // Post types
			'context' => 'normal',
			'priority' => 'low',
			'show_names' => true, // Show field names on the left
			'fields' => array(
				array(
					'name' => __('Enable Takeover', 'takeover'),
					'desc' => __('Check this to enable takeover options', 'takeover'),
					'id' => $prefix . '_active',
					'type' => 'checkbox',
					'options' => array(
						'enable' => '1',
					),
				),
				array(
					'name' => __('Background color', 'takeover'),
					'desc' => __('Takeover page background color', 'takeover'),
					'id' => $prefix . '_bgcolor',
					'type' => 'colorpicker',
				),
				array(
					'name' => __('Takeover image', 'takeover'),
					'desc' => __('Background image of takeover', 'takeover'),
					'id' => $prefix . '_image',
					'type' => 'file',
					'allow' => array( 'url', 'attachment' ),
				),
                array(
					'name' => __('Takeover Link', 'takeover'),
					'desc' => __('Page takeover links to', 'takeover'),
					'id' => $prefix . '_link',
					'type' => 'text_url',
				),
			),
		);
		
		return $meta_boxes;
	}
}

$GLOBALS['smedia']['takeover'] = new SM_Takeover();