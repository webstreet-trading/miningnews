<?php
/**
 * 3SMedia Top Contributors
 *
 * @package   SM_Top_Contributors
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

/**
 * Top Contributors
 *
 * @package SM_Top_Contributors
 * @author  Charles Coleman <echcoleman@gmail.com>
 */
class SM_Top_Contributors {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		$plugin = SMedia::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();
		
		// only run in admin section
		if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
			// custom user profile fields
			if ( current_user_can( 'create_users' ) ) {
				add_action( 'show_user_profile', array( $this, 'show_extra_profile_fields' ) );
				add_action( 'edit_user_profile', array( $this, 'show_extra_profile_fields' ) );

				add_action( 'personal_options_update', array( $this, 'save_extra_profile_fields' ) );
				add_action( 'edit_user_profile_update', array( $this, 'save_extra_profile_fields' ) );
			}
		}
		
		// remove auto loading rel=next post link in header since it messes with accurate post views
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );

		// track page views
		add_action( 'wp_head', array( $this, 'track_page_views' ) );
		
		// on load check if top contributor page should be initialized
		add_action( 'wp_head', array( $this, 'load' ), 1 );
	}
	
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	/**
	 * Add additional profile fields
	 *
	 * @since    1.0.0
	 *
	 * @param    WP_User  $user     User being edited
	 */
	public function show_extra_profile_fields( $user ) {
		
?>
<h3><?php _e( 'Admin Settings', $this->plugin_slug ); ?></h3>

<table class="form-table">
<tr>
	<th><label for="sm_post_views"><?php echo apply_filters( 'user_sm_post_views_label', __('Author Post Views Number', $this->plugin_slug) ); ?></label></th>
	<td><input type="text" name="sm_post_views" id="sm_post_views" value="<?php echo esc_attr($user->sm_post_views) ?>" class="regular-text" /></td>
</tr>
</table>
<?php
	}
	
	/**
	 * Save additional profile fields
	 *
	 * @since    1.0.0
	 *
	 * @param    int    $user_id    User id to save fields for
	 */
	public function save_extra_profile_fields( $user_id ) {

		if ( !current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}
		
		update_usermeta( $user_id, 'sm_post_views', (int) $_POST['sm_post_views'] );
	}

	/**
	 * Top Contributors: Track page views
	 *
	 * @since    1.0.0
	 */
	public function track_page_views() {
		
		global $post;
		
		// bots list (we do not track bot views)
		$bots = array(
			'wordpress', 'googlebot', 'google', 'msnbot', 'ia_archiver', 'lycos', 'jeeves', 'scooter',
			'fast-webcrawler', 'slurp@inktomi', 'turnitinbot', 'technorati', 'yahoo', 'findexa', 
			'findlinks', 'gaisbo', 'zyborg', 'surveybot', 'bloglines', 'blogsearch', 'pubsub', 
			'syndic8', 'userland', 'gigabot', 'become.com'
		);
		
		if ( !empty( $_SERVER['HTTP_USER_AGENT'] ) && is_singular()
			&& !preg_match( '/' . implode( '|', $bots ) . '/i', $_SERVER['HTTP_USER_AGENT'] ) ) {
			
			// get post author(s)
			// compatible with co-authors plus
			if ( function_exists('get_coauthors') ) {
				$authors = get_coauthors();
			}
			else {
				$authors = array( new WP_User( $post->post_author ) );
			}
			
			// ensure that views aren't affected by author
			if ( is_user_logged_in() ) {
				foreach ( $authors as $author ) {
					if ( get_current_user_id() == $author->ID ) {
						return;
					}
				}
			}
			
			// update all authors view count
			foreach ( $authors as $author ) {
				// only track authors
				if ( $author instanceof WP_User && in_array( 'author', $author->roles ) ) {
					// get author's total post views, increment by 1 and save
					$views = (int) get_user_meta( $author->ID, 'sm_post_views', true );
					$views++;
					update_user_meta( $author->ID, 'sm_post_views', $views );
				}
			}
		}
	}
	
	/**
	 * Get number of post views for author
	 *
	 * @since    1.0.0
	 *
	 * @param    WP_User  $author     Author user to get post views for
	 * @return   int      Number of post views for author
	 */
	public static function get_list( $sort = 'post_views', $args = array() ) {
		
		// set default sort
		if ( $sort == 'post_views' ) {
			$default_order = array(
				'orderby' => 'sm_post_views',
				'order' => 'DESC',
			);
		}
		else {
			$default_order = array(
				'orderby' => 'display_name',
				'order' => 'ASC',
			);
		}
		
		// default args - check that user has post views and it is greater than 0
		$defaults = array(
			'meta_key' => 'sm_post_views',
			'meta_query' => array(
				array(
					'key' => 'sm_post_views',
					'value' => '0',
					'compare' => '>'
				)
			),
		);
		$args = array_merge( $args, $defaults, $default_order );
		
		// get users
		$users = get_users( $args );
		return $users;
	}
	
	/**
	 * Load Top Contributor if on correct page
	 *
	 * @since    1.0.0
	 */
	public function load() {
		global $post;
        
        // if post id not available, do nothing
		if ( !$post->ID ) {
			return;
		}
		
		if ( (is_page()) && !is_admin() ) {
			
			$defaults = array(
				'tc_page' => 0,
				'tc_page_az' => 0,
			);
			
			$settings = get_option( 'smedia_settings', '' );
			if ( !is_array( $settings ) ) {
				$settings = array();
			}
			
			$settings = array_merge( $settings , $defaults );
			
			switch ( $post->ID ) {
				
				// top contributor page
				case $settings['tc_page']:
					add_filter( 'the_content', array( $this, 'content_top_contributors' ) );
					break;
				
				// contributor az page
				case $settings['tc_page_az']:
					add_filter( 'the_content', array( $this, 'content_contributors_az' ) );
					break;
				
				default:
					// do nothing
					break;
			}
		}
	}
	
	/**
	 * Content for top contributors
	 *
	 * @since    1.0.0
	 *
	 * @param    string   $content    Current Content
	 * @return   string   New content
	 */
	public function content_top_contributors( $content ) {
		
		$users = self::get_list();
		
		ob_start();
		include_once ( dirname( dirname( __FILE__ ) ) . '/public/views/top-contributors/top-contributors.php' );		
		$content = ob_get_contents();
		ob_end_clean();
		
		return $content;
	}
	
	/**
	 * Content for contributors az
	 *
	 * @since    1.0.0
	 *
	 * @param    string   $content    Current Content
	 * @return   string   New content
	 */
	public function content_contributors_az( $content ) {
		
		$users = self::get_list( 'az' );
		
		ob_start();
		include_once ( dirname( dirname( __FILE__ ) ) . '/public/views/top-contributors/contributors-az.php' );		
		$content = ob_get_contents();
		ob_end_clean();
		
		return $content;
	}
}
