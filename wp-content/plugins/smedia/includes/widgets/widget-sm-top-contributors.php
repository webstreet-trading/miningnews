<?php

/**
 * 3SMedia Top Contributors widget
 */
class Widget_SM_Top_Contributors extends WP_Widget {

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {
		
		// Call $plugin_slug from public plugin class.
		$plugin = SMedia::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// setup widget
		parent::__construct(
			'sm-top-contributors',
			__( 'Top Contributors', $this->plugin_slug ),
			array(
				'classname'		=>	'widget-sm-top-contributors',
				'description'	=>	__( 'Show Top Contributors in sidebar widget', $this->plugin_slug )
			)
		);

	} // end constructor

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param	array	args		The array of form elements
	 * @param	array	instance	The current instance of the widget
	 */
	public function widget( $args, $instance ) {
		
		$num = (int) $instance['num'];
		if ($num <= 0) {
			$num = 10;
		}
		
		// get users
		$users = SM_Top_Contributors::get_list( array( 'number' => $num ) );
		if ( !empty( $users ) ) {
			
			extract( $args, EXTR_SKIP );
		
			// set title
			$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
			if ( !$title ) {
				$title = __( 'Top Contributors', $this->plugin_slug );
			}

			echo $before_widget;

			echo $before_title;
			echo $title;
			echo $after_title;
        
        ?>

<ul class="sm-contributors">
	<?php
	$pos = 0;
	foreach ( $users as $user ) {
		$pos++;
		$user_url = get_author_posts_url( $user->ID );
	?>
	<li>
		<div class="sm-contributor-image">
			<a href="<?php echo $user_url; ?>"><?php echo get_avatar( $user->ID, '50' ); ?></a>
		</div>
		<div class="sm-contributor-pos">
			<div class="sm-contributor-num"><?php echo $pos; ?></div>
		</div>
		<div class="sm-contributor-content">
			<div class="sm-contributor-name"><a href="<?php echo $user_url; ?>"><?php echo $user->display_name; ?></a></div>
			<?php printf( __( 'Number of post views: %s', $this->plugin_slug ), get_the_author_meta( 'sm_post_views', $user->ID ) ); ?>
		</div>
	</li>
	<?php
	}
	?>
</ul>
		<?php
		
			echo $after_widget;
		}

	} // end widget

	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param	array	new_instance	The new instance of values to be generated via the update.
	 * @param	array	old_instance	The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		// num must be int
		$instance['num'] = (int) $new_instance['num'];

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param	array	instance	The array of keys and values for the widget.
	 */
	public function form( $instance ) {

    	// default widget values
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => __( 'Top Contributors', $this->plugin_slug ),
				'num' => 10,
			)
		);
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('num'); ?>"><?php _e( 'Number to display:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo $instance['num']; ?>" class="" id="<?php echo $this->get_field_id( 'num' ); ?>" />
		</p>
		<?php

	} // end form

} // end class

add_action( 'widgets_init', create_function( '', 'register_widget("Widget_SM_Top_Contributors");' ) );
