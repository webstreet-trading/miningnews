<?php

/**
 * 3SMedia Tag of the month widget
 */
class Widget_SM_Totm extends WP_Widget {

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {
		
		// Call $plugin_slug from public plugin class.
		$plugin = SMedia::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// setup widget
		parent::__construct(
			'sm-totm',
			__( '3SMedia Tag of the Month', $this->plugin_slug ),
			array(
				'classname'		=>	'widget-sm-totm',
				'description'	=>	__( 'Tag of the month sidebar widget', $this->plugin_slug )
			)
		);

	} // end constructor

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param	array	args		The array of form elements
	 * @param	array	instance	The current instance of the widget
	 */
	public function widget( $args, $instance ) {
		
		$num = (int) $instance['num'];
		if ($num <= 0) {
			$num = 5;
		}
		
		// get posts
		$posts = get_posts( array( 'tag' => $instance['tag'], 'numberposts' => $num) );
		
		// if posts found
		if ( !empty( $posts) ) {
			
			extract( $args, EXTR_SKIP );

			// set title
			$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
			
			$term = get_term_by( 'slug', $instance['tag'], 'post_tag' );
			
			// color styles
			$widget_styles = array();
			if ( $instance['widget-color'] ) {
				$widget_styles[] = 'color: ' . $instance['widget-color'];
			}
			if ( $instance['widget-bg'] ) {
				$widget_styles[] = 'background: ' . $instance['widget-bg'];
			}
			
			$highlight_styles = array();
			if ( $instance['color'] ) {
				$highlight_styles[] = 'color: ' . $instance['color'];
			}
			if ( $instance['color-bg'] ) {
				$highlight_styles[] = 'background: ' . $instance['color-bg'];
			}
		
			echo $before_widget;
		?>

		<div class="sm-totm-wrapper" style="<?php echo implode( '; ', $widget_styles ); ?>">
			
			<?php if ( $instance['symbol'] ): ?>
			<div class="sm-totm-symbol" style="<?php echo implode( '; ', $highlight_styles ); ?>"><?php echo $instance['symbol']; ?></div>
			<?php endif; ?>
			<?php if ( $title ): ?>
			<div class="sm-totm-title"><?php echo $title; ?></div>
			<?php endif; ?>
			<div class="sm-totm-tag-name"><?php echo $instance['tag-name']; ?></div>
			<div class="sm-totm-content">
				<ul>
					<?php foreach ( $posts as $post ) : ?>
					<li><a href="<?php echo apply_filters( 'the_permalink', get_permalink( $post->ID ) ); ?>"><?php echo get_the_title( $post->ID ); ?></a></li>
					<?php endforeach; ?>
				</ul>
				<a href="<?php echo get_tag_link( $term->term_id ); ?>"><?php _e( 'View More', $this->plugin_slug ); ?></a>
			</div>
			
		</div>

		<?php
			echo $after_widget;
		
		}

	} // end widget

	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param	array	new_instance	The new instance of values to be generated via the update.
	 * @param	array	old_instance	The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		$instance['symbol'] = esc_attr( $new_instance['symbol'] );
		$instance['tag-name'] = esc_attr( $new_instance['tag-name'] );
		$instance['widget-color'] = esc_attr( $new_instance['widget-color'] );
		$instance['widget-bg'] = esc_attr( $new_instance['widget-bg'] );
		$instance['color'] = esc_attr( $new_instance['color'] );
		$instance['color-bg'] = esc_attr( $new_instance['color-bg'] );
		$instance['tag'] = esc_attr( $new_instance['tag'] );
		
		// num must be int
		$instance['num'] = (int) $new_instance['num'];

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param	array	instance	The array of keys and values for the widget.
	 */
	public function form( $instance ) {

    	// default widget values
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => __( 'Feature of the Month', $this->plugin_slug ),
				'num' => 5,
			)
		);
		
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'symbol' ); ?>"><?php _e( 'Symbol:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'symbol' ); ?>" value="<?php echo $instance['symbol']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'symbol' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tag-name' ); ?>"><?php _e( 'Feature Name:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'tag-name' ); ?>" value="<?php echo $instance['tag-name']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'tag-name' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'widget-color' ); ?>"><?php _e( 'Widget Color (CSS format: #CCCCCC):', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'widget-color' ); ?>" value="<?php echo $instance['widget-color']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'widget-color' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'widget-bg' ); ?>"><?php _e( 'Widget Background Color (CSS format: #CCCCCC):', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'widget-bg' ); ?>" value="<?php echo $instance['widget-bg']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'widget-bg' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e( 'Highlight Color (CSS format: #CCCCCC):', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'color' ); ?>" value="<?php echo $instance['color']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'color' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'color-bg' ); ?>"><?php _e( 'Highlight Background Color (CSS format: #CCCCCC):', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'color-bg' ); ?>" value="<?php echo $instance['color-bg']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'color-bg' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tag' ); ?>"><?php _e( 'Tag (Posts with this tag will be displayed):', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'tag' ); ?>" value="<?php echo $instance['tag']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'tag' ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e( 'Number of posts to display:', $this->plugin_slug ); ?></label>
            <input type="text" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo $instance['num']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" />
		</p>
		
		<?php

	} // end form

} // end class

add_action( 'widgets_init', create_function( '', 'register_widget("Widget_SM_Totm");' ) );
