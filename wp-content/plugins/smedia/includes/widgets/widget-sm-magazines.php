<?php

/**
 * 3SMedia Magazines widget
 */
class Widget_SM_Magazines extends WP_Widget {

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {
		
		// Call $plugin_slug from public plugin class.
		$plugin = SMedia::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// setup widget
		parent::__construct(
			'sm-magazines',
			__( '3SMedia Magazines', $this->plugin_slug ),
			array(
				'classname'		=>	'widget-sm-magazines',
				'description'	=>	__( 'Show site Magazines in sidebar widget', $this->plugin_slug )
			),
			array(
				'width'    =>  250,
				'height'   =>  350
			)
		);

	} // end constructor

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param	array	args		The array of form elements
	 * @param	array	instance	The current instance of the widget
	 */
	public function widget( $args, $instance ) {
		
		// ensure magazine are setup
		if ( empty( $instance['magazines'] ) ) {
			return;
		}
			
		extract( $args, EXTR_SKIP );

		// set title
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		if ( !$title ) {
			$title = __( 'Magazines', $this->plugin_slug );
		}

		echo $before_widget;

		echo $before_title;
		echo $title;
		echo $after_title;

		?>

<div class="sm-magazines">
	<?php foreach ( $instance['magazines'] as $magazine ): ?>

	<div class="sm-magazine">
		<div class="sm-magazine-image">
			<a target="_blank" href="<?php echo $magazine['magazine_url']; ?>"><img src="<?php echo $magazine['magazine_image']; ?>" class="alignleft" /></a>
		</div>
		<div class="content">
			<?php echo wpautop( $magazine['magazine_content'] ); ?>
			<center>
				<a target="_blank" href="<?php echo $magazine['magazine_url']; ?>" class="button"><?php _e( 'Subscribe', $this->plugin_slug ); ?></a>
			</center>
		</div>
	</div>

	<?php endforeach; ?>
</div>

<?php if ( count( $instance['magazines'] ) > 1 ): ?>
<script type="text/javascript">
(function($) {
	$('.sm-magazines').bxSlider({
		minSlides: 1,
		maxSlides: 1,
		slideMargin: 0,
		pager: false,
        auto: true,
        autoHover: true
	});
})(jQuery);
</script>
<?php endif; ?>

		<?php
		echo $after_widget;

	} // end widget

	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param	array	new_instance	The new instance of values to be generated via the update.
	 * @param	array	old_instance	The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		
		// get number of entries
		$count = count( $new_instance['magazine_content'] );
		$magazines = array();
		for( $i=0; $i<$count; $i++ ) {
			if (  !isset( $new_instance['magazine_content'][$i] )
					|| !isset( $new_instance['magazine_image'][$i] )
					|| !isset( $new_instance['magazine_url'][$i] ) 
					) {
				continue;
			}
			if (  !trim( $new_instance['magazine_content'][$i] )
					&& !trim( $new_instance['magazine_image'][$i] )
					&& !trim( $new_instance['magazine_url'][$i] )
					) {
				continue;
			}
			$magazines[$i]['magazine_content'] = esc_attr( $new_instance['magazine_content'][$i] );
			$magazines[$i]['magazine_image'] = esc_attr( $new_instance['magazine_image'][$i] );
			$magazines[$i]['magazine_url'] = esc_attr( $new_instance['magazine_url'][$i] );
		}
		$instance['magazines'] = $magazines;

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param	array	instance	The array of keys and values for the widget.
	 */
	public function form( $instance ) {
		
		$default_magazine = array(
			'magazine_content' => '',
			'magazine_image' => '',
			'magazine_url' => '',
		);
		
    	// default widget values
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => __( 'Magazines', $this->plugin_slug ),
				'magazines' => array( $default_magazine ),
			)
		);
		$pos = 0;
		?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', $this->plugin_slug ); ?></label>
		<input type="text" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" />
	</p>
		
	<!-- Magazines Template -->
	<div class="sm-magazines">
		<?php
		// get template to use
		ob_start(); ?>
		<div class="magazine">
			<h3><?php _e( 'Magazine', $this->plugin_slug ); ?> <span class="magazine-num"><?php echo $pos; ?></span></h3>
			<p>
				<label><?php _e( 'Content:', $this->plugin_slug ); ?></label>
				<textarea name="<?php echo $this->get_field_name( 'magazine_content' ); ?>[]" col="12" rows="8" class="widefat"><?php echo $magazine['magazine_content']; ?></textarea>
			</p>
			<p>
				<label><?php _e( 'Image URL:', $this->plugin_slug ); ?></label>
				<input type="text" name="<?php echo $this->get_field_name( 'magazine_image' ); ?>[]" value="<?php echo $magazine['magazine_image']; ?>" class="widefat" />
			</p>
			<p>
				<label><?php _e( 'Subscribe URL:', $this->plugin_slug ); ?></label>
				<input type="text" name="<?php echo $this->get_field_name( 'magazine_url' ); ?>[]" value="<?php echo $magazine['magazine_url']; ?>" class="widefat" />
			</p>
		</div>
		<?php
		$template = str_replace( "\r", '', str_replace( "\n", '', ob_get_clean() ) );
		?>
		
		<?php foreach ( $instance['magazines'] as $magazine ): $pos++; ?>
		<div class="magazine">
			<h3><?php _e( 'Magazine', $this->plugin_slug ); ?> <span class="magazine-num"><?php echo $pos; ?></span></h3>
			<p>
				<label><?php _e( 'Content:', $this->plugin_slug ); ?></label>
				<textarea name="<?php echo $this->get_field_name( 'magazine_content' ); ?>[]" col="12" rows="8" class="widefat"><?php echo $magazine['magazine_content']; ?></textarea>
			</p>
			<p>
				<label><?php _e( 'Image URL:', $this->plugin_slug ); ?></label>
				<input type="text" name="<?php echo $this->get_field_name( 'magazine_image' ); ?>[]" value="<?php echo $magazine['magazine_image']; ?>" class="widefat" />
			</p>
			<p>
				<label><?php _e( 'Subscribe URL:', $this->plugin_slug ); ?></label>
				<input type="text" name="<?php echo $this->get_field_name( 'magazine_url' ); ?>[]" value="<?php echo $magazine['magazine_url']; ?>" class="widefat" />
			</p>
		</div>
		<?php endforeach; ?>
	</div>
		
	<p>
		<button id="sm-magazine-add" type="button" class="sm-magazine-add button"><?php _e( 'Add Magazine', $this->plugin_slug ); ?></button>
		<input type="hidden" name="sm-magazine-widget-id" class="sm-magazine-widget-id" value="<?php echo $this->number; ?>" />
		<input type="hidden" name="sm-magazine-template" class="sm-magazine-template" value='<?php echo addslashes( $template ) ?>' />
	</p>
		
		<?php

	} // end form

} // end class

add_action( 'widgets_init', create_function( '', 'register_widget("Widget_SM_Magazines");' ) );
