<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   SMedia
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */
?>

<div class="wrap">

	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
	<form method="post" action="options.php" enctype="multipart/form-data">
		<?php settings_fields( 'smedia' ); ?>
		<?php do_settings_sections( $this->plugin_slug ); ?>
		<p class="submit">
			<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e( 'Save Changes', $this->plugin_slug ); ?>" />
		</p>
	</form>

</div>
