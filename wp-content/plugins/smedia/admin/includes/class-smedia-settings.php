<?php
/**
 * 3SMedia Settings
 *
 * @package   SMedia_Admin_Settings
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 Charles Coleman
 */

/**
 * SMedia_Admin_Settings class
 *
 * @package SMedia_Admin_Settings
 * @author  Charles Coleman <echcoleman@gmail.com>
 */
class SMedia_Admin_Settings {
	
	/**
	 * Saved settings.
	 *
	 * @since    1.0.0
	 *
	 * @var      array
	 */
	protected $setting;
	
	/**
	 * Construct settings
	 *
	 * @since     1.0.0
	 */
	public function __construct() {
		
		/*
		 * Call $plugin_slug from public plugin class.
		 */
		$plugin = SMedia::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();
		
		// get saved settings options
		$this->setting = self::get_settings();
		
		// setup settings
		$this->setup_settings();
		
		// register the checkbox
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}
	
	/**
	 * Return saved settings
	 *
	 * @since     1.0.0
	 *
	 * @return    array    Settings
	 */
	public static function get_settings() {
		return get_option( 'smedia_settings', '' );
	}
		
	/**
	 * Setup the settings
	 * 
	 * @since     1.0.0
	 */
	public function setup_settings() {
		
		register_setting( 'smedia', 'smedia_settings', array( $this, 'validate_settings' ) );
		
		// get settings
		$settings = $this->settings();
		
		// setup sections and settings
		foreach ( $settings as $section => $section_data ) {
			
			// add section
			$section = $this->plugin_slug . '_' . $section;
			add_settings_section(
				$section,
				$section_data['title'],
				array( $this, 'callback_blackhole' ),
				$this->plugin_slug
			);
			
			// add section settings
			foreach ( $section_data['settings'] as $setting ) {
				
				if ( !isset( $setting['id'] ) ) {
					continue;
				}
				$options = array_merge(array('section' => 'smedia_settings'), $setting);
				
				add_settings_field(
					$setting['id'],
					$setting['title'],
					array( $this, 'setting_callback' ),
					$this->plugin_slug,
					$section,
					$options
				);
			}
		}
	}
	
	/**
	 * Setup the settings
	 * 
	 * @since    1.0.0
	 * 
	 * @return   array    Settings page information
	 */
	protected function settings() {
		
		return array(
			'general' => array(
				'title' => __( 'Top Contributors', $this->plugin_slug ),
				'settings' => array(
					array(
						'title' => __( 'Page', $this->plugin_slug ),
						'id' => 'tc_page',
						'type' => 'page',
						'desc' => __( 'Page Top Contributors will be listed', $this->plugin_slug ),
					),
					array(
						'title' => __( 'Alphabetical Page', $this->plugin_slug ),
						'id' => 'tc_page_az',
						'type' => 'page',
						'desc' => __( 'Page Contributors will be listed alphabetically', $this->plugin_slug ),
					),
				)
			)
		);
	}
	
	/**
	 * Setting callback to ouput setting field
	 * 
	 * @since    1.0.0
	 * 
	 * @param    array    $field    Setting field data
	 */
	public function setting_callback( $field ) {
		
		if ( !isset( $field['type'] ) ) {
			return;
		}
		
		// field defaults
		$field_defaults = array(
			'id' => '',
			'title' => '',
			'class' => '',
			'css' => '',
			'default' => '',
		);
		$field = array_merge( $field_defaults, $field );
		
		// set description
		if ( isset( $field['desc'] ) ) {
			$description = '<span class="description">' . wp_kses_post( $field['desc'] ) . '</span>';
		}
		else {
			$description = '';
		}
		
		// set name
		$name = $field['section'] . '[' . $field['id'] . ']';
		
		// get current value
		$value = $this->get_value( $field['id'], $field['default'] );
		
		switch ( $field['type'] ) {
			case 'text':
				echo "<input name='{$name}' id='{$field['id']}' type='text' value='{$value}' class='{$field['class']}' style='{$field['css']}' />";
				echo $description;
				
				break;

			case 'textarea':
				$defaults = array(
					'cols' => 40,
					'rows' => 6,
				);
				$field = array_merge($defaults, $field);
				echo "<textarea name='{$name}' id='{$field['id']}' cols='{$field['cols']}' rows='{$field['rows']}' class='{$field['class']}' style='{$field['css']}'>{$value}</textarea>";
				echo $description;
				
				break;
			
			case 'select':
			case 'multiselect':
				
				?>

				<select
					name="<?php echo $name; ?>" id="<?php echo $field['id']; ?>"
					class="<?php echo $field['class']; ?>" style="<?php echo $field['css']; ?>"
					<?php if ( $field['type'] == 'multiselect' ) echo 'multiple="multiple"'; ?>
				>
					<?php
					foreach ( $field['options'] as $key => $val ) {
						if ( is_array( $value ) ) {
							$selected = selected( in_array( key, $value ), $key, false );
						}
						else {
							$selected = selected( $value, $key, false );
						}
						?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php echo $selected; ?>><?php echo $val ?></option>
						<?php
					}
					?>
					
				</select>
				<?php echo $description; ?>

				<?php
				
				break;
				
			case 'page':
				
				wp_dropdown_pages(array(
					'name' => $name,
					'show_option_none' => __( 'no page selected', $this->plugin_slug ),
					'selected' => $value
				));
				echo $description;
				
				break;
		}
		
	}
	
	/**
	 * Validate Settings
	 *
	 * @since    1.0.0
	 *
	 * @param    string   $setting    Setting field id
	 * @param    string   $default    Default value for field
	 * 
	 * @return   string   Setting value
	 */
	public function get_value( $setting, $default = '' ) {
		
		// get setting if exists
		if ( isset( $this->setting[$setting] ) ) {
			return $this->setting[$setting];
		}
		else {
			return $default;
		}
	}
	
	/**
	 * Validate Settings
	 *
	 * @since    1.0.0
	 *
	 * @param    array    $input    Settings input
	 * 
	 * @return   boolean  Return early if no settings page is registered.
	 */
	public function validate_settings( $input ) {
		
		// flush rewrite rules
		flush_rewrite_rules();
		
		// no checking on settings
		return $input;
	}
	
	/**
	 * Callback with no output
	 *
	 * @since    1.0.0
	 */
	public function callback_blackhole() {}
}