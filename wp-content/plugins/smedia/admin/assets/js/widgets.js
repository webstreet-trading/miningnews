(function ( $ ) {
	"use strict";

	$(function () {
		
		$().ready(function(){
			
			// add magazines
			$(".sm-magazine-add").on( 'click', function(){
				// get magazines section
				var magazines = $(this).parent().siblings(".sm-magazines");
				
				// create element from template
				var magazine = $( $(".sm-magazine-template").val().replace(/\\/g, '') );
				
				// add counter
				magazine.find('.magazine-num').html( magazines.find(".magazine").length + 1 );
				
				// replace placeholder with widget id
				var widget_id = $(this).siblings('.sm-magazine-widget-id').val();
				var html = magazine.html().replace( /<[^<>]+>/g, function( m ) {
					return m.replace( /__i__|%i%/g, widget_id );
				} );
				magazine.html(html);
				
				// add magazine
				magazines.append(magazine);
			});
			
		});

	});

}(jQuery));