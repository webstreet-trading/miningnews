<?php
/**
 * Top Contributors
 *
 * @package   SMedia
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

include( 'elements/menu.php' );
?>
<?php if ( !empty( $users ) ): ?>
<ul class="sm-contributors">
	<?php
	$pos = 0;
	foreach ( $users as $user ) {
		$pos++;
		include ('elements/contributor.php');
	}
	?>
</ul>
<?php endif; ?>