<?php
/**
 * Contributor
 *
 * @package   SMedia
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

$user_url = get_author_posts_url( $user->ID );
?>
<li>
	<?php if ($pos): ?>
	<div class="sm-contributor-pos">
		<div class="sm-contributor-num"><?php echo $pos; ?></div>
	</div>
	<?php endif; ?>
	<div class="sm-contributor-image">
		<a href="<?php echo $user_url; ?>"><?php echo get_avatar( $user->ID, '80' ); ?></a>
	</div>
	<div class="sm-contributor-content">
		<div class="sm-contributor-name"><a href="<?php echo $user_url; ?>"><?php echo $user->display_name; ?></a></div>
		<?php printf( __( 'Number of post views: %s', $this->plugin_slug ), get_the_author_meta( 'sm_post_views', $user->ID ) ); ?>
		<a class="sm-contributor-link" href="<?php echo $user_url; ?>">
			<?php _e( 'View all posts', $this->plugin_slug ); ?>
		</a>
	</div>
</li>