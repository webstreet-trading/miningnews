<?php
/**
 * Contributor menu
 *
 * @package   SMedia
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

global $post;

$menu = array();

// get paths
$settings = get_option( 'smedia_settings' );

// top contributor page
if ( isset( $settings['tc_page'] ) && get_page( $settings['tc_page'] ) ) {
	$permalink = ($settings['tc_page'] == $post->ID)? false:get_permalink( $settings['tc_page'] );
	$menu[] = array(
		'permalink' => $permalink,
		'title' => __( 'Top Contributors', $this->plugin_url )
	);
}

// contributor az page
if ( isset( $settings['tc_page_az'] ) && get_page( $settings['tc_page_az'] ) ) {
	$permalink = ($settings['tc_page_az'] == $post->ID)? false:get_permalink( $settings['tc_page_az'] );
	$menu[] = array(
		'permalink' => $permalink,
		'title' => __( 'Contributors A-Z', $this->plugin_url )
	);
}
?>
<?php if ( !empty($menu) ): ?>
<ul class="sm-contributor-menu">
	<?php foreach ($menu as $item): ?>
	<li>
		<?php if ($item['permalink']): ?>
		<a href="<?php echo $item['permalink']; ?>">
		<?php endif; ?>
			<?php echo $item['title']; ?>
		<?php if ($item['permalink']): ?>
		</a>
		<?php endif; ?>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>