<?php
/**
 * Contributors A-Z
 *
 * @package   SMedia
 * @author    Charles Coleman <echcoleman@gmail.com>
 * @license   GPL-2.0+
 * @link      http://www.3smedia.co.za/
 * @copyright 2014 3SMedia (Pty) Ltd
 */

include( 'elements/menu.php' );
?>
<?php 
if ( !empty( $users ) ) {
	$pos = false;
	
	// range
	$alphabet = range( 'A', 'Z' );
	$keys = array_merge( array( '#' ), $alphabet );
	$values = array_pad( array(), count($keys), false );
	$range = array_combine( $keys, $values );
	
	ob_start();
	foreach ( $users as $user ) {
		
		// get letter for section
		$letter = strtoupper( $user->display_name[0] );
		if ( !in_array( $letter, $alphabet ) ) {
			$letter = '#';
		}
		if ( $range[ $letter ] === false ) {
			$range[ $letter ] = true;
			echo '<a class="sm-contributor-anchor" name="' . $letter . '">' . $letter . '</a>';
		}
		
		// display contributor
		include ('elements/contributor.php');
	}
	$contributors = ob_get_contents();
	ob_end_clean();
	?>
<ul id="sm-contributors-letters">
	<?php foreach ( $range as $letter => $link ): ?>
	<li>
		<?php if ($link): ?>
		<a href="#<?php echo $letter; ?>">
		<?php endif; ?>
			<?php echo $letter; ?>
		<?php if ($link): ?>
		</a>
		<?php endif; ?>
	</li>
	<?php endforeach; ?>
</ul>
<div id="sm-contributors" class="contributors-az">
	<?php echo $contributors; ?>
</div>
<?php
}

