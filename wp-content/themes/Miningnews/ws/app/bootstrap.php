<?php
/**
 * Basic setup
 */

// include functions
require_once WS_APP . 'functions.php';

// set textdomain and version
WS::setup('ws', '1.0.0');
