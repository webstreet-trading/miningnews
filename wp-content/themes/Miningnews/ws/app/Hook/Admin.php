<?php

namespace WSApp\Hook;

use WS\Hook\Abstracts\Admin as Hook;

/**
 * WS Admin Class
 */
class Admin extends Hook {

    /**
     * Add settings
     * 
     * @param array $settings Current settings
     * @return array Settings to use
     */
    public function settings($settings) {

        // add your settings
        $add_settings = [
            // general
            [
                'name' => __('General', $this->textdomain),
                'id' => 'general_title',
                'type' => 'title',
            ],
            [
                'name' => __('Header Landing pages', $this->textdomain),
                'desc' => __('Listing of landing page logos that appear under the main header', $this->textdomain),
                'id' => 'header-landing-pages',
                'type' => 'group',
                'options' => [
                    'group_title' => __('Landing Page {#}', $this->textdomain),
                    'add_button' => __('Add Another', $this->textdomain),
                    'remove_button' => __('Remove', $this->textdomain),
                    'sortable' => true,
                ],
                'fields' => [
                    [
                        'id' => 'page',
                        'name' => __('Landing Page', $this->textdomain),
                        'desc' => __('To what does the landing page link to', $this->textdomain),
                        'type' => 'text_url',
                    ],
                    [
                        'id' => 'logo',
                        'name' => __('Logo', $this->textdomain),
                        'type' => 'file',
                        'query_args' => [
                            'type' => 'image'
                        ],
                        'options' => [
                            'url' => false,
                        ],
                    ],
                ],
            ],
            [
                'name' => __('Footer Tabs', $this->textdomain),
                'desc' => __('Tabs section just above the footer widgets.', $this->textdomain),
                'id' => 'footer-tabs',
                'type' => 'group',
                'options' => [
                    'group_title' => __('Tab {#}', $this->textdomain),
                    'add_button' => __('Add Another', $this->textdomain),
                    'remove_button' => __('Remove', $this->textdomain),
                    'sortable' => true,
                ],
                'fields' => [
                    [
                        'id' => 'title',
                        'name' => __('Heading', $this->textdomain),
                        'desc' => __('Tab text heading', $this->textdomain),
                        'type' => 'text',
                    ],
                    [
                        'id' => 'content',
                        'name' => __('Content', $this->textdomain),
                        'desc' => __('Tab content to display', $this->textdomain),
                        'type' => 'wysiwyg',
                    ],
                ],
            ],
            [
                'name' => __('Landing Menu', $this->textdomain),
                'desc' => __('Landing Page menu at the top of the landing page template main col.', $this->textdomain),
                'id' => 'landing-menu',
                'type' => 'group',
                'options' => [
                    'group_title' => __('Landing Menu Link {#}', $this->textdomain),
                    'add_button' => __('Add Another', $this->textdomain),
                    'remove_button' => __('Remove', $this->textdomain),
                    'sortable' => true,
                ],
                'fields' => [
                    [
                        'id' => 'title',
                        'name' => __('Menu Item', $this->textdomain),
                        'desc' => __('Enter Menu Item Name', $this->textdomain),
                        'type' => 'text',
                    ],
                    [
                        'id' => 'link',
                        'name' => __('Item Link', $this->textdomain),
                        'desc' => __('To what does the link scroll to', $this->textdomain),
                        'type' => 'text',
                    ],
                ],
            ],
        ];

        // combine settings and return
        return array_merge($settings, $add_settings);
    }

}
