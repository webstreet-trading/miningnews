<?php

namespace WSApp\Loader;

use WS\Loader\Init as Loader;

/**
 * WS Init Class
 */
class Init extends Loader {
    
    /**
     * Register system hooks
     */
    public function system_hooks() {
        
        
    }
    
    /**
     * Register public hooks
     */
    public function public_hooks() {
        
    }
    
    /**
     * Register admin hooks
     */
    public function admin_hooks() {
        
    }
    
    /**
     * Register custom post types and taxonomies
     */
    public function register_posts_taxonomies() {
        // register
    }
}
