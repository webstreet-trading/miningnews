<?php
/**
 * Form output
 *
 * Override this template by copying it to yourtheme/ws-view/form.php
 * 
 * @uses string $form
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<?php echo $form; ?>