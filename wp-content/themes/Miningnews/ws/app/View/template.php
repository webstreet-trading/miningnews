<?php
/**
 * Template page
 *
 * Override this template by copying it to yourtheme/ws-view/template.php
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

// get classes from ws template
$classes = array(WsTemplate::get_the_arg('controller'));
if (is_array(WsTemplate::get_the_arg('classes'))) {
    $classes = array_merge($classes, WsTemplate::get_the_arg('classes'));
}
else {
    $classes[] = WsTemplate::get_the_arg('classes');
}
?>

<?php get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<article id="ws-page" <?php post_class($classes); ?>>
                <header class="entry-header">
                    <?php WsTemplate::the_title('<h1 class="entry-title">', '</h1>'); ?>
                </header>
                <div class="entry-content">
                    <?php WsTemplate::the_content(); ?>
                </div>
            </article>

		</main>
	</div>
</div>

<?php get_footer(); ?>
