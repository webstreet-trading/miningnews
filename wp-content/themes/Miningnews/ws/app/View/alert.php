<?php
/**
 * Template page
 *
 * Override this template by copying it to yourtheme/ws-view/alert.php
 * 
 * @uses string $message Alert text message
 * @uses string $type Type of alert
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

// determine alert class
switch ($type) {
    case 'success':
        $class = 'alert-success';
        break;
    case 'error':
        $class = 'alert-error';
        break;
    case 'info':
        $class = 'alert-info';
        break;
    default:
        $class = '';
        break;
}
?>
<div class="alert <?php echo $class; ?>">
    <button type="button" class="alert-close">×</button>
    <div class="message"><?php echo $message; ?></div>
</div>