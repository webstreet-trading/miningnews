<?php
/**
 * Admin Alerts
 *
 * Override this template by copying it to yourtheme/ws-view/admin/alert.php
 * 
 * @uses string $message Alert text message
 * @uses string $type Type of alert
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

// determine alert class
switch ($type) {
    case 'success':
        $class = 'notice-success';
        break;
    case 'error':
        $class = 'notice-error';
        break;
    case 'info':
        $class = 'notice-info';
        break;
    default:
        $class = '';
        break;
}
?>
<div class="notice is-dismissible <?php echo $class; ?>">
    <p><?php echo $message; ?></p>
</div>