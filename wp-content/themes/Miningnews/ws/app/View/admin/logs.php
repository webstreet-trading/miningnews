<?php
/**
 * Logs page
 *
 * Override this template by copying it to yourtheme/ws-views/logs.php
 * 
 * @uses array $logs Array of log files
 * @uses string $view_log Log file currently being viewed
 * @uses array $notices Array of admin notices
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$logger = WS\Component\Logger::instance();
?>
<div class="wrap ws-logs">
<h2><?php _e('Integration Logs', $domain); ?></h2>
<?php
if (!empty($notices)) {
    foreach ($notices as $notice) {
        echo ws_view('admin/alert', $notice);
    }
}
?>
<?php if ($logs): ?>
	<div id="log-viewer-select">
		<div class="alignleft">
			<h3><?php printf(__('Viewing log: %s', $domain), esc_html($view_log)); ?>.log</h3>
		</div>
		<div class="alignright">
			<form id="logger-form" action="<?php echo admin_url('admin.php?page=' . WS::$textdomain . '_logger'); ?>" method="post">
				<select name="view">
					<?php foreach ($logs as $key => $log_file) : ?>
						<option value="<?php echo esc_attr($key); ?>" <?php selected(sanitize_title($view_log), $key); ?>><?php echo esc_html($log_file); ?></option>
					<?php endforeach; ?>
				</select>
				<input type="submit" class="button" value="<?php esc_attr_e('View', $domain); ?>" />
                <button id="log-clear" class="button"><?php _e('Clear', $domain); ?></button>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	<div id="log-viewer">
		<textarea cols="70" rows="25"><?php echo esc_textarea($logger->get_log_content($view_log)); ?></textarea>
	</div>
<?php else : ?>
	<div class="updated"><p><?php _e('There are currently no logs to view.', $domain); ?></p></div>
<?php endif; ?>
