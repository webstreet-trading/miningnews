<?php
/**
 * Template for options page
 *
 * Override this template by copying it to yourtheme/ws-view/admin/settings//infusionsoft-help.php
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$url = 'https://keys.developer.infusionsoft.com/apps/register';
?>
<p>
    <?php printf(__('To get the below client details you need to register a key here: <a href="%s" target="_blank">%s</a><br/>Once it\'s activated you\'ll be able to make call to the Infusionsoft API.', $domain), $url, $url); ?>
</p>