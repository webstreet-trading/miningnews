<?php
/**
 * Options page
 *
 * Override this template by copying it to yourtheme/ws-view/admin/settings/page_display.php
 * 
 * @uses string $key Menu page key
 * @uses string $metabox_id Id of metabox to display
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<div class="wrap cmb2-options-page <?php echo $key; ?>">
    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
    <?php cmb2_metabox_form($metabox_id, $key); ?>
</div>
