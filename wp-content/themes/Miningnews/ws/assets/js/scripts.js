/**
 * Functions
 */

if (typeof ws === 'undefined') {
    var ws = {};
}

/**
 * Output alert
 * 
 * @param {string} msg Alert message
 * @param {string} type Alert type
 * @void
 */
ws.alert = function(msg, type) {
    if (msg === undefined) {
        return;
    }
    
    // determine class
    var alert_class = 'alert ';
    if (type === 'error') {
        alert_class += ' alert-error';
    }
    else if (type === 'success') {
        alert_class += ' alert-success';
    }
    else if (type === 'info') {
        alert_class += ' alert-info';
    }
    
    // create alert
    var alert_close = jQuery('<button type="button" class="alert-close">×</button>')
        .click(ws.alert_close);
    var alert = jQuery('<div></div>')
        .addClass(alert_class)
        .html('<div class="message">' + msg + '</div>')
        .append(alert_close);
    
    // if block does not exist, create it
    if (jQuery('#alerts-block').length < 1) {
        jQuery('body').prepend(
            jQuery('<div></div>')
            .attr('id', 'alerts-block')
        );
    }
    
    // add alert to block
    jQuery('#alerts-block').append(alert);
};

/**
 * Success wrapper function for alerts
 * 
 * @param {string} msg Alert message
 * @void
 */
ws.success = function(msg) {
    ws.alert(msg, 'success');
};

/**
 * Error wrapper function for alerts
 * 
 * @param {mixed} msg Alert message or array of error messages from wp_send_json_error
 * @void
 */
ws.error = function(msg) {
    if (jQuery.isArray(msg)) {
        var message = '';
        jQuery.each( msg, function( key, value ) {
            message += value.message + '<br/>';
        });
        msg = message;
    }
    ws.alert(msg, 'error');
};

/**
 * Close alert when close button clicked
 * 
 * @void
 */
ws.alert_close = function() {
    jQuery(this).parent().remove();
};

/**
 * Clear all alerts
 * 
 * @void
 */
ws.alerts_clear = function() {
    jQuery('#alerts-block').remove();
};

/**
 * Match heights
 */
ws.matchheights = function() {
    // wait for images to load
    document.addEventListener(
        'load',
        function(event){
            var elm = event.target;
            if ( elm.nodeName.toLowerCase() === 'img' && jQuery(elm).closest('#main').length && !jQuery(elm).hasClass('loaded')){
                jQuery(elm).addClass('loaded');
                if (jQuery('#main img.loaded').length === jQuery('#main img').length) {
                    jQuery('.match-selection-height').each(function() {
                        // create class selector
                        var classes = '.' + jQuery(this).attr('class').replace(/ /g, '.');
                        jQuery(classes).matchHeight();
                    });
                    
                    // ensure that all heights match (if required)
                    jQuery('.matchheight').matchHeight();
                }
            }
        },
        true // Capture event
    );
};

/**
 * Detect clicks off
 * 
 * @param {function} callback
 */
jQuery.fn.clickOff = function(callback) {
    var clicked = false;
    var parent = this;
    
    parent.click(function() {
        clicked = true;
    });
    
    jQuery(document).click(function(event) { 
        if (!clicked) {
            callback(parent, event);
        }
        clicked = false;
    });
};

/**
 * WS Ajax
 * 
 * @param {mixed} data Ajax call data
 * @param {function} callback Function callback with ajax result. Optional
 */
ws.ajax = function(data, callback) {
    data += '&action=ws';
    jQuery.post(
        ws.ajaxurl,
        data,
        function(result){
            if (!result.success && result.data.message !== 'undefined') {
                ws.error(result.data.message);
            }
            else if (result.success && result.data.message !== 'undefined') {
                ws.success(result.data.message)
            }
            
            if (callback !== 'undefined') {
                callback(result);
            }
        }
    );
};

/**
 * WS Ajax Form
 * 
 * Make request with form that contains data
 * 
 * @param {DOMElement} form Form element to submit
 * @param {function} callback Function callback with ajax result. Optional
 */
ws.ajax_form = function(form, callback) {
    
    // get serialized form data
    var data = jQuery(form).serialize();
    
    // make request
    ws.ajax(data, callback);
};

/**
 * WS Ajax Data
 * 
 * Make request with element that contains data attributes
 * 
 * @param {DOMElement} element Element to get data from
 * @param {function} callback Function callback with ajax result. Optional
 */
ws.ajax_data = function(element, callback) {
    
    // get defined elements
    var data = {};
    data['controller'] = jQuery(element).data('controller');
    data['method'] = jQuery(element).data('method');
    data['params'] = jQuery(element).data('params');
    data['nonce'] = jQuery(element).data('nonce');
    
    // serialize data
    var data = jQuery.param(data);
    
    // make request
    ws.ajax(data, callback);
};

(function($) {
    "use strict";
    
    if (ws === 'undefined') {
        ws = {};
    }

    $().ready(function() {

        /* Alerts */
        $('#alerts-block').on('click', '.alert-close', ws.alert_close);

        /* Popups */
        
        // open popups on page load
        if ( $('.autopopup').length ) {
            $.magnificPopup.open({
                items: {
                    src: '.autopopup'
                },
                type: 'inline'
            });
        }

        // open popups with links
        $('.open-popup-link').magnificPopup({
            type: 'inline',
            midClick: true
        });
        
        /* Ajax */
        
        $('.ajax-link').click(function(e){
            e.preventDefault();
            ws.ajax_data(this);
        });

        $('.ajax-form').submit(function(e){
            e.preventDefault();
            ws.ajax_form(this);
        });

        /**
         * Misc
         */

        if ($.fn.tabs) {
            $('.tabs').tabs();
        }

        if ($.fn.datepicker) {
            $('.datepicker').datepicker({
                dateFormat: 'd MM yy',
                inline: true
            });
            $('.datepicker').keydown(function(e){
                if(e.keyCode === 13) {
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }
            });
        }
        
        if ($.fn.slick) {
            $('.carousel-slider').slick({
                autoplay: false,
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }

        // fix z-index youtube video embedding
        $('iframe').each(function(){
            var url = $(this).attr('src');
            $(this).attr('src',url+'?wmode=transparent');
        });

        // responsive oembeds
        var $all_oembed_videos = $("iframe[src*='youtube']");
        $all_oembed_videos.each(function() {
            $(this).removeAttr('height').removeAttr('width').wrap( "<div class='embed-container'></div>" );
        });

        // run match heights
        $('.matchheight').matchHeight();
        ws.matchheights();
    });
    
}(jQuery));
