(function( $ ) {
    'use strict';

    $().ready(function(){
        
        $('#log-clear').click(function(e){
            e.preventDefault();
            
            // set form value to clear
            var input = $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'clear').val('1');
            $('#logger-form').append($(input));
            
            // change action and submit
            $('#logger-form').submit();
        });
        
    });

})( jQuery );
