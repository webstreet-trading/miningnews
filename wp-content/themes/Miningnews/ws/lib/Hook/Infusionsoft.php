<?php

namespace WSApp\Hook;

use WS\Hook\Abstracts\Admin as Hook;

/**
 * Infusionsoft hook class
 * 
 * @property WS\Component\Infusionsoft $InfusionsoftComponent
 */
class Infusionsoft extends Hook {
    
    /**
	 * Add settings
     * 
     * @param array $settings Current settings
     * @return array Settings to use
	 */
	public function settings($settings) {
        
        // delete token
        if (isset($_GET['action'])) {
            
            switch ($_GET['action']) {
                case 'deauth':
                    delete_option('infusionsoft_token');
                    break;
            }
        }
        
        // setup settings
        $add_settings = array(
            array(
                'name' => __('Infusionsoft', $this->textdomain),
                'id' => 'infusionsoft',
                'type' => 'title',
            ),
            array(
                'id' => 'infusionsoft-help',
                'type' => 'view',
                'view' => 'admin/settings/infusionsoft-help',
            ),
            array(
                'name' => __('Client Key', $this->textdomain),
                'desc' => __('Application client key', $this->textdomain),
                'id' => 'is_client_id',
                'type' => 'text',
            ),
            array(
                'name' => __('Client Secret', $this->textdomain),
                'desc' => __('Application client secret', $this->textdomain),
                'id' => 'is_client_secret',
                'type' => 'text',
            ),
        );

        $is_token = get_option('infusionsoft_token');
        if (!$is_token && ws_setting('is_client_id') && ws_setting('is_client_secret')) {
            
            $auth_url = $this->api->getAuthorizationUrl();
            
            $add_settings[] = array(
                'id' => 'is_html',
                'name' => ' ',
                'type' => 'html_block',
                'html' => '<a href="' . $auth_url . '" class="button">' . __('Authorize', $this->textdomain) . '</a>',
            );
        }
        elseif ($is_token) {
            $settings_key = BusinessUnite::get_instance()->get_class('admin-settings')->key;
            $add_settings[] = array(
                'id' => 'is_html',
                'name' => ' ',
                'type' => 'html_block',
                'html' => '<a href="' . admin_url('admin.php?page=' . $settings_key . '&action=deauth') . '" class="button" onclick="confirm(\'' . __('De-authorize Infusionsoft?', $this->textdomain)  .'\')">' . __('De-authorize', $this->textdomain) . '</a>',
            );
        }
        
        // combine settings and return
        return array_merge($settings, $add_settings);
    }
    
    /**
     * Authorize handle
     */
    public function authorize_handle() {
        
        // handle infusionsoft authorize action
        if (isset($_GET['authorize']) && 'infusionsoft' == $_GET['authorize']) {
            
            $code = '';
            if (isset($_GET['code'])) {
                $code = $_GET['code'];
            }
            
            // authorize
            $this->load_component('Infusionsoft');
            $result = $this->InfusionsoftComponent->authorize($code);
            if ($result !== true) {
                $this->alert(
                    __('Authorization Failed. Please check your settings and try again.', $this->textdomain), 'error'
               );
            }
            else {
                $this->alert(
                    __('Authorization successful', $this->textdomain)
               );
            }
            
            wp_redirect(admin_url('admin.php?page=' . WS::settings_key()));
            exit;
        }
    }
}
