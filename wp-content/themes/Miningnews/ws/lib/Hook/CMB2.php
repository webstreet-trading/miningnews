<?php

namespace WS\Hook;

use WS\Hook\Abstracts\Hook;

/**
 * WS CMB2 hook class
 */
class CMB2 extends Hook {
    
    /**
     * Register scripts
     */
    public function enqueue_scripts() {
        
        // get divi map details
        // @todo fallback without divi
        if (function_exists('et_pb_get_google_api_key')) {
            wp_register_script('google-maps-api', add_query_arg(array('key' => et_pb_get_google_api_key(), 'libraries' => 'places'), is_ssl() ? 'https://maps.googleapis.com/maps/api/js' : 'http://maps.googleapis.com/maps/api/js'), array(), ET_BUILDER_VERSION, true);
        }
        
        // cmb map script
        wp_enqueue_script('cmb-map', get_stylesheet_directory_uri() . '/ws/assets/js/cmb-map.js', array('jquery', 'google-maps-api'), $this->version, true);
    }
    
    /**
     * CMB2 Post Title field render
     * 
     * @param array  $field              The passed in `CMB2_Field` object
     * @param mixed  $escaped_value      The value of this field escaped.
     *                                   It defaults to `sanitize_text_field`.
     *                                   If you need the unescaped value, you can access it
     *                                   via `$field->value()`
     * @param int    $object_id          The ID of the current object
     * @param string $object_type        The type of object you are working with.
     *                                   Most commonly, `post` (this applies to all post-types),
     *                                   but could also be `comment`, `user` or `options-page`.
     * @param object $field_type_object  This `CMB2_Types` object
     */
    public function render_post_title($field, $escaped_value, $object_id, $object_type, $field_type_object) {

        // get post title
        $title = '';
        $post = get_post($object_id);
        if (isset($_POST[ $field->args('id') ])) {
            $title = $_POST[ $field->args('id') ];
        }
        elseif ($post) {
            $title = $post->post_title;
        }

        // display
        echo $field_type_object->input(array(
            'id' => 'post_title', // should only be used once so id is same if multitple
            'value' => $title,
       ));
    }
    
    /**
     * CMB2 Post Content field render
     * 
     * @param array  $field              The passed in `CMB2_Field` object
     * @param mixed  $escaped_value      The value of this field escaped.
     *                                   It defaults to `sanitize_text_field`.
     *                                   If you need the unescaped value, you can access it
     *                                   via `$field->value()`
     * @param int    $object_id          The ID of the current object
     * @param string $object_type        The type of object you are working with.
     *                                   Most commonly, `post` (this applies to all post-types),
     *                                   but could also be `comment`, `user` or `options-page`.
     * @param object $field_type_object  This `CMB2_Types` object
     */
    public function render_post_content($field, $escaped_value, $object_id, $object_type, $field_type_object) {

        // get post content
        $content = '';
        $post = get_post($object_id);
        if (isset($_POST[ $field->args('id') ])) {
            $content = $_POST[ $field->args('id') ];
        }
        elseif ($post) {
            $content = $post->post_content;
        }

        // set field arguments
        $args = array(
            'id' => 'post_content', // should only be used once so id is same if multitple
            'value' => $content,
       );

        $type = $field->args('display_type');
        if ('wysiwyg' == $type) {
            echo $field_type_object->wysiwyg($args);
        }
        else {
            echo $field_type_object->textarea($args);
        }
    }


    /**
     * CMB2 Post Content sanitize
     *
     * @param bool|mixed $override_value Sanitization/Validation override value to return.
     *                                   Default false to skip it.
     * @param mixed      $value      The value to be saved to this field.
     * @param int        $object_id  The ID of the object where the value will be saved
     * @param array      $field_args The current field's arguments
     * @param object     $sanitizer  This `CMB2_Sanitize` object
     */
    public function sanitize_post_content($override_value, $value, $object_id, $field_args, $sanitizer) {
        // sanitize
        return $sanitizer->textarea($value);
    }

    /**
     * Render CMB HTML block
     * 
     * @param string $field
     * @param string $escaped_value
     * @param int $object_id
     * @param string $object_type
     * @param string $field_type_object
     */
    public function render_callback_for_html_block($field, $escaped_value, $object_id, $object_type, $field_type_object) {
        extract(array(
            'tag'   => (isset($field->args['tag'])) ? $field->args['tag'] : 'div',
            'class' => (isset($field->args['class'])) ? $field->args['class'] : 'cmb2-metabox-html',
            'html' => $field->args['html'],
            'desc' => $field_type_object->_desc()
       ));

        echo sprintf('<%1$s class="%2$s">%3$s</%1$s>%4$s', $tag, $class, $html, $desc);
    }

    /**
     * Render CMB View
     * 
     * @param string $field
     * @param string $escaped_value
     * @param int $object_id
     * @param string $object_type
     * @param string $field_type_object
     */
    public function render_callback_for_view($field, $escaped_value, $object_id, $object_type, $field_type_object) {
        extract(array(
            'tag' => (isset($field->args['tag'])) ? $field->args['tag'] : 'div',
            'class' => (isset($field->args['class'])) ? $field->args['class'] : 'cmb2-metabox-view',
            'view' => (isset($field->args['view'])) ? $field->args['view'] : '',
            'vars' => (isset($field->args['vars'])) ? $field->args['vars'] : array(),
            'desc' => $field_type_object->_desc()
       ));

        // if view not set, do not output anything
        if (!isset($view)) {
            return;
        }

        // setup vars
        $vars_default = compact('field', 'escaped_value', 'object_id', 'object_type', 'field_type_object');
        $vars = array_merge($vars, $vars_default);

        // get view html
        $view_content = ws_view($view, $vars);

        echo sprintf('<%1$s class="%2$s">%3$s</%1$s>%4$s', $tag, $class, $view_content, $desc);
    }
    
    /**
     * CMB2 Map field render
     * 
     * @param array  $field              The passed in `CMB2_Field` object
     * @param mixed  $escaped_value      The value of this field escaped.
     *                                   It defaults to `sanitize_text_field`.
     *                                   If you need the unescaped value, you can access it
     *                                   via `$field->value()`
     * @param int    $object_id          The ID of the current object
     * @param string $object_type        The type of object you are working with.
     *                                   Most commonly, `post` (this applies to all post-types),
     *                                   but could also be `comment`, `user` or `options-page`.
     * @param object $field_type_object  This `CMB2_Types` object
     */
    public function render_map($field, $escaped_value, $object_id, $object_type, $field_type_object) {
        
        // load map assets
		$this->enqueue_map_scripts();
        
        // field label
        $args = wp_parse_args($field->args(), array(
			'placeholder' => __('Enter your address', $this->textdomain),
			'name' => $field->_name(),
			'id' => $field->_id(),
		));
        
        // search input
        echo $field_type_object->input(array(
			'type' => 'text',
			'name' => $args['_name'] . '[address]',
			'value' => isset($escaped_value['address']) ? $escaped_value['address'] : '',
			'class' => 'cmb-map-search',
            'placeholder' => $args['placeholder'],
			'desc' => '',
		));
        
        // map container
		echo '<div class="cmb-map"></div>';
        
        // description
		$field_type_object->_desc(true, true);
        
        // map latitude and lonitude
		echo $field_type_object->input(array(
			'type' => 'hidden',
			'name' => $args['_name'] . '[latitude]',
			'value' => isset($escaped_value['latitude']) ? $escaped_value['latitude'] : '',
			'class' => 'cmb-map-latitude',
			'desc' => '',
		));
		echo $field_type_object->input(array(
			'type' => 'hidden',
			'name' => $args['_name'] . '[longitude]',
			'value' => isset($escaped_value['longitude']) ? $escaped_value['longitude'] : '',
			'class' => 'cmb-map-longitude',
			'desc' => '',
		));
	}

	/**
	 * Map field enqueue scripts
	 */
	public function enqueue_map_scripts() {
		wp_enqueue_script('cmb-map');
	}
    
    /**
     * Short-circuit saving for custom types
     *
     * @param null|bool $check  Whether to allow updating metadata for the given type.
     * @param array $args {
     *     Array of data about current field including:
     *
     *     @type string $value    The value to set
     *     @type string $type     The current object type
     *     @type int    $id       The current object ID
     *     @type string $field_id The ID of the field being updated
     *     @type bool   $repeat   Whether current field is repeatable
     *     @type bool   $single   Whether current field is a single database row
     * }
     * @param array $field_args All field arguments
     * @param CMB2_Field object $field This field object
     */
    public function override_meta_save($check, $args, $field_args, $field) {

        // check if just sanitizing data (no id)
        if (0 === $args['id']) {
            return;
        }

        // types to short-circuit
        $types = array(
            'post_title',
            'post_content',
       );
        if (in_array($field_args['type'], $types)) {
            return true;
        }
    }
    
    /**
     * CMB2 Custom field escape disable
     */
    public function types_esc_disable() {
        return '';
    }
    
    /**
     * CMB2 Disable save for fields
     */
    function sanitize_disable() {
        // never use default cbm2 metadata saving
        return false;
    }
}
