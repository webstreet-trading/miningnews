<?php

namespace WS\Hook\Abstracts;

use WS\Hook\Abstracts\Hook;

/**
 * WS System hook class
 */
abstract class System extends Hook {
    
    /**
	 * Load textdomain for translation
	 */
	public function load_textdomain() {
		load_theme_textdomain(
			$this->textdomain,
			WS_PATH . 'languages/'
		);
	}
}
