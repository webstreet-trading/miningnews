<?php

namespace WS\Hook\Abstracts;

use WS;
use WS\Core\Abstracts\ControllerModel;

/**
 * WS Hook class
 */
abstract class Hook extends ControllerModel {
    
    /**
     * Views subfolder to use
     * 
     * @var string
     */
    public $view_path = null;
    
    /**
     * Alert object
     * 
     * @var WS\Component\Alert
     */
    private $alerter = null;
    
    /**
     * Loader options
     * 
     * @var array
     */
    public $load_allowed = array('Model', 'Component');

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // set base default variable
        $this->view_path = $this->name;
        
        // add alerter
        $this->alerter = WS::get_class('Component', 'Alert', 'WS');
    }
    
    /**
     * Render view
     * 
     * @param string $view Relative view to use. If it starts with a '/' it uses full view path
     */
    public function render($view) {
        
        // set full view path
        if (substr($view, 0, 1) != '/') {
            $view = $this->view_path . '/' . $view;
        }
        
        // return view
        return ws_view($view, $this->view_vars);
    }
    
    /**
     * Add alert message
     * 
     * @param mixed $message Alert message string or WP_Error object
     * @param string $type Type of alert: success, error or info. Defaults to success
     */
    protected function alert($message, $type = 'success') {
        if (is_admin()) {
            $this->alerter->admin_add($message, $type);
        }
        else {
            $this->alerter->add($message, $type);
        }
    }
    
    /**
     * Load Model
     * 
     * @param string $model Model to load
     */
    protected function load_model($model) {
        return $this->load('Model', $model);
    }
}
