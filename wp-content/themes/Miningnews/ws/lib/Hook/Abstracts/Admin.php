<?php

namespace WS\Hook\Abstracts;

use WS;
use WS\Hook\Abstracts\Hook;

/**
 * WS Admin hook class
 */
abstract class Admin extends Hook {
    
    /**
	 * Add settings
     * 
     * @param array $settings Current settings
     * @return array Settings to use
	 */
	public function settings( $settings ) {
        
        return $settings;
    }
    
    /**
	 * Setup admin menu
	 */
    public function admin_menu() {
        
        // add sub menu logs
        $logger = WS::get_class('Component', 'Logger', 'WS');
        add_submenu_page(
            WS::settings_key(),
            __('Logs', $this->textdomain),
            __('Logs', $this->textdomain),
            'manage_options',
            $this->textdomain . '_logger',
            array($logger, 'admin_log_page')
        );
    }
    
    /**
     * Register admin meta boxes
	 * 
	 * @param array $meta_boxes Existing metaboxes
	 * @return array Array of meta boxes
     */
    public function set_meta_boxes($meta_boxes = array()) {
        global $pagenow;
        
        // set default post type
        $typenow = false;
        
        // get new post type
        if ('post-new.php' == $pagenow && isset($_REQUEST['post_type']) && post_type_exists($_REQUEST['post_type'])) {
            $typenow = $_REQUEST['post_type'];
        }
        // get editing post type
        elseif('post.php' == $pagenow) {
            if (isset($_REQUEST['post'])) {
                $typenow = get_post_type($_REQUEST['post']);
            }
            elseif (isset($_REQUEST['post_ID'])) {
                $typenow = get_post_type($_REQUEST['post_ID']);
            }
        }
        
        // if type not set, ignore
        if (!$typenow) {
            return $meta_boxes;
        }
        
        // look for models with fields
        $models = WS::directory_files(WS_APP . 'Model');
        foreach ($models as $model_class) {
            
            // get model
            $Model = WS::get_class('Model', basename($model_class, '.php'));
            
            // if post type not current type, ignore
            // if fields not avaiable, ignore
            // if metaboxes populate not set as true, ignore
            if (!$Model || $Model->get_post_type() != $typenow || empty($Model->fields) || !$Model->metaboxes_populate) {
                continue;
            }
            
            // add meta box with fields
            $meta_boxes[] = array(
                'id' => $Model->name . '_fields',
                'title' => sprintf(__('%s fields', $this->textdomain), WS::humanize($Model->name)),
                'object_types' => array($typenow),
                'context' => 'normal',
                'priority' => 'high',
                'fields' => $Model->fields,
            );
        }
        
        return $meta_boxes;
    }
}
