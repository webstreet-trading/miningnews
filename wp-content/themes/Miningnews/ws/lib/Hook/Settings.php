<?php

namespace WS\Hook;

use WS;
use WS\Hook\Abstracts\Hook;

/**
 * WS Settings hook class
 */
class Settings extends Hook {
    
	/**
 	 * Option key, and option page slug
     * 
 	 * @var string
 	 */
	protected $key = '';
    
	/**
 	 * Options page metabox id
     * 
 	 * @var string
 	 */
	protected $metabox_id = '';
    
	/**
	 * Options Page title
     * 
	 * @var string
	 */
	protected $title = '';
    
    /**
	 * Options Page subtitle
     * 
	 * @var string
	 */
	protected $subtitle = '';
    
	/**
	 * Options Page hook
     * 
	 * @var string
	 */
	protected $options_page = '';
    
	/**
	 * Constructor
	 */
	public function __construct() {
        parent::__construct();
        
        // setup key and metabox id
        $this->key = WS::settings_key();
        $this->metabox_id = 'ws_option_metabox';
        
		// set our titles
		$this->title = __('WS Settings', $this->textdomain);
        $this->subtitle = __('Settings', $this->textdomain);
	}
    
	/**
	 * Register our setting to WP
	 */
	public function init() {
		register_setting($this->key, $this->key);
	}
    
	/**
	 * Add menu options page
	 */
	public function add_options_page() {
		$this->options_page = add_menu_page($this->title, $this->title, 'manage_options', $this->key, array($this, 'admin_page_display'));
        add_submenu_page($this->key, $this->subtitle, $this->subtitle, 'manage_options', $this->key);
        
		// include CMB CSS in the head to avoid FOUC
		add_action("admin_print_styles-{$this->options_page}", array('CMB2_hookup', 'enqueue_cmb_css'));
	}
    
	/**
	 * Admin page markup. Mostly handled by CMB2
	 */
	public function admin_page_display() {
        echo ws_view('admin/settings/page_display', array('key' => $this->key, 'metabox_id' => $this->metabox_id));
	}
    
	/**
	 * Add the options metabox to the array of metaboxes
	 */
	function add_options_page_metabox() {
        
        // get available pages
        $pages = $this->get_hierarchical_pages_array();
        
		// hook in our save notices
		add_action("cmb2_save_options-page_fields_{$this->metabox_id}", array($this, 'settings_notices'), 10, 2);
        
        // setup cmb box
		$cmb = new_cmb2_box(array(
			'id' => $this->metabox_id,
			'hookup' => false,
			'cmb_styles' => false,
			'show_on' => array(
				// These are important, don't remove
				'key' => 'options-page',
				'value' => array($this->key)
			),
		));
        
        // get additional settings
        $settings = apply_filters($this->key, array());
        
        // add custom settings
        if (!empty($settings)) {
            foreach ($settings as $setting) {
                if ('page' == $setting['type']) {
                    $setting['type'] = 'select';
                    $setting['options'] = $pages;
	}
                $cmb->add_field($setting);
            }
        }
	}
    
	/**
	 * Register settings notices for display
	 *
	 * @param int $object_id Option key
	 * @param array $updated   Array of updated fields
	 * @return void
	 */
	public function settings_notices($object_id, $updated) {
		if ($object_id !== $this->key || empty($updated)) {
			return;
		}
		add_settings_error($this->key . '-notices', '', __('Settings updated.', $this->textdomain), 'updated');
		settings_errors($this->key . '-notices');
	}
    
	/**
	 * Public getter method for retrieving protected/private variables
	 * 
	 * @param string $field Field to retrieve
	 * @return mixed Field value or exception is thrown
	 */
	public function __get($field) {
		// Allowed fields to retrieve
		if (in_array($field, array('key', 'metabox_id', 'title', 'options_page'), true)) {
			return $this->{$field};
		}
		throw new Exception('Invalid property: ' . $field);
	}
    
    /**
     * Retrieves a hierarchical array of pages
     *
     * @param array|string $args Optional. Override default arguments.
     * @return array Array of pages
     */
    protected function get_hierarchical_pages_array($args = '') {
        
        $defaults = array(
            'depth' => 0, 'child_of' => 0,
            'selected' => 0, 'echo' => 1,
            'name' => 'page_id', 'id' => '',
            'show_option_none' => '', 'show_option_no_change' => '',
            'option_none_value' => ''
       );
        $pages = get_pages(wp_parse_args($args, $defaults));

		if (empty($pages)) //nothing to go through
			return $pages;

		$parent_field = 'post_parent';

		/*
		 * Need to display in hierarchical order.
		 * Separate elements into two buckets: top level and children elements.
		 * Children_elements is two dimensional array, eg.
		 * Children_elements[10][] contains all sub-elements whose parent is 10.
		 */
		$top_level_pages = array();
		$children_pages  = array();
		foreach ($pages as $page) {
			if (0 == $page->$parent_field)
				$top_level_pages[] = $page;
			else
				$children_pages[ $page->$parent_field ][] = $page;
		}

		/*
		 * When none of the elements is top level.
		 * Assume the first one must be root of the sub elements.
		 */
		if (empty($top_level_pages)) {

			$first = array_slice($pages, 0, 1);
			$root = $first[0];

			$top_level_pages = array();
			$children_pages  = array();
			foreach ($pages as $page) {
				if ($root->$parent_field == $page->$parent_field)
					$top_level_pages[] = $page;
				else
					$children_pages[ $page->$parent_field ][] = $page;
			}
		}
        
        $hierarchical_pages = array();
		foreach ($top_level_pages as $page) {
			$this->hierarchical_page_add($page, $children_pages, 0, $hierarchical_pages);
        }

		/*
		 * If we are displaying all levels, and remaining children_elements is not empty,
		 * then we got orphans, which should be displayed regardless.
		 */
		if (count($children_pages) > 0) {
			$empty_array = array();
			foreach ($children_pages as $orphans) {
				foreach($orphans as $op) {
					$this->hierarchical_page_add($op, $empty_array, 0, $hierarchical_pages);
                }
            }
        }
        
        return $hierarchical_pages;
    }
    
    /**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max
	 * depth and no ignore elements under that depth. It is possible to set the
	 * max depth to include all depths, see walk() method.
	 *
	 * This method should not be called directly, use the walk() method instead.
	 *
	 * @param object $page Data object.
	 * @param array $children_pages List of elements to continue traversing.
	 * @param int $depth Depth of current element.
     * @param string $hierarchical_pages Passed by reference. Used to append additional content.
	 */
    protected function hierarchical_page_add($page, &$children_pages, $depth, &$hierarchical_pages) {
        
        if (!$page)
			return;
        
        $spacer = str_repeat('-', $depth * 2);
        $hierarchical_pages[ $page->ID ] = $spacer . $page->post_title;

		// descend only when there are childrens for this element
		if (isset($children_pages[ $page->ID ])) {

			foreach($children_pages[ $page->ID ] as $child) {
				$this->hierarchical_page_add($child, $children_pages, $depth + 1, $hierarchical_pages);
			}
			unset($children_pages[ $page->ID ]);
		}
        
    }
    
    /**
     * Fires after all fields have been saved.
     *
     * @param int $object_id The ID of the current object
     * @param array $cmb_id The current box ID
     * @param string $updated Array of field ids that were updated. Will only include field ids that had values change.
     * @param array $cmb This CMB2 object
     */
    public function after_save($object_id, $cmb_id, $updated, $cmb) {
        
        // ensure rewrite rules are flushed
        // @todo improve how this is done
        add_option($this->textdomain . '_trigger_flush_rewrite_rules', true);
    }
}