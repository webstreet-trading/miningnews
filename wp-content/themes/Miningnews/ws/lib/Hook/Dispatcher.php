<?php

namespace WS\Hook;

use WS;
use WsTemplate;
use WS\Hook\Abstracts\Hook;

/**
 * WS Dispatcher hook class
 */
class Dispatcher extends Hook {
    
    /**
     * Controller Internal functions
     * These functions are public but are not allowed during dispatch
     * 
     * @var array
     */
    protected $controller_internal_functions = array('dispatch', 'render');
    
    /**
     * Add query vars for posts filter
     * 
     * @param array $qv
     * @return array
     */
    public function query_vars($qv) {
        $qv[] = 'ws-controller';
        $qv[] = 'ws-params';
        return $qv;
    }
    
    /**
     * Add rewrite rules custom pages
     * 
     * @param array $rules Current rules
     * @return array New rules
     */
    public function rewrite_rules_array($rules) {
        
        $rewrite_paths = array();
        
        $controllers = WS::directory_files(WS_APP . 'Controller');
        foreach ($controllers as $controller_class) {
            
            // filter out invalid controllers
            if (substr($controller_class, -4, 4) != '.php') {
                continue;
            }
            
            // get controller
            $Controller = WS::get_class('Controller', basename($controller_class, '.php'));
            if (!$Controller) {
                continue;
            }
            
            // check rewrite pages
            $class_methods = get_class_methods($Controller);
            foreach ($class_methods as $method) {
                // ignore dispather, render and hidden methods
                if (!in_array($method, $this->controller_internal_functions) && substr($method, 0, 1) !== '_') {
                    $rewrite_paths[] = $Controller->name;
                    break;
                }
            }
        }
        
        // setup new rules based on rewrite paths
        $newrules = array();
        foreach ($rewrite_paths as $path) {
            $baserule = $path.'/';
            $basepath = 'index.php?ws-controller=' . $path;
            $newrules[$baserule . '(.+)$'] = $basepath . '&ws-params=$matches[1]';
            $newrules[$baserule . '?$'] = $basepath;
        }
        
        return $newrules + $rules;
    }
    
    /**
     * Filter the path of the current template before including it
     * 
     * @param string $template The path of the template to include
     * @return string Path of template to include
     */
    public function controller_page_template($template) {
        
        // check if controller page called
        if (get_query_var('ws-controller')) {
            
            // set controller class
            $Controller = WS::get_class('Controller', WS::camelize(get_query_var('ws-controller')));
            
            // check if class exists
            if (!$Controller) {
                return $template;
            }
            
            // get params
            $params = array();
            if (get_query_var('ws-params')) {
                $params = explode('/', get_query_var('ws-params'));
            }
            
            // extract method from params
            $method = 'index';
            if (!empty($params)) {
                $method = $params[0];
                array_shift($params);
            }
            
            // dispatch
            $result = $this->dispatch($Controller, $method, $params);
            if ($result) {
                // setup template
                WsTemplate::setup($result['title'], $result['content'], $result['args']);
                
                // return template to use
                return $result['template'];
            }
            // dispatch failed
            else {
                global $wp_query;
                $wp_query->set_404();
                status_header(404);
                
                return get_404_template();
            }
        }
        
        return $template;
    }
    
    /**
     * Handle ajax request
     */
    public function ajax() {
        
        // set args
        $args = wp_parse_args($_POST, array(
            'controller' => false,
            'method' => 'index',
            'params' => array(),
            'nonce' => false,
        ));
        
        // if valid, continue
        if ($args['controller'] && $args['method']) {
            
            // set controller
            $Controller = WS::get_class('Controller', WS::camelize($args['controller']));
            
            // check if class exists
            if (!$Controller) {
                wp_send_json_error();
            }
            
            // get params
            $params = maybe_unserialize($args['params']);
            $params = ws_maybe_json_decode($args['params']);
            
            // default method
            if ($args['method']) {
                $method = 'ajax_' . $args['method'];
            }
            else {
                $method = 'ajax_index';
            }
            
            // dispatch
            $result = $this->dispatch($Controller, $method, $params);
            if (!is_wp_error($result)) {
                wp_send_json_success($result);
            }
            // dispatch failed
            else {
                wp_send_json_error($result);
            }
        }
    }
    
    /**
     * Dispatch a controller method with params
     * 
     * @param WS\Controller\Abstracts\Controller $Controller Controller to use
     * @param array $method Controller method to call
     * @param array $params Controller parameters
     * @return mixed Return array of dispatch result or false on failure
     *                  array(title=>TITLE,content=>CONTENT,args=>ARGS)
     */
    public function dispatch($Controller, $method, $params = array()) {
        
        // format method text to function name format
        $method = WS::underscore($method);
        
        // check not method is allowed
        if (in_array($method, $this->controller_internal_functions)) {
            return false;
        }
        
        // dispatch controller method
        // @todo: implement try catch when controller/method does not exist
        $result = $Controller->dispatch($method, $params);
        if (is_wp_error($result)) {
            return false;
        }
        
        // page template
        $page_template = $Controller->page_template;
        
        // page title
        $page_title = $Controller->page_title;
        
        // page args
        $page_args = $Controller->page_args;
        $page_args['controller'] = $Controller->name;
        
        // auto render
        if (!$result && $Controller->auto_render) {
            $content = $Controller->render();
        }
        else {
            $content = $result;
        }
        
        return array(
            'template' => $page_template,
            'title' => $page_title,
            'content' => $content,
            'args' => $page_args,
        );
    }
}
