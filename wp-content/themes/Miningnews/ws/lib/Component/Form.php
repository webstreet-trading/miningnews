<?php

namespace WS\Component;

use WS;
use WS\Core\Abstracts\Base;
use WP_Error;

/**
 * WS Form class
 * 
 * Uses CMB2 form
 */
class Form extends Base {
    
    /**
     * CMB2 Config data
     * 
     * @var array
     */
    protected $mb_config = array();
    
    /**
     * Fields array that include validation data
     * 
     * @var array
     */
    protected $fields = array();
    
    /**
     * CMB2 formatted fields
     * 
     * @var array
     */
    protected $cmb2_fields = array();
    
    /**
     * Object ID
     * 
     * @var mixed 
     */
    protected $object_id;
    
    /**
     * Object type
     * 
     * @var string
     */
    protected $object_type;
    
    /**
     * Form Setup
     * 
     * @param string $mb_id Metabox id
     * @param string $object_type Object type
     * @param array $fields Metabox fields
     * @param mixed $object_id Object id
     * @param array $settings Metabox settings
     */
    public function setup($mb_id, $object_type, $fields, $object_id = null, $settings = array()) {
        
        // ensure object id set, defaults to fake id
        if (!$object_id) {
            $object_id = 'fake-object-id';
        }
        
        // setup metabox config
        $mb_config = wp_parse_args($settings, array(
            'show_names' => true,
            'cmb_styles' => false,
            'hookup' => false,
            'show_names'=> true,
            'show_on_cb' => function($cmb) { return (!is_admin()); /* only show if frontend */ },
            'cmb_styles' => false,
            'enqueue_js' => true,
            'hookup' => true,
            'save_fields' => false,
            'defaults' => array(),
       ));
        
        // set ids
        $this->object_id = $object_id;
        $mb_config['id'] = $mb_id;
        
        // set fields
        $this->fields = $fields;
        
        // set object type (front end should only be one object type)
        if (is_array($object_type)) {
            $this->object_type = reset($object_type);
        }
        else {
            $this->object_type = $object_type;
        }
        $mb_config['object_types'] = array($this->object_type);
        
        // set config
        $this->mb_config = $mb_config;
        
        // format fields to cmb2 format
        $this->format_cmb2_fields();
        
        // setup metabox
        $this->setup_cmb2_box();
    }
    
    /**
     * Set object id
     * 
     * @param int $object_id
     */
    public function set_object_id($object_id) {
        $this->object_id = $object_id;
    }
    
    /**
     * Setup CMB2 Frontend boxes
     */
    public function setup_cmb2_box() {
        
        // setup box
        $cmb_box = new_cmb2_box($this->mb_config);
        
        // add fields
        foreach ($this->cmb2_fields as $field) {
            $cmb_box->add_field($field);
        }
    }
    
    /**
     * Format fields to cmb2 format
     */
    public function format_cmb2_fields() {
        
        // @todo test to see if it works correctly with repeatable groups
        foreach ($this->fields as $id => $field) {
            $formatted_field = $field;
            
            // if id not set, use array key
            if (!isset($formatted_field['id'])) {
                $formatted_field['id'] = $id;
            }
            
            // if placeholder set, put in attributes
            if (isset($formatted_field['placeholder'])) {
                if (!isset($formatted_field['attributes'])) {
                    $formatted_field['attributes'] = array();
                }
                $formatted_field['attributes']['placeholder'] = $formatted_field['placeholder'];
            }
            
            // if name not set, humanize id
            if (!isset($field['name'])) {
                $formatted_field['name'] = WS::humanize($formatted_field['id']);
            }
            
            // validations
            if (isset($field['validate'])) {
                $formatted_field = $this->format_cmb2_field_validation($field, $formatted_field);
            }
            
            // for date use international format by default
            if (in_array($field['type'], array('text_date', 'text_date_timestamp', 'text_datetime_timestamp', 'text_datetime_timestamp_timezone'))) {
                $formatted_field['date_format'] = 'Y-m-d';
            }
            
            // add error classes and messages
            // @todo
            
            $this->cmb2_fields[] = $formatted_field;
        }
    }
    
    /**
     * Format field validation to cmb2 format
     */
    protected function format_cmb2_field_validation($field, $formatted_field) {
        
        if (!is_array($field['validate'])) {
            $field['validate'] = array($field['validate']);
        }
        foreach ($field['validate'] as $validate => $setting) {

            // used for shorthand rules like required, numeric, etc
            if (is_numeric($validate)) {
                $validate = $setting;
            }
            
            // attributes based on validation
            switch ($validate) {
                case 'required':
                    $formatted_field['attributes']['required'] = 'required';
                    break;
                case 'numeric':
                    $formatted_field['attributes']['numeric'] = 'numeric';
                    break;
                case 'minlength':
                    $formatted_field['attributes']['minlength'] = $setting;
                    break;
                case 'maxlength':
                    $formatted_field['attributes']['maxlength'] = $setting;
                    break;
                case 'terms_agree':
                    $formatted_field['attributes']['terms_agree'] = 'terms_agree';
                    break;
            }
        }
        
        return $formatted_field;
    }
    
    /**
     * Display form
     * 
     * @param array $args Optional CMB2 form arguments array
     */
    public function display($args = array()) {
        
        // only add this filter on display to set field values during display, it causes issues if it runs when form is processed
        add_filter('cmb2_override_meta_value', array($this, 'remember_post_data'), 10, 4);
        
        $args = wp_parse_args($args, array(
            'action' => false,
            'save_button' => __('Save', $this->textdomain),
            'form_format' => '<form class="cmb-form" method="post" id="%1$s" enctype="multipart/form-data" encoding="multipart/form-data"><input type="hidden" name="object_id" value="%2$s">%3$s<input type="submit" name="submit-cmb" value="%4$s" class="button button-submit"></form>',
        ));
        
        // set action
        if ($args['action']) {
            $args['form_format'] = '<form action="'.$args['action'].'" class="cmb-form" method="post" id="%1$s" enctype="multipart/form-data" encoding="multipart/form-data"><input type="hidden" name="object_id" value="%2$s">%3$s<input type="submit" name="submit-cmb" value="%4$s" class="button button-submit"></form>';
        }
        unset($args['action']);
        
        // get metabox
        $cmb = cmb2_get_metabox($this->mb_config['id'], $this->object_id);
        
        // return form
        return cmb2_get_metabox_form($cmb, $this->object_id, $args);
    }
    
    /**
     * Remember posted data
     * 
     * Filter whether to override getting of meta value.
     * Returning a non 'cmb2_field_no_override_val' value
     * will effectively short-circuit the value retrieval.
     * 
     * Add ability to get value from post data
     *
     * @param mixed $value     The value get_metadata() should
     *                         return - a single metadata value,
     *                         or an array of values.
     *
     * @param int   $object_id Object ID.
     *
     * @param array $args {
     *     An array of arguments for retrieving data
     *
     *     @type string $type     The current object type
     *     @type int    $id       The current object ID
     *     @type string $field_id The ID of the field being requested
     *     @type bool   $repeat   Whether current field is repeatable
     *     @type bool   $single   Whether current field is a single database row
     * }
     *
     * @param CMB2_Field object $field This field object
     */
    public function remember_post_data($value, $object_id, $field_args, $field) {
    
        // get submitted values
        if (isset($_POST[ $field->args('id') ])) {
            $value = $_POST[ $field->args('id') ];
        }
        // get global defaults
        elseif (isset($this->mb_config['defaults'][ $field->args('id') ])) {
            $value = $this->mb_config['defaults'][ $field->args('id') ];
        }

        return $value;
    }
    
    /**
     * Process submitted metabox
     * 
     * @return boolean Return true on success, WP_Error object on error
     */
    public function process() {
        
        // check if form submitted
        if (empty($_POST) || ! isset($_POST['submit-cmb'], $_POST['object_id'])) {
            return false;
        }
        
        // get metabox
        $cmb = cmb2_get_metabox($this->mb_config['id'], $this->object_id);
        
        // check security nonce
        if (! isset($_POST[ $cmb->nonce() ]) || ! wp_verify_nonce($_POST[ $cmb->nonce() ], $cmb->nonce())
                && $_POST['object_id'] == $this->object_id) {
            return new WP_Error('form-security', __('Security check failed', $this->textdomain));
        }
        
        // run validation errors
        $errors = $this->validations();
        if ($errors->get_error_codes()) {
            return $errors;
        }
        
        return true;
    }
    
    /**
     * Process submitted metabox
     * 
     * @param array $save_fields Fields to save that is not handled by default like pending status
     * @return boolean Return Object id on success, false on failure
     */
    public function save($save_fields = array()) {
        
        $result = $this->process();
        if (is_wp_error($result) || $result !== true) {
            return $result;
        }
        
        // get metabox
        $cmb = cmb2_get_metabox($this->mb_config['id'], $this->object_id);
        
        // fetch sanitized values
        $sanitized_values = $cmb->get_sanitized_values($_POST);
        
        // screw it, sanitize content_post here since it seems to be stripped out currently
        if (isset($_POST['post_content'])) {
            $block = join("|",array("make_columns", "make_row"));
            $rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]", $_POST['post_content']);
            $sanitized_values['post_content'] = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]", $rep);
        }
        
        // get fields that will not be saved by cmb2
        $save_fields['post_title'] = $sanitized_values['post_title'];
        $save_fields['post_name'] = wp_strip_all_tags($sanitized_values['post_title']);
        $save_fields['post_content'] = (isset($sanitized_values['post_content'])? $sanitized_values['post_content'] : ' ');
        
        // check if new or existing object
        if ('fake-object-id' == $this->object_id) {
            
            // set data
            $post_data = array(
                'post_author' => get_current_user_id(),
                'post_type' => $this->object_type,
                // @todo: implement greater control over new post status
                'post_status' => 'publish',
            );
            
            // merge with save fields
            $post_data = array_merge($post_data, $save_fields);
            
            // create the post
            $post_id = wp_insert_post($post_data, true);
            if (is_wp_error($post_id)) {
                return new WP_Error('form-save-fail', __('Error saving. Please try again', $this->textdomain));
            }
            $this->object_id = $post_id;
        }
        else {
            
            // save existing fields
            $post_data = $save_fields;
            $post_data['ID'] = $this->object_id;
            
            wp_update_post($post_data);
        }
        
        // save remaining fields
        $cmb->save_fields($this->object_id, 'post', $sanitized_values); // only hanle post types for now
        
        return $this->object_id;
    }
    
    /**
     * Run validations on metabox
     * 
     * @since 1.0.0
     * @return WP_Error
     */
    protected function validations() {
        
        $errors = new WP_Error();
        
        // only run required and filter validations for now
        // @todo: implement further validations
        
        // validate required
        foreach ($this->cmb2_fields as $field) {
            
            // always validate post_title
            if ('post_title' == $field['type'] && (!isset($_POST[ $field['id'] ]) || !$_POST[ $field['id'] ])) {
                $errors->add('post_title', sprintf(__('%s needs to be filled in', $this->textdomain), $field['name']));
            }
            
            if (isset($field['validate'])) {
                // check if single rule shorthand used
                if (!is_array($field['validate'])) {
                    $field['validate'] = array($field['validate']);
                }
                
                // go through all validation rules
                foreach ($field['validate'] as $rule => $setting) {
                    
                    // get rule and it's values
                    if (is_int($rule)) {
                        $rule = $setting;
                    }
                    
                    // if not set but not required
                    if ((!isset($_POST[ $field['id'] ]) || !$_POST[ $field['id'] ]) && 'required' != $rule) {
                        continue;
                    }
                    
                    // get value
                    if (isset($_POST[ $field['id'] ])) {
                        $value = $_POST[ $field['id'] ];
                    }
                    else {
                        $value = '';
                    }
                    
                    // handle rules
                    switch($rule) {
                        case 'required':
                            // ignore post_title since it's checked earlier
                            if ('post_title' == $field['type']) {
                                continue;
                            }
                            if (!isset($value) || !trim($value)) {
                                $errors->add('required', sprintf(__('%s is a required field', $this->textdomain), $field['name']));
                            }
                            break;
                        
                        case 'email':
                            if (!is_email($value)) {
                                $errors->add('invalid-email', sprintf(__('%s needs a valid email address', $this->textdomain), $field['name']));
                            }
                            break;
                        case 'numeric':
                            if (!preg_match('/^[0-9]+$/', $value)) {
                                $errors->add('invalid-numeric', sprintf(__('%s needs a valid numeric value', $this->textdomain), $field['name']));
                            }
                            break;
                        case 'minlength':
                            if (strlen(trim($value)) < $setting) {
                                $errors->add('invalid-minlength', sprintf(__('%s needs to be at least %s characters', $this->textdomain), $field['name'], $setting));
                            }
                            break;
                        case 'maxlength':
                            if (strlen(trim($value)) > $setting) {
                                $errors->add('invalid-maxlength', sprintf(__('%s needs to be no more than %s characters', $this->textdomain), $field['name'], $setting));
                            }
                            break;
                        case 'terms_agree':
                            if (!isset($value) || !trim($value)) {
                                $errors->add('invalid-terms_agree', sprintf(__('Please agree with terms', $this->textdomain), $field['name'], $setting));
                            }
                            break;
                    }
                }
            }
        }
        
        /**
		 * Run custom validations
		 *
		 * @param WP_Error $errors Validation errors
         * @param int $metabox_id Metabox id being validated
		 */
        $errors = apply_filters($this->textdomain . '_form_validation', $errors, $this->mb_config['id']);
        
        return $errors;
    }
    
    /**
     * Get current object id
     * 
     * @return mixed Object id
     */
    public function get_object_id() {
        return $this->object_id;
    }
}
