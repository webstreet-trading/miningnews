<?php

namespace WS\Component;

use WS\Core\Abstracts\Base;

/**
 * WS Email Class
 */
class Email extends Base {
    
    /**
     * Send an email
     * 
     * @param string|array $to Array or comma-separated list of email addresses to send message
     * @param string $subject Email subject
     * @param string $message Message contents
     * @param string|array $headers Optional. Additional headers
     * @param string|array $attachments Optional. Files to attach
     * @return bool Whether the email contents were sent successfully
     */
    public function send($to, $subject, $message, $headers = array(), $attachments = array()) {
        
		add_filter('wp_mail_content_type', array($this, 'get_content_type'));
        
		$return  = wp_mail($to, $subject, $message, $headers, $attachments);

		remove_filter('wp_mail_content_type', array($this, 'get_content_type'));

		return $return;
    }
    
    /**
	 * Get email content type.
	 *
	 * @return string
	 */
	public function get_content_type(){
		return 'text/html';
	}
}
