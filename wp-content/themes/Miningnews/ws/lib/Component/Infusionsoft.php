<?php

namespace WS\Component;

use WS;
use WS\Core\Abstracts\Base;

/**
 * WS Infusionsoft Class
 */
class Infusionsoft extends Base {
    
    /**
     * API infusionsoft object
     * 
     * @var \Infusionsoft\Infusionsoft 
     */
    protected $api = null;
    
    /**
     * Token option key
     * 
     * @var string
     */
    protected $token_option_key = 'infusionsoft_token';
    
    /**
     * Token object
     * 
     * @var Infusionsoft\Token
     */
    protected $token_object = null;
    
    /**
     * Logger class
     * 
     * @var WS\Component\Logger
     */
    protected $logger = null;
    
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // logger
        $this->logger = Logger::instance();
        
        // infusionsoft api
        $client_id = ws_setting('is_client_id');
        $client_secret = ws_setting('is_client_secret');
        if ($client_id && $client_secret) {
            $this->api = new \Infusionsoft\Infusionsoft(array(
                'clientId' => $client_id,
                'clientSecret' => $client_secret,
                'redirectUri' => admin_url('?authorize=infusionsoft'),
           ));
            
            // set token
            $this->set_token();
        }
    }
    
    /**
     * Make a log entry
     * 
     * @param string $message Log message
     * @param mixed $data Log data
     * @return boolean Whether the log was made successfully
     */
    public function log($message, $data = false) {
        return $this->logger->add('infusionsoft', $message, $data);
    }
    
    /**
     * Authorize API
     * 
     * @param string $code Authorization code
     * @return mixed Return true (boolean) on success or error message
     */
    public function authorize($code) {
        
        try {
        
            // authorize
            if ($code && !$this->api->getToken()) {
                $this->api->requestAccessToken($code);
            }
            if ($this->api->getToken()) {
                $this->save_token($this->api->getToken());
            }
            
            return true;
            
        }
        catch (Exception $e) {
            $this->log(__('Authorization failed', $this->textdomain), $e->getMessage());
        }
        return false;
    }
    
    /**
     * Get token
     */
    protected function get_token() {
        if (!$this->token_object) {
            $this->token_object = get_option($this->token_option_key);
        }
        return $this->token_object;
    }
    
    /**
     * Save token
     * 
     * @param Infusionsoft\Token $token
     */
    protected function save_token($token) {
        update_option($this->token_option_key, $token);
        $this->token_object = $token;
    }
    
    /**
     * Set token
     */
    protected function set_token() {
        if ($this->get_token()) {
            $this->api->setToken($this->get_token());
        }
    }
    
    /**
     * Check if token needs to be refreshed, if so refresh token
     */
    protected function check_token_expire() {
        if ($this->get_token()) {
            $token_expire = $this->get_token()->endOfLife;
            
            // get current time (keeping mind infusionsoft timezone)
            $datetime = new \DateTime('now', new \DateTimeZone('America/New_York'));
            $current_time = $datetime->format('U');
            
            // check time expired
            if ($token_expire < $current_time) {
                // if expired, refresh token
                $this->save_token($this->api->refreshAccessToken());
                $this->set_token();
            }
        }
        else {
            $this->log(__('Token Error: Token not set.', $this->textdomain));
        }
    }
    
    /**
     * API Calls
     * 
     * @param string $service Infusionsoft API Service name
     * @param string $method Method to call on service
     * @param array $args Parameters for method call
     * @return mixed
     */
    public function api_call($service, $method, $args = array()) {
        
        if (method_exists($this->api, $service)) {
            
            try {
                
                // only run if authorized
                if ($this->get_token()) {
                
                    // do token check
                    $this->check_token_expire();

                    // make call
                    return call_user_func_array(array($this->api->$service, $method), $args);
                }
                else {
                    $this->log(__('Tried to make API with unauthorized token', $this->textdomain));
                }
            }
            catch (\Exception $e) {
                $log_data = array(
                    'params' => array(
                        'service' => $service,
                        'method' => $method,
                        'args' => $args,
                   ),
                    'exception' => print_r($e->getMessage(), true),
               );
                $this->log(__('API call error', $this->textdomain), $log_data);
            }
        }
        return false;
    }
}