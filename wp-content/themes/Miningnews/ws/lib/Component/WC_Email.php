<?php

namespace WS\Component;

use WC_Email as Parent_WC_Email;
use WC_Emails;

if (!class_exists('woocommerce')) {
    return;
}
    
require_once(WP_PLUGIN_DIR . '/woocommerce/includes/emails/class-wc-email.php');

/**
 * WS Generic WooCommerce mailer class
 */
class WC_Email extends Parent_WC_Email {

	/**
	 * Constructor
	 *
	 * @return void
	 */
	public function __construct() {
        
        // since this custom class is sometimes called before complete woocommerce email setup,
        // ensure that WC_Emails class instance has been setup
        WC_Emails::instance();

		$this->id = 'ws-mail';
		$this->template_html = 'emails/ws_email.php';
		$this->template_plain = 'emails/plain/ws_email.php';

		// Call parent constructor
		parent::__construct();
	}

	/**
	 * Send email
	 *
	 * @return void
	 */
	public function send($recipient, $subject, $content, $headers = array(), $attachments = array()) {
        
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->content = $content;
        $this->attachments = $attachments;
        
        // use normal content to setup plain content
        $doc = new \DOMDocument();
        @$doc->loadHTML($content);
        $links = $doc->getElementsByTagName('a');
        foreach ($links as $link) {
            $href = $link->getAttribute('href');
            $text = $link->nodeValue;
            $text_link = $doc->createElement('span', $text . ' (' . $href . ') ');
            $link->parentNode->replaceChild($text_link, $link);
        }
        $body = $doc->getElementsByTagName('body');
        $this->plain_content = $body[0]->nodeValue;
        
        // use subject in heading
		$this->heading = $subject;
        
        // get headers
        if (empty($headers)) {
            $headers = $this->get_headers();
        }
        
        // send email
        return parent::send($this->get_recipient(), $this->get_subject(), $this->get_content(), $headers, $this->get_attachments());
	}

	/**
	 * get_content_html function.
	 *
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html($this->template_html, array(
			'content' 		=> $this->content,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => false
		));
	}

	/**
	 * get_content_plain function.
	 *
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html($this->template_plain, array(
			'content' 		=> $this->plain_content,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => true
		));
	}
}
