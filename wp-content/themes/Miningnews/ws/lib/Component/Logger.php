<?php

namespace WS\Component;

use WS\Core\Abstracts\Base;

/**
 * WS Logger class
 */
class Logger extends Base {
    
    /**
	 * Instance of this class.
     * 
	 * @var object
	 */
	protected static $instance = null;
    
    /**
     * Log directory
     * 
     * @var string
     */
    protected $log_dir;
    
    /**
     * Setup has been run status
     * 
     * @var boolean
     */
    protected $setup_ran = false;

    /**
     * Stores open file pointer resources
     * 
     * @var array
     */
    protected $fhandles;

    /**
     * Constructor
	 */
	public function __construct() {
        parent::__construct();
        
        // set upload directory
        $upload_dir = wp_upload_dir();
        $this->log_dir = $upload_dir['basedir'] . '/ws-logs/';
        
        // setup logs directory
        $this->setup();
        
        // setup file handlers array
        $this->fhandles = array();
    }
    
    /**
	 * Returns the running object
     * 
	 * @return WS\Component\Logger
	 **/
	public static function instance() {
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
    
    /**
     * Create logs directory if required
     */
    public function setup() {
        
        // if already setup, do nothing
        if ($this->setup_ran) {
            return;
        }
        
        // ensure directory and security htaccess file exits, if not create them
        if (wp_mkdir_p($this->log_dir) && ! file_exists(trailingslashit($this->log_dir) . '.htaccess')) {
            $file_handle = @fopen(trailingslashit($this->log_dir) . '.htaccess', 'w');
            if ($file_handle) {
                fwrite($file_handle, 'deny from all');
                fclose($file_handle);
            }
        }
    }
    
    /**
     * Get a log file path
     * 
     * @param string $file File name
     * @return string Log file path
     */
    protected function get_file_path($file) {
        return trailingslashit($this->log_dir) . sanitize_file_name($file) . '.log';
    }

    /**
     * Open log file for writing
     *
     * @param string $file
     * @param string $mode
     * @return bool
     */
    protected function open($file, $mode = 'a') {
        if (isset($this->fhandles[$file])) {
            return true;
        }

        $this->fhandles[$file] = @fopen($this->get_file_path($file), $mode);
        if ($this->fhandles[$file]) {
            return true;
        }

        return false;
    }

    /**
     * Close a file pointer resource
     *
     * @param string $file
     * @return bool
     */
    protected function close($file) {
        $result = false;

        if (isset($this->fhandles[$file]) && is_resource($this->fhandles[$file])) {
            $result = fclose($this->fhandles[$file]);
            unset($this->fhandles[$file]);
        }

        return $result;
    }

    /**
     * Add a log entry to file
     *
     * @param string $file
     * @param string $message
     * @param string $data
     * @return bool
     */
    public function add($file, $message, $data = false) {
        $result = false;

        if ($this->open($file) && is_resource($this->fhandles[$file])) {
            $time = date_i18n('Y-m-d H:i:s -');
            $string = $time . " " . $message . "\n";
            if ($data) {
                $string .= 'DATA: ' . print_r($data, true) . "\n";
            }
            $string .= "\n";
            $result = fwrite($this->fhandles[$file], $string);
        }

        return false !== $result;
    }

    /**
     * Clear entries from chosen file
     *
     * @param string $file
     * @return bool
     */
    public function clear($file) {
        $result = false;

        // Close the file if it's already open.
        $this->close($file);

        //  Place file pointer at the beginning of the file
        if ($this->open($file, 'w') && is_resource($this->fhandles[$file])) {
            $result = true;
        }

        return $result;
    }
    
    /**
     * Destructor
     * 
     * Close all still opened files
     */
    public function __destruct() {
        foreach ($this->fhandles as $file => $resource) {
            $this->close($file);
        }
    }
    
    /**
     * Admin log page
     */
    public function admin_log_page() {
        
        $notices = array();
        $logs = $this->get_logs_files();

        // get current view
		if (!empty($_POST['view']) && array_key_exists(sanitize_title($_POST['view']), $logs)) {
			$view_log = sanitize_title($_POST['view']);
		}
        elseif (! empty($logs)) {
			$view_log = key($logs);
		}
        
        // clear log if requested (clear only on matched logs)
        if (!empty($_POST['clear']) && array_key_exists(sanitize_title($_POST['view']), $logs)) {
            if ($this->clear($view_log)) {
                $notices[] = array(
                    'type' => 'success',
                    'message' => __('Log has been successfully cleared.', $this->textdomain),
               );
            }
            else {
                $notices[] = array(
                    'type' => 'error',
                    'message' => __('Unable to clear log. Please try again.', $this->textdomain),
               );
            }
        }

		echo ws_view('admin/logs', compact('logs', 'view_log', 'notices'));
    }
    
    /**
	 * Get content for a log file
     * 
	 * @return string Log file content
	 */
    public function get_log_content($file) {
        return file_get_contents($this->get_file_path($file));
    }
    
    /**
	 * Scan the log files
     * 
	 * @return array Return array of log files
	 */
	private function get_logs_files() {
		$files  = @scandir($this->log_dir);
		$log_files = array();

        // scan files to get valid log files
		if ($files) {
			foreach ($files as $k => $value) {
				if (!in_array($value, array('.', '..'))) {
					if (!is_dir($value) && strstr($value, '.log')) {
                        $log_name = substr($value, 0, -4);
						$log_files[ $log_name ] = $value;
					}
				}
			}
		}

		return $log_files;
	}

}
