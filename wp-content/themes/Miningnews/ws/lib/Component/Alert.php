<?php

namespace WS\Component;

use WS\Core\Abstracts\Base;

/**
 * WS Alert Class
 */
class Alert extends Base {
    
    /**
     * Session data
     * 
     * @var array
     */
    protected $session;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // check if WP_Session plugin available, otherwise use normal sessions
        if (class_exists('WP_Session')) {
            $this->session = \WP_Session::get_instance();
        }
        else {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            $this->session =& $_SESSION;
        }
        
        // ensure empty value is populated
        if (!isset($this->session['ws_alerts'])) {
            $this->session['ws_alerts'] = array();
        }
    }
    
    /**
     * Add alert message
     * 
     * @param mixed $message Alert message string or WP_Error object
     * @param string $type Type of alert: success, error or info. Defaults to success
     */
    public function add($message, $type = 'success') {
        
        // check if wp error received
        if (is_wp_error($message) && $message->get_error_codes()) {
            $message = $this->wp_error_message($message);
        }
        
        // check if alert overwritten
        $override = apply_filters('ws_alert_add_override', false, $message, $type);
        if ($override) {
            return;
        }

        // add alert message
        $this->session['ws_alerts'][] = array(
            'message' => $message,
            'type' => $type,
        );
    }

    /**
     * Display alert messages
     */
    public function display() {
        
        // check if alert display overwritten
        $override = apply_filters('ws_alert_display_override', false);
        if ($override) {
            return;
        }
        
        // display alerts
        foreach ($this->session['ws_alerts'] as $k => $alert) {
            echo ws_view('alert', $alert);
            unset($this->session['ws_alerts'][$k]);
        }
    }
    
    /**
     * Add admin alert
     * 
     * @param mixed $message Alert message string or WP_Error object
     * @param string $type Type of alert: success, error or info. Defaults to success
     */
    public function admin_add($message, $type = 'success') {
        
        // check if wp error received
        if (is_wp_error($message) && $message->get_error_codes()) {
            $message = $this->wp_error_message($message);
        }

        // add alert
        $alerts = get_option($this->textdomain . '_admin_alerts', array());
        $alerts[] = array(
            'type' => $type,
            'message' => $message,
        );
        update_option($this->textdomain . '_admin_alerts', $alerts);
	}
    
    /**
     * Display admin alerts
     */
    public function admin_display() {
        $alerts = get_option($this->textdomain . '_admin_alerts');
        if (!empty($alerts)) {
            foreach ($alerts as $alert) {
                echo ws_view('admin/alert', $alert);
            }
            delete_option($this->textdomain . '_admin_alerts');
        }
    }
    
    /**
     * Get WP Error alert message
     * 
     * @param WP_Error $message
     * @return string String error message
     */
    protected function wp_error_message($message) {
        
        // check if single allowed
        $single = true;
        if (count($message->get_error_codes()) > 1) {
            $single = false;
        }

        // create error message
        $error_msg = '<strong>' . __('Following error(s) found:', $this->textdomain) . '</strong>' . "<br />\n";
        foreach ($message->get_error_codes() as $code) {
            $severity = $message->get_error_data($code);
            if ('message' == $severity) {
                continue;
            }
            foreach ($message->get_error_messages($code) as $error) {
                if ($single) {
                    $error_msg = $error;
                }
                else {
                    $error_msg .= ' - ' . $error . "<br />\n";
                }
            }
        }
        return $error_msg;
    }
}