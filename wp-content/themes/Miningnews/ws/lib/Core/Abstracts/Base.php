<?php

namespace WS\Core\Abstracts;

use WS;

/**
 * WS Base Abstract Class that is inherited by other classes
 */
abstract class Base {
    
    /**
     * The string used to uniquely identify
     * 
     * @var string 
     */
	protected $textdomain;
    
    /**
     * The current version
     * 
     * @var string 
     */
	protected $version;
    
    /**
     * The theme folder name
     * 
     * @var string 
     */
	protected $theme;
    
    /**
     * Constructor
     */
    public function __construct() {
        
        // set textdomain and version
        $this->textdomain = WS::$textdomain;
        $this->version = WS::$version;
        
        // set theme folder name
        $this->theme = basename(dirname(WS_PATH));
    }
}