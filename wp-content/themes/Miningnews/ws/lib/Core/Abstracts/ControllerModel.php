<?php

namespace WS\Core\Abstracts;

use WS;
use WS\Core\Abstracts\Base;
use WS\Component\Logger;

/**
 * WS Controller Model Abstract Class
 */
abstract class ControllerModel extends Base {
    
    /**
     * Class name
     * 
     * @var string
     */
    public $name = null;
    
    /**
     * Logger class
     * 
     * @var WS\Component\Logger
     */
    protected $logger = null;
    
    /**
     * Loader options
     * 
     * @var array
     */
    protected $load_allowed = array('Component');
    
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // logger
        $this->logger = Logger::instance();
        
        // set base default variables
        $this->name = WS::dashify((new \ReflectionClass($this))->getShortName());
    }
    
    /**
     * Send an email
     * 
     * @param string|array $to Array or comma-separated list of email addresses to send message
     * @param string $subject Email subject
     * @param string $message Message contents
     * @param string|array $headers Optional. Additional headers
     * @param string|array $attachments Optional. Files to attach
     * @return bool Whether the email contents were sent successfully
     */
    protected function email($to, $subject, $message, $headers = array(), $attachments = array()) {
        return ws_email($to, $subject, $message, $headers, $attachments);
    }
    
    /**
     * Make a log entry
     * 
     * @param string $message Log message
     * @param mixed $data Log data
     * @return boolean Whether the log was made successfully
     */
    protected function log($message, $data = false) {
        return $this->logger->add('ws', $message, $data);
    }
    
    /**
     * Load allowed class
     * 
     * @param string $type Type to load
     * @param string $class Class to load
     */
    protected function load($type, $class) {
        if (!in_array($type, $this->load_allowed)) {
            return false;
        }
        
        // get class and load if available
        $AppObject = WS::get_class($type, $class);
        if ($AppObject) {
            $property_name = $class . $type;
            $this->$property_name = $AppObject;
            return true;
        }
        
        return false;
    }
    
    /**
     * Load Component
     * 
     * @param string $component Component to load
     */
    protected function load_component($component) {
        return $this->load('Component', $component);
    }
}
