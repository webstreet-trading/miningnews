<?php

namespace WS\Loader;

use \WS;
use WS\Core\Abstracts\Base;

/**
 * WS Init Class
 */
class Init extends Base {
    
    /**
     * The array of actions registered
     * 
     * @var array
     */
    private $actions = array();
    
    /**
     * The array of filters registered
     * 
     * @var array 
     */
    private $filters = array();
    
    /**
     * The array of shortcodes registered
     * 
     * @var array 
     */
    private $shortcodes = array();
    
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // add system hooks
        $this->internal_system_hooks();
        $this->system_hooks();
        $this->register_posts_taxonomies();
        
        // add public hooks
        if (!is_admin()) {
            $this->internal_public_hooks();
            $this->public_hooks();
        }

        // add admin hooks
        if (is_admin()) {
            $this->internal_admin_hooks();
            $this->admin_hooks();
        }
        
        // run hooks
        $this->run();
    }
    
    /**
     * Register action
     * 
     * @param string $tag Name of the action
     * @param string|object $class Class name or object for the action
     * @param string $function Callback function
     * @param int $priority Call priority. Default 10
     * @param int $accepted_args Number of arguments that should be passed to callback. Default 1
     */
	public function add_action($tag, $class, $function, $priority = 10, $accepted_args = 1) {
		$this->actions = $this->register($this->actions, $tag, $class, $function, $priority, $accepted_args);
	}
    
    /**
     * Register filter
     * 
     * @param string $tag Name of the filter
     * @param string|object $class Class name or object for the filter
     * @param string $function Callback function
     * @param int $priority Call priority. Default 10
     * @param int $accepted_args Number of arguments that should be passed to callback. Default 1
     */
	public function add_filter($tag, $class, $function, $priority = 10, $accepted_args = 1) {
		$this->filters = $this->register($this->filters, $tag, $class, $function, $priority, $accepted_args);
	}
    
    /**
     * Register shortcode
     * 
     * @param string $tag Name of the shortcode
     * @param string|object $class Class name or object for the filter
     * @param string $function Callback function
     */
	public function add_shortcode($tag, $class, $function) {
        $this->shortcodes = $this->register($this->shortcodes, $tag, $class, $function);
	}
    
    /**
     * Register a hook
     * 
     * @param array $hooks The collection of hooks
     * @param string $tag Name of the hook
     * @param string|object $class Class name or object for the hook
     * @param string $function Callback function
     * @param int $priority Call priority. Default 10
     * @param int $accepted_args Number of arguments that should be passed to callback. Default 1
     * @return array The collection of hooks
     */
    private function register($hooks, $tag, $class, $function, $priority = '10', $accepted_args = '1') {
        
        $callback = $this->format_callback($class, $function);
        
        // hook unique id
        // set priority to 10 to make it easier if we want to remove it,
        // since we should not call the same combination with different priorities
        // for some reason there is a bug that wp_ajax_ws and wp_ajax_nopriv_ws gets the same id, prefix tag to prevent this
        $hook_id = $tag . '_' . _wp_filter_build_unique_id($tag, $callback, 10);
        
        // add hooks
        $hooks[$hook_id] = array(
			'tag' => $tag,
			'callback' => $callback,
			'priority' => $priority,
			'accepted_args' => $accepted_args
		);

		return $hooks;
	}
    
    /**
     * Remove loader action
     * 
     * @param string $tag Name of the action
     * @param string|object $class Class name or object for the action
     * @param string $function Callback function
     */
	public function remove_action($tag, $class, $function) {
		$this->actions = $this->remove($this->actions, $tag, $class, $function);
	}
    
    /**
     * Remove loader filter
     * 
     * @param string $tag Name of the filter
     * @param string|object $class Class name or object for the filter
     * @param string $function Callback function
     */
	public function remove_filter($tag, $class, $function) {
		$this->filters = $this->remove($this->filters, $tag, $class, $function);
	}
    
    /**
     * Remove loader shortcode
     * 
     * @param string $tag Name of the shortcode
     * @param string|object $class Class name or object for the filter
     * @param string $function Callback function
     */
	public function remove_shortcode($tag, $class, $function) {
        $this->shortcodes = $this->remove($this->shortcodes, $tag, $class, $function);
	}
    
    /**
     * Remove a loader hook
     * 
     * @param array $hooks The collection of hooks
     * @param string $tag Name of the hook
     * @param string|object $class Class name or object for the hook
     * @param string $function Callback function
     * @return array The collection of hooks
     */
    private function remove($hooks, $tag, $class, $function) {
        
        $callback = $this->format_callback($class, $function);
        
        // hook unique id
        // set priority to 10 to make it easier if we want to remove it,
        // since we should not call the same combination with different priorities
        // for some reason there is a bug that wp_ajax_ws and wp_ajax_nopriv_ws gets the same id, prefix tag to prevent this
        $hook_id = $tag . '_' . _wp_filter_build_unique_id($tag, $callback, 10);
        
        // remove hook
        if (isset($hooks[$hook_id])) {
            unset($hooks[$hook_id]);
        }

		return $hooks;
	}
    
    /**
     * Format callback
     * 
     * @param string|object $class Class name or object for the hook
     * @param string $function Callback function
     * @return mixed Formatted callback for hook
     */
    private function format_callback($class, $function) {
        
        // get callback for string class
        if (is_string($class)) {
            
            // if class string name, it may be hook class
            $Hook = WS::get_class('Hook', $class);
            if ($Hook && is_object($Hook)) {
                $class = $Hook;
            }
            
            $callback = array($class, $function);
        }
        // add class to callback if it exists
        elseif (is_object($class)) {
            $callback = array($class, $function);
        }
        else {
            $callback = $function;
        }
        
        return $callback;
    }
    
    /**
     * Add the hooks
     */
    public function run() {
        foreach ($this->filters as $hook) {
			add_filter($hook['tag'], $hook['callback'], $hook['priority'], $hook['accepted_args']);
		}
        
        foreach ($this->actions as $hook) {
			add_action($hook['tag'], $hook['callback'], $hook['priority'], $hook['accepted_args']);
		}
        
        foreach ($this->shortcodes as $hook) {
			add_shortcode($hook['tag'], $hook['callback']);
		}
    }
    
    /**
     * Register internal system hooks
     */
    function internal_system_hooks() {
        
        // system
        $this->add_action('plugins_loaded', 'System', 'load_textdomain');
        
        // controller ajax actions
        $this->add_action('wp_ajax_ws', 'Dispatcher', 'ajax');
        $this->add_action('wp_ajax_nopriv_ws', 'Dispatcher', 'ajax');
        
        // CMB2 fields customizer
        $this->add_filter('cmb2_render_html_block', 'CMB2', 'render_callback_for_html_block', 10, 5);
        $this->add_filter('cmb2_render_view', 'CMB2', 'render_callback_for_view', 10, 5);
        $this->add_action('cmb2_render_post_title', 'CMB2', 'render_post_title', 10, 5);
        $this->add_action('cmb2_render_post_content', 'CMB2', 'render_post_content', 10, 5);
        $this->add_filter('cmb2_sanitize_post_content', 'CMB2', 'sanitize_post_content', 10, 5);
        $this->add_filter('cmb2_override_meta_save', 'CMB2', 'override_meta_save', 10, 4);
        $this->add_filter('cmb2_types_esc_html', 'CMB2', 'types_esc_disable');
        $this->add_filter('cmb2_types_esc_view', 'CMB2', 'types_esc_disable');
        $this->add_filter('cmb2_sanitize_post_html', 'CMB2', 'sanitize_disable');
        $this->add_filter('cmb2_sanitize_post_view', 'CMB2', 'sanitize_disable');
        $this->add_filter('cmb2_render_map', 'CMB2', 'render_map', 10, 5);
    }
    
    /**
     * Register internal public hooks
     */
    function internal_public_hooks() {
        
        // controller query vars
        $this->add_filter('query_vars', 'Dispatcher', 'query_vars');
        
        // controller pages
        $this->add_filter('template_include', 'Dispatcher', 'controller_page_template');
        
        // set page title
        $this->add_filter('document_title_parts', 'WsTemplate', 'document_title_parts');
    }
    
    /**
     * Register internal admin hooks
     */
    function internal_admin_hooks() {
        
        // rewrite rules
        $this->add_action('rewrite_rules_array', 'Dispatcher', 'rewrite_rules_array');
        
        // load settings
        $this->add_filter('admin_init', 'Settings', 'init');
        $this->add_filter('admin_menu', 'Settings', 'add_options_page');
        $this->add_filter('cmb2_admin_init', 'Settings', 'add_options_page_metabox');
        $this->add_action('cmb2_save_options-page_fields', 'Settings', 'after_save', 10, 4);
        
        // admin
        $this->add_filter('admin_menu', 'Admin', 'admin_menu', 11); // call after admin settings admin menu filter
        $this->add_filter('ws_settings', 'Admin', 'settings');
        $this->add_filter('cmb2_meta_boxes', 'Admin', 'set_meta_boxes');
        
        // alerts
        $Alert = WS::get_class('Component', 'Alert', 'WS');
        $this->add_action('admin_notices', $Alert, 'admin_display');
    }
    
    /**
     * Register system hooks
     */
    public function system_hooks() {}
    
    /**
     * Register public hooks
     */
    public function public_hooks() {}
    
    /**
     * Register admin hooks
     */
    public function admin_hooks() {}
    
    /**
     * Register custom post types and taxonomies
     */
    public function register_posts_taxonomies() {}
}
