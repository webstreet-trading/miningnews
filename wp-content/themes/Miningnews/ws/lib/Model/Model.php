<?php

namespace WS\Model;

use WS;
use WS\Core\Abstracts\ControllerModel;

use \WP_Query;

/**
 * WS Model class
 */
abstract class Model extends ControllerModel {
    
    /**
     * Post type
     * 
     * @var string
     */
    protected $post_type = null;
    
    /**
     * Auto Populate WP edit with model field metaboxes
     * 
     * @var boolean
     */
    public $metaboxes_populate = false;
    
    /**
     * Form object
     * 
     * @var WS\Component\Form
     */
    protected $form = null;
    
    /**
     * Form errors
     * 
     * @var \WP_Error
     */
    protected $form_errors = array();
    
    /**
     * Model fields
     * 
     * @var string
     */
    public $fields = null;
    
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // set fields
        $this->set_fields();
        
        // set post type
        if (!isset($this->post_type)) {
            $this->post_type = WS::dashify($this->name);
        }
        
        // set form object
        $this->form = WS::get_class('Component', 'Form');
    }
    
    /**
     * Get model post type
     * 
     * @return string
     */
    public function get_post_type() {
        return $this->post_type;
    }
    
    /**
     * Set fields for model
     */
    public function set_fields() {
        $this->fields = array();
    }
    
    /**
     * Form Setup
     * 
     * @param int $post_id Post to edit
     * @param array $args Optional form arguments
     *      @type array $settings Metabox settings
     *      @type array $fields Fields array to populate
     *      @type string $post_type Form post type
     */
    public function setup_form($post_id = '', $args = array()) {
        extract(wp_parse_args($args, array(
            'settings' => array(),
            'fields' => $this->fields,
            'post_type' => $this->post_type,
        )));
        if (!empty($fields)) {
            $this->form->setup($this->name . '-form', $post_type, $fields, $post_id, $settings);
        }
    }
    
    /**
     * Display form
     * 
     * @param array $args Optional CMB2 form arguments array
     * @return string Form html
     */
    public function form($args = array()) {
        return $this->form->display($args);
    }
    
    /**
     * Process submitted metabox
     * 
     * @return boolean Return true on success, WP_Error object on error
     */
    public function process() {
        $result = $this->form->process();
        if (is_wp_error($result)) {
            $this->form_errors = $result;
        }
        return $result;
    }
    
    /**
     * Process submitted form
     * 
     * @param array $save_fields Fields to save that is not handled by default like pending status
     * @return boolean Return Object id on success, false on failure
     */
    public function save($save_fields = array()) {
        $result = $this->form->save($save_fields);
        if (!is_wp_error($result) && true !== $result) {
            return $result;
        }
        elseif (is_wp_error($result)) {
            $this->form_errors = $result;
        }
        return false;
    }
    
    /**
     * Get current model post ID
     * 
     * @return int
     */
    public function get_ID() {
        return $this->form->get_object_id();
    }
    
    /**
     * Get form errors
     * 
     * @return \WP_Error
     */
    public function get_errors() {
        return $this->form_errors;
    }
    
    /**
	 * Make a query
     * 
     * post_type arg defaults to model's post type
	 *
	 * @param string|array $args {
	 *     Optional. Array or string of Query parameters.
	 *
	 *     @type int          $attachment_id           Attachment post ID. Used for 'attachment' post_type.
	 *     @type int|string   $author                  Author ID, or comma-separated list of IDs.
	 *     @type string       $author_name             User 'user_nicename'.
	 *     @type array        $author__in              An array of author IDs to query from.
	 *     @type array        $author__not_in          An array of author IDs not to query from.
	 *     @type bool         $cache_results           Whether to cache post information. Default true.
	 *     @type int|string   $cat                     Category ID or comma-separated list of IDs (this or any children).
	 *     @type array        $category__and           An array of category IDs (AND in).
	 *     @type array        $category__in            An array of category IDs (OR in, no children).
	 *     @type array        $category__not_in        An array of category IDs (NOT in).
	 *     @type string       $category_name           Use category slug (not name, this or any children).
	 *     @type string       $comment_status          Comment status.
	 *     @type int          $comments_per_page       The number of comments to return per page.
	 *                                                 Default 'comments_per_page' option.
	 *     @type array        $date_query              An associative array of WP_Date_Query arguments.
	 *                                                 See WP_Date_Query::__construct().
	 *     @type int          $day                     Day of the month. Default empty. Accepts numbers 1-31.
	 *     @type bool         $exact                   Whether to search by exact keyword. Default false.
	 *     @type string|array $fields                  Which fields to return. Single field or all fields (string),
	 *                                                 or array of fields. 'id=>parent' uses 'id' and 'post_parent'.
	 *                                                 Default all fields. Accepts 'ids', 'id=>parent'.
	 *     @type int          $hour                    Hour of the day. Default empty. Accepts numbers 0-23.
	 *     @type int|bool     $ignore_sticky_posts     Whether to ignore sticky posts or not. Setting this to false
	 *                                                 excludes stickies from 'post__in'. Accepts 1|true, 0|false.
	 *                                                 Default 0|false.
	 *     @type int          $m                       Combination YearMonth. Accepts any four-digit year and month
	 *                                                 numbers 1-12. Default empty.
	 *     @type string       $meta_compare            Comparison operator to test the 'meta_value'.
	 *     @type string       $meta_key                Custom field key.
	 *     @type array        $meta_query              An associative array of WP_Meta_Query arguments. See WP_Meta_Query.
	 *     @type string       $meta_value              Custom field value.
	 *     @type int          $meta_value_num          Custom field value number.
	 *     @type int          $menu_order              The menu order of the posts.
	 *     @type int          $monthnum                The two-digit month. Default empty. Accepts numbers 1-12.
	 *     @type string       $name                    Post slug.
	 *     @type bool         $nopaging                Show all posts (true) or paginate (false). Default false.
	 *     @type bool         $no_found_rows           Whether to skip counting the total rows found. Enabling can improve
	 *                                                 performance. Default false.
	 *     @type int          $offset                  The number of posts to offset before retrieval.
	 *     @type string       $order                   Designates ascending or descending order of posts. Default 'DESC'.
	 *                                                 Accepts 'ASC', 'DESC'.
	 *     @type string|array $orderby                 Sort retrieved posts by parameter. One or more options may be
	 *                                                 passed. To use 'meta_value', or 'meta_value_num',
	 *                                                 'meta_key=keyname' must be also be defined. To sort by a
	 *                                                 specific `$meta_query` clause, use that clause's array key.
	 *                                                 Accepts 'none', 'name', 'author', 'date', 'title',
	 *                                                 'modified', 'menu_order', 'parent', 'ID', 'rand',
	 *                                                 'relevance', 'RAND(x)' (where 'x' is an integer seed value),
	 *                                                 'comment_count', 'meta_value', 'meta_value_num', 'post__in',
	 *                                                 'post_name__in', 'post_parent__in', and the array keys
	 *                                                 of `$meta_query`. Default is 'date', except when a search
	 *                                                 is being performed, when the default is 'relevance'.
	 *
	 *     @type int          $p                       Post ID.
	 *     @type int          $page                    Show the number of posts that would show up on page X of a
	 *                                                 static front page.
	 *     @type int          $paged                   The number of the current page.
	 *     @type int          $page_id                 Page ID.
	 *     @type string       $pagename                Page slug.
	 *     @type string       $perm                    Show posts if user has the appropriate capability.
	 *     @type string       $ping_status             Ping status.
	 *     @type array        $post__in                An array of post IDs to retrieve, sticky posts will be included
	 *     @type string       $post_mime_type          The mime type of the post. Used for 'attachment' post_type.
	 *     @type array        $post__not_in            An array of post IDs not to retrieve. Note: a string of comma-
	 *                                                 separated IDs will NOT work.
	 *     @type int          $post_parent             Page ID to retrieve child pages for. Use 0 to only retrieve
	 *                                                 top-level pages.
	 *     @type array        $post_parent__in         An array containing parent page IDs to query child pages from.
	 *     @type array        $post_parent__not_in     An array containing parent page IDs not to query child pages from.
	 *     @type string|array $post_type               A post type slug (string) or array of post type slugs. Defaults to model's post type
	 *                                                 Default 'any' if using 'tax_query'.
	 *     @type string|array $post_status             A post status (string) or array of post statuses.
	 *     @type int          $posts_per_page          The number of posts to query for. Use -1 to request all posts.
	 *     @type int          $posts_per_archive_page  The number of posts to query for by archive page. Overrides
	 *                                                 'posts_per_page' when is_archive(), or is_search() are true.
	 *     @type array        $post_name__in           An array of post slugs that results must match.
	 *     @type string       $s                       Search keyword(s). Prepending a term with a hyphen will
	 *                                                 exclude posts matching that term. Eg, 'pillow -sofa' will
	 *                                                 return posts containing 'pillow' but not 'sofa'. The
	 *                                                 character used for exclusion can be modified using the
	 *                                                 the 'wp_query_search_exclusion_prefix' filter.
	 *     @type int          $second                  Second of the minute. Default empty. Accepts numbers 0-60.
	 *     @type bool         $sentence                Whether to search by phrase. Default false.
	 *     @type bool         $suppress_filters        Whether to suppress filters. Default false.
	 *     @type string       $tag                     Tag slug. Comma-separated (either), Plus-separated (all).
	 *     @type array        $tag__and                An array of tag ids (AND in).
	 *     @type array        $tag__in                 An array of tag ids (OR in).
	 *     @type array        $tag__not_in             An array of tag ids (NOT in).
	 *     @type int          $tag_id                  Tag id or comma-separated list of IDs.
	 *     @type array        $tag_slug__and           An array of tag slugs (AND in).
	 *     @type array        $tag_slug__in            An array of tag slugs (OR in). unless 'ignore_sticky_posts' is
	 *                                                 true. Note: a string of comma-separated IDs will NOT work.
	 *     @type array        $tax_query               An associative array of WP_Tax_Query arguments.
	 *                                                 See WP_Tax_Query->queries.
	 *     @type string       $title                   Post title.
	 *     @type bool         $update_post_meta_cache  Whether to update the post meta cache. Default true.
	 *     @type bool         $update_post_term_cache  Whether to update the post term cache. Default true.
	 *     @type bool         $lazy_load_term_meta     Whether to lazy-load term meta. Setting to false will
	 *                                                 disable cache priming for term meta, so that each
	 *                                                 get_term_meta() call will hit the database.
	 *                                                 Defaults to the value of `$update_post_term_cache`.
	 *     @type int          $w                       The week number of the year. Default empty. Accepts numbers 0-53.
	 *     @type int          $year                    The four-digit year. Default empty. Accepts any four-digit year.
	 * }
     * 
     * @param boolean $return_query On true returns WP_Query object, on false returns results array. Default false
     * @return WP_Query|array Query object or results array
	 */
    public function query($args, $return_query = false) {
        
        if (get_query_var('paged', false)) {
            $page = get_query_var('paged', 1);
        }
        elseif (get_query_var('page', false)) {
            $page = get_query_var('page', 1);
        }
        else {
            $page = 1;
        }
        
        // set defaults
        $args = wp_parse_args($args, array(
            'post_type' => $this->post_type,
            'paged' => $page,
        ));
        
        // create query
        $the_query = new WP_Query($args);
        $results = $the_query->query($args);
        
        // if query object should be returned
        if ($return_query) {
            return $the_query;
        }
        else {
            return $results;
        }
    }
    
    /**
     * Retrieve list of latest posts or posts matching criteria.
     *
     * The defaults are as follows:
     *
     * @see Model::query()
     *
     * @param array $args {
     *     Optional. Arguments to retrieve posts. See WP_Query::parse_query() for all
     *     available arguments.
     *
     *     @type int        $numberposts      Total number of posts to retrieve. Is an alias of $posts_per_page
     *                                        in WP_Query. Accepts -1 for all. Default 5.
     *     @type int|string $category         Category ID or comma-separated list of IDs (this or any children).
     *                                        Is an alias of $cat in WP_Query. Default 0.
     *     @type array      $include          An array of post IDs to retrieve, sticky posts will be included.
     *                                        Is an alias of $post__in in WP_Query. Default empty array.
     *     @type array      $exclude          An array of post IDs not to retrieve. Default empty array.
     *     @type bool       $suppress_filters Whether to suppress filters. Default true.
     * }
     * @return array List of posts.
     */
    public function get_posts($args) {
        $defaults = array(
            'numberposts' => 5,
            'category' => 0, 'orderby' => 'date',
            'order' => 'DESC', 'include' => array(),
            'exclude' => array(), 'meta_key' => '',
            'meta_value' =>'', 'post_type' => 'post',
            'suppress_filters' => true,
            'post_type' => $this->post_type,
        );

        $r = wp_parse_args($args, $defaults);
        if (empty($r['post_status']))
            $r['post_status'] = ('attachment' == $r['post_type']) ? 'inherit' : 'publish';
        if (! empty($r['numberposts']) && empty($r['posts_per_page']))
            $r['posts_per_page'] = $r['numberposts'];
        if (! empty($r['category']))
            $r['cat'] = $r['category'];
        if (! empty($r['include'])) {
            $incposts = wp_parse_id_list($r['include']);
            $r['posts_per_page'] = count($incposts);  // only the number of posts included
            $r['post__in'] = $incposts;
        } elseif (! empty($r['exclude']))
            $r['post__not_in'] = wp_parse_id_list($r['exclude']);

        $r['ignore_sticky_posts'] = true;
        $r['no_found_rows'] = true;
        
        return $this->query($r);
    }
}
