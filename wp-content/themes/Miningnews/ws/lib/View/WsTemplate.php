<?php
/**
 * WS Template class
 */
class WsTemplate {
    
    /**
     * Title of page
     * 
     * @var string 
     */
    static protected $title = '';
    
    /**
     * Content of page
     * 
     * @var string 
     */
    static protected $content = '';
    
    /**
     * Additional page arguments
     * 
     * @var array 
     */
    static protected $args = array();
    
    /**
     * Setup template variables
     * 
     * @param string $title Title of page
     * @param string $content Content of page
     * @param array $args Additional arguments like controller and class
     */
    static public function setup($title, $content, $args = array()) {
        
        self::$title = $title;
        self::$content = $content;
        
        $args = wp_parse_args($args, array(
            'controller' => '',
            'class' => '',
        ));
        $args['controller'] = WS::underscore($args['controller']);
        self::$args = array_merge($args, self::$args);
    }
    
    /**
     * Add/Update argument
     * 
     * @param string $key Argument key
     * @param string $value Argument value
     */
    static public function set_arg($key, $value) {
        self::$args[$key] = $value;
    }
    
    /**
	 * Template modify document title parts
	 *
	 * @param array $parts {
	 *     The document title parts.
	 *
	 *     @type string $title Title of the viewed page.
	 *     @type string $page Optional. Page number if paginated.
	 *     @type string $tagline Optional. Site description when on home page.
	 *     @type string $site Optional. Site title when not on home page.
	 * }
	 */
    static public function document_title_parts($parts) {
        $parts['title'] = WsTemplate::the_title('', '', false);
        return $parts;
    }
    
    /**
     * Template title
     *
     * @param string $before Optional. Markup to prepend to the title. Default empty.
     * @param string $after Optional. Markup to append to the title. Default empty.
     * @param bool $echo Optional. Whether to echo or return the title. Default true for echo.
     * @return string|void Current template title if $echo is false.
     */
    static public function the_title($before = '', $after = '', $echo = true) {
        
        // apply title filter
        $title = apply_filters('the_title', self::$title, false);
        
        // check not empty
        if (strlen($title) == 0) {
            return;
        }
        
        // set before and after
        $title = $before . $title . $after;
        
        if ($echo) {
            echo $title;
        }
        return $title;
    }
    
    /**
     * Get the title
     * 
     * @return string Title text
     */
    static public function get_the_title() {
        return self::$title;
    }
    
    /**
     * Template content get
     * 
     * @param boolean $echo Output content. Default true
     * @return string Content
     */
    static public function the_content($echo = true) {
        
        // apply title filter
        $content = str_replace(']]>', ']]&gt;', self::$content);
        
        if ($echo) {
            echo $content;
        }
        return $content;
    }
    
    /**
     * Template argument output
     * 
     * @param string $arg The argument to retrieve
     * @param boolean $echo Output argument. Default true
     * @return string Argument string
     */
    static public function the_arg($arg, $echo = true) {
        
        $string = self::get_the_arg($arg);
        
        if ($echo) {
            echo $string;
        }
        return $string;
    }
    
    /**
     * Get the argument
     * 
     * @param string $arg The argument to retrieve
     * @return string Argument string
     */
    static public function get_the_arg($arg) {
        
        if (!isset(self::$args[$arg])) {
            return;
        }
        
        return self::$args[$arg];
    }
    
    /**
     * Return path to template file
     * 
     * @return string Template file path
     */
    static public function template_path() {
        return WS::get_viewpath('template');
    }
}
