<?php

namespace WS\Controller\Abstracts;

use WS;
use WS\Core\Abstracts\ControllerModel;

/**
 * WS Controller class
 */
abstract class Controller extends ControllerModel {
    
    /**
     * Controller method
     * 
     * @var string
     */
    protected $method = '';
    
    /**
     * Page template to use
     * 
     * @var string
     */
    public $page_template = null;
    
    /**
     * Page title to use
     * 
     * @var string
     */
    public $page_title = null;
    
    /**
     * Page args to use
     * 
     * @var array
     */
    public $page_args = array();
    
    /**
     * Views subfolder to use
     * 
     * @var string
     */
    protected $view_path = null;
    
    /**
     * View file to render
     * 
     * @var string
     */
    protected $view = null;
    
    /**
     * Variables for the view
     * 
     * @var array
     */
    protected $view_vars = array();
    
    /**
     * If true will auto render the view after page
     * 
     * @var boolean 
     */
    public $auto_render = true;
    
    /**
     * Alert object
     * 
     * @var WS\Component\Alert
     */
    private $alerter = null;
    
    /**
     * Loader options
     * 
     * @var array
     */
    protected $load_allowed = array('Model', 'Component');

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        
        // set base default variables
        $this->page_template = \WsTemplate::template_path();
        $this->page_title = WS::humanize($this->name);
        $this->view_path = $this->name;
        
        // add alerter
        $this->alerter = WS::get_class('Component', 'Alert', 'WS');
    }
    
    /**
     * Dispatch controller
     * 
     * @param string $method Method to call on controller
     * @param array $params Method parameters
     */
    public function dispatch($method, $params = array()) {
        
        // ensure method exists
        if (!method_exists($this, $method)) {
            return new \WP_Error('404_method', __('Method not found for controller', $this->textdomain));
        }
        
        // set controller method
        $this->method = $method;
        
        // call controller method with params
        switch (count($params)) {
            case 0:
                return $this->{$method}();
            case 1:
                return $this->{$method}($params[0]);
            case 2:
                return $this->{$method}($params[0], $params[1]);
            case 3:
                return $this->{$method}($params[0], $params[1], $params[2]);
            case 4:
                return $this->{$method}($params[0], $params[1], $params[2], $params[3]);
            case 5:
                return $this->{$method}($params[0], $params[1], $params[2], $params[3], $params[4]);
            default:
                return call_user_func_array(array(&$this, $method), $params);
        }
    }
    
    /**
     * Set page template
     * 
     * @param string $path Page template view
     */
    protected function set_page_template($view) {
        $this->page_template = \WS::get_viewpath($view);
    }
    
    /**
     * Set page title
     * 
     * @param string $title Page title
     */
    protected function set_page_title($title) {
        $this->page_title = $title;
    }
    
    /**
     * Set page argument
     * 
     * @param string $key Argument key
     * @param mixed $value Value of argument
     */
    protected function set_page_arg($key, $value) {
        $this->page_args[$key] = $value;
    }
    
    /**
     * Sets variable for view
     * 
     * @param string|array $arg1 String or array of data
     * @param string|array $arg2 Value of arg1 if arg1 is string key
     */
    protected function set($arg1, $arg2 = null) {
        if (is_array($arg1)) {
            $data = $arg1;
        }
        else {
            $data = array($arg1 => $arg2);
        }
        $this->view_vars = $data + $this->view_vars;
    }
    
    /**
     * Redirect to location
     * 
     * @param string $location Location to redirect to
     * @param int $status Optional HTTP status code (404, etc)
     * @param boolean $exit Exit after redirect. Default true
     */
    protected function redirect($location, $status = null, $exit = true) {
        if (!isset($location)) {
            $location = site_url();
        }
        
        wp_redirect($location, $status);
        
        // exit if needed
        if ($exit) {
            exit();
        }
    }
    
    /**
     * Get Referer
     * 
     * If referer not found return fallback url
     * 
     * @param string $fallback Optional. Fallback url to return if referer not found. Default to site url
     */
    protected function referer($fallback = null) {
        $referer = wp_get_referer();
        if (!$referer) {
            if ($fallback) {
                $referer = $fallback;
            }
            else {
                $referer = site_url();
            }
        }
        return $referer;
    }
    
    /**
     * Internally redirects one page to another
     *
     * @param string $method The new method to be 'redirected' to
     * @param array ...$args Arguments passed to the page
     * @return mixed Returns the return value of the called page
     */
    protected function set_method($method, ...$args) {
        $this->method = $method;
        return $this->$method(...$args);
    }
    
    /**
     * Set the view variable
     * 
     * @param string $view Relative view to use. If it starts with a '/' it uses full view path. Defaults to method name
     */
    protected function set_view($view) {
        $this->view = $view;
    }
    
    /**
     * Render view
     * 
     * @param string $view Relative view to use. If it starts with a '/' it uses full view path. Defaults to method name
     */
    public function render($view = '') {
        
        // if view not use, use current method
        if (!$view) {
            if ($this->view) {
                $view = $this->view;
            }
            else {
                $view = WS::dashify($this->method);
            }
        }
        
        // disable auto render
        $this->auto_render = false;
        
        // set full view path
        if (substr($view, 0, 1) != '/') {
            $view = $this->view_path . '/' . $view;
        }
        
        // return view
        return ws_view($view, $this->view_vars);
    }
    
    /**
     * Add alert message
     * 
     * @param mixed $message Alert message string or WP_Error object
     * @param string $type Type of alert: success, error or info. Defaults to success
     */
    protected function alert($message, $type = 'success') {
        if (is_admin()) {
            $this->alerter->admin_add($message, $type);
        }
        else {
            $this->alerter->add($message, $type);
        }
    }
    
    /**
     * Load Model
     * 
     * @param string $model Model to load
     */
    protected function load_model($model) {
        return $this->load('Model', $model);
    }
}
