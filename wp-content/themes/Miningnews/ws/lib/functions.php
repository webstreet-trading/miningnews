<?php

/**
 * Get view
 * 
 * Short view helper function
 * 
 * @param string $view The view
 * @param array $vars View arguments
 * @return string View content
 */
function ws_view($view, $vars = array()) {
    return WS::view($view, $vars);
}

/**
 * Get setting(s)
 * 
 * Pass field key if specific option required otherwise all options will be retrieved
 * Short setting helper function
 * 
 * @param string $field_key Options array key
 * @param mixed $default Default value to return if setting key not found
 * @return mixed Option value or array of options keys and their values
 */
function ws_setting($field_key, $default = null) {
    return WS::get_setting($field_key, $default);
}

/**
 * Display errors
 * 
 * Shorthand helper function
 */
function ws_alerts() {
    $Alert = WS::get_class('Component', 'Alert', 'WS');
    $Alert->display();
}

/**
 * Send an email
 * 
 * Short email helper
 * 
 * @param string|array $to Array or comma-separated list of email addresses to send message
 * @param string $subject Email subject
 * @param string $message Message contents
 * @param string|array $headers Optional. Additional headers
 * @param string|array $attachments Optional. Files to attach
 * @return bool Whether the email contents were sent successfully
 */
function ws_email($to, $subject, $message, $headers = array(), $attachments = array()) {
    $Email = apply_filters('ws_email_class_override', WS::get_class('Component', 'Email', 'WS'));
    return $Email->send($to, $subject, $message, $headers, $attachments);
}

/**
 * Register post type
 * 
 * Helper to add post types with less code
 * 
 * @param string $post_type Post type key. Must not exceed 20 characters and may only 
 *                          contain lowercase alphanumeric characters, dashes and underscores. See sanitize_key()
 * @param string $singular Post type singular text
 * @param string $plural Post type plural text
 * @param array $args Post type arguments for registration
 * @return WP_Post_Type|WP_Error The registered post type object, or an error object
 */
function ws_register_post_type($post_type, $singular, $plural, $args = array()) {
    
    // set post type labels
    $labels = array(
        'name' => _x($plural, 'post type general name', 'ws'),
        'singular_name' => _x($singular, 'post type singular name', 'ws'),
        'menu_name' => _x($plural, 'admin menu', 'ws'),
        'name_admin_bar' => _x($singular, 'add new on admin bar', 'ws'),
        'add_new' => _x('Add New', 'hub', 'ws'),
        'add_new_item' => __('Add New ' . $singular, 'ws'),
        'new_item' => __('New ' . $singular, 'ws'),
        'edit_item' => __('Edit ' . $singular, 'ws'),
        'view_item' => __('View ' . $singular, 'ws'),
        'all_items' => __('All ' . $plural, 'ws'),
        'search_items' => __('Search ' . $plural, 'ws'),
        'parent_item_colon' => __('Parent ' . $plural . ':', 'ws'),
        'not_found' => __('No ' . strtolower($plural) . ' found.', 'ws'),
        'not_found_in_trash' => __('No ' . strtolower($plural) . ' found in Trash.', 'ws'),
    );
    
    // set post type args
    $args = wp_parse_args($args, array(
        'labels' => $labels,
        'description' => $plural,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => $post_type),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
    ));
    
    // register post type
    return register_post_type($post_type, $args);
}

/**
 * Register taxonomy
 * 
 * Helper to add taxonomy with less code
 * 
 * @param string $taxonomy Taxonomy key, must not exceed 32 characters
 * @param array|string $object_type Object type or array of object types with which the taxonomy should be associated
 * @param string $singular Taxonomy singular text
 * @param string $plural Taxonomy plural text
 * @param array $args Taxonomy arguments for registration
 * @return void|WP_Error Error object on error, void if success
 */
function ws_register_taxonomy($taxonomy, $object_type, $singular, $plural, $args = array()) {
    
    // set taxonomy labels
    $labels = array(
        'name' => _x($singular, 'taxonomy general name', 'ws'),
        'singular_name' => _x($singular, 'taxonomy singular name', 'ws'),
        'search_items' => __('Search ' . $plural, 'ws'),
        'popular_items' => __('Popular ' . $plural, 'ws'),
        'all_items' => __('All ' . $plural, 'ws'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit ' . $singular, 'ws'),
        'update_item' => __('Update ' . $singular, 'ws'),
        'add_new_item' => __('Add New ' . $singular, 'ws'),
        'new_item_name' => __('New ' . $singular, 'ws'),
        'add_or_remove_items' => __('Add or remove ' . strtolower($plural), 'ws'),
        'choose_from_most_used' => __('Choose from the most used ' . strtolower($plural), 'ws'),
        'not_found' => __('No ' . strtolower($plural) . ' found.', 'ws'),
        'menu_name' => $plural,
    );
    
    // set taxonomy args
    $args = wp_parse_args($args, array(
        'label' => $plural,
        'labels' => $labels,
        'public' => true,
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => $taxonomy),
    ));
    
    // register taxonomy
    register_taxonomy($taxonomy, $object_type, $args);
}

/**
 * Decode string if it was JSON encoded
 * 
 * @param string $original Maybe decode original, if is needed
 * @param bool $assoc When TRUE, returned objects will be converted into associative arrays
 * @param int $depth User specified recursion depth
 * @param int $options Bitmask of JSON decode options. Currently only JSON_BIGINT_AS_STRING is supported (default is to cast large integers as floats)
 * @return mixed Decoded result
 */
function ws_maybe_json_decode($original, $assoc = false, $depth = 512, $options = 0) {
    if (is_string($original)) {
        $decoded = json_decode($original);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return $decoded;
        }
    }
    return $original;
}

/**
 * Output html data attributes for a ajax request
 * 
 * @param string $controller The controller to call
 * @param string $method Controller method. Default index
 * @param array $params Params for the method. Default array()
 * @param string $nonce Nonce code. Optional. Will generate default nonce if not set
 */
function ws_ajax_html_data($controller, $method = 'index', $params = array(), $nonce = false) {
    
    // set default nonce
    if (!$nonce) {
        $nonce = wp_create_nonce('ws');
    }
    
    // setup data
    $data = array(
        'controller' => $controller,
        'method' => $method,
        'params' => json_encode($params),
        'nonce' => $nonce,
    );
    
    // format data
    $atts = array();
    foreach ($data as $key => $value) {
        $atts[] = 'data-' . $key . '="' . esc_attr($value). '"';
    }
    
    // output
    echo implode(' ', $atts);
}

if (class_exists('woocommerce')) {

/**
 * Override add alert message for Woocommerce
 * 
 * @param boolean $override Current override status
 * @param mixed $message Alert message string
 * @param string $type Type of alert: success, error or info. Defaults to success
 * @return boolean Return true to override alert
 */
function ws_wc_alert_add($override, $message, $type) {
    wc_add_notice($message, $type);
    return true;
}
add_filter('ws_alert_add_override', 'ws_wc_alert_add', 10, 3);

/**
 * Override display alert messages for Woocommerce
 * 
 * @param boolean $override Current override status
 * @return boolean Return true to override alert display
 */
function ws_wc_alert_display($override) {
    wc_print_notices();
    return true;
}
add_filter('ws_alert_display_override', 'ws_wc_alert_display');

/**
 * Override email class for Woocommerce
 */
function ws_wc_email_class($emailer) {
    // override and return class to use
    return WS::get_class('Component', 'WC_Email', 'WS');
}
add_filter('ws_email_class_override', 'ws_wc_email_class');

}

/**
 * Divi related functions
 */

/**
 * Divi titles filter
 * 
 * Add support for Template title
 * 
 * @param string $custom_title Custom title
 * @return string Custom title to use
 */
function elegant_titles_filter($custom_title) {
    global $shortname, $themename;
    $custom_title = '';
    $sitename = get_bloginfo('name');
    $site_description = get_bloginfo('description');
    #if the title is being displayed on the homepage
    if ((is_home() || is_front_page()) && ! elegant_is_blog_posts_page()) {
        if ('on' === et_get_option($shortname . '_seo_home_title')) {
            $custom_title = et_get_option($shortname . '_seo_home_titletext');
        } else {
            $seo_home_type = et_get_option($shortname . '_seo_home_type');
            $seo_home_separate = et_get_option($shortname . '_seo_home_separate');
            if ($seo_home_type == 'BlogName | Blog description') {
                $custom_title = $sitename . esc_html($seo_home_separate) . $site_description;
            }
            if ($seo_home_type == 'Blog description | BlogName') {
                $custom_title = $site_description . esc_html($seo_home_separate) . $sitename;
            }
            if ($seo_home_type == 'BlogName only') {
                $custom_title = $sitename;
            }
        }
    }
    #if the title is being displayed on single posts/pages
    if (((is_single() || is_page()) && ! is_front_page()) || elegant_is_blog_posts_page()) {
        global $wp_query;
        $postid = elegant_is_blog_posts_page() ? intval(get_option('page_for_posts')) : $wp_query->post->ID;
        $key = et_get_option($shortname . '_seo_single_field_title');
        $exists3 = get_post_meta($postid, '' . $key . '', true);
        if ('on' === et_get_option($shortname . '_seo_single_title') && '' !== $exists3) {
            $custom_title = $exists3;
        } else {
            $seo_single_type = et_get_option($shortname . '_seo_single_type');
            $seo_single_separate = et_get_option($shortname . '_seo_single_separate');
            $page_title = single_post_title('', false);
            if ($seo_single_type == 'BlogName | Post title') {
                $custom_title = $sitename . esc_html($seo_single_separate) . $page_title;
            }
            if ($seo_single_type == 'Post title | BlogName') {
                $custom_title = $page_title . esc_html($seo_single_separate) . $sitename;
            }
            if ($seo_single_type == 'Post title only') {
                $custom_title = $page_title;
            }
        }
    }
    #if the title is being displayed on index pages (categories/archives/search results)
    if (is_category() || is_archive() || is_search() || is_404()) {
        $page_title = '';
        $seo_index_type = et_get_option($shortname . '_seo_index_type');
        $seo_index_separate = et_get_option($shortname . '_seo_index_separate');
        if (is_category() || is_tag() || is_tax()) {
            $page_title = single_term_title('', false);
        } else if (is_post_type_archive()) {
            $page_title = post_type_archive_title('', false);
        } else if (is_author()) {
            $page_title = get_the_author_meta('display_name', get_query_var('author'));
        } else if (is_date()) {
            $page_title = esc_html__('Archives', $themename);
        } else if (is_search()) {
            $page_title = sprintf(esc_html__('Search results for "%s"', $themename), esc_attr(get_search_query()));
        } else if (is_404()) {
            $page_title = esc_html__('404 Not Found', $themename);
        }
        if ($seo_index_type == 'BlogName | Category name') {
            $custom_title = $sitename . esc_html($seo_index_separate) . $page_title;
        }
        if ($seo_index_type == 'Category name | BlogName') {
            $custom_title = $page_title . esc_html($seo_index_separate) . $sitename;
        }
        if ($seo_index_type == 'Category name only') {
            $custom_title = $page_title;
        }
    }
    
    // use template title
    if (WsTemplate::get_the_title()) {
        $seo_single_type = et_get_option($shortname . '_seo_single_type');
        $seo_single_separate = et_get_option($shortname . '_seo_single_separate');
        $page_title = WsTemplate::get_the_title();
        if ($seo_single_type == 'BlogName | Post title') {
            $custom_title = $sitename . esc_html($seo_single_separate) . $page_title;
        }
        if ($seo_single_type == 'Post title | BlogName') {
            $custom_title = $page_title . esc_html($seo_single_separate) . $sitename;
        }
        if ($seo_single_type == 'Post title only') {
            $custom_title = $page_title;
        }
    }
    
    $custom_title = wp_strip_all_tags($custom_title);
    return $custom_title;
}

/**
 * Simple output form cmb2 fields
 * 
 * @param int|WP_Post|null $post When using post to get fields and metadata
 * @param array $fields Optional fields. Falls back to post type model fields
 * @param array $args
 */
function cmb2_output_fields($post = null, $fields = array(), $args = array()) {
    
    // set args with defaults
    $args = wp_parse_args($args, array(
        'data' => array(),
        'title_format' => '<h3>%s</h3>',
        'field_template' => '%s<p>%s</p>',
        'field_name_format' => '<strong>%s</strong>',
        'field_array_glue' => '<br/> - ',
    ));
    
    if (isset($post)) {
        
        // get post
        $post = get_post($post);
        if ($post) {
            
            // get fields
            if (empty($fields)) {
                $Model = WS::get_class('Model', WS::camelize($post->post_type));
                $fields = $Model->get_fields();
            }

            // get metadata if not set
            if (empty($args['data'])) {
                $args['data'] = get_post_meta($post->ID);
            }
            
            // add title and content to data
            $args['data']['post_title'] = $post->post_title;
            $args['data']['post_content'] = $post->post_content;
        }
    }
    
    // set data
    $data = $args['data'];
    
    ob_start();
    foreach ($fields as $id => $field) {
        
        // if id not set, use array key
        if (!isset($field['id'])) {
            $field['id'] = $id;
        }

        // if name not set, humanize id
        if (!isset($field['name'])) {
            $field['name'] = WS::humanize($field['id']);
        }
        
        if ('title' == $field['type']) {
            printf($args['title_format'], $field['name']);
        }
        else {
            if (isset($data[$field['id']]) && is_array($data[$field['id']]) && isset($data[$field['id']][0]) && 'group' != $field['type']) {
                $data[$field['id']] = $data[$field['id']][0];
            }
            $value = false;
            if (isset($data[$field['id']])) {
                $value = maybe_unserialize($data[$field['id']]);
                if (is_array($value)) {
                    if ('group' == $field['type']) {
                        foreach ($data[$field['id']] as $k => $v) {
                            if (!isset($field['options']['group_title'])) {
                                $field['options']['group_title'] = __('Entry {#}', 'bu');
                            }
                            if (in_array($field['id'], array('retainer_turnover', 'retainer_staff_compliment', 'per_service_rate', 'hourly_rate'))) {
                                if (count($v) <= 1) {
                                    $value = false;
                                    continue;
                                }
                            }
                            
                            $grouptitle = array(array(
                                'name' => str_replace('{#}', $k + 1, $field['options']['group_title']),
                                'type' => 'title',
                                'id' => 'title_' . $field['id'] . '_' . $k + 1,
                            ));
                            $group_args = $args;
                            $group_args['data'] = $v;
                            echo cmb2_output_fields($post, array_merge($grouptitle, $field['fields']), $group_args);
                        }
                        $value = false;
                    }
                    else {
                        $value = ' - ' . implode($args['field_array_glue'], $value);
                    }
                }
            }

            // no value, no need to output
            if (!$value) {
                continue;
            }
            
            // output field name
            $fieldname = '';
            if (isset($field['name']) && $field['name']) {
                $fieldname = sprintf($args['field_name_format'], $field['name']);
            }
            
            $fieldvalue = $value;
            if ('file' == $field['type']) {
                $fieldvalue = '<a href="' . $value . '">' . $value . '</a>';
            }
            
            printf($args['field_template'], $fieldname, $fieldvalue);
        }
    }
    return ob_get_clean();
}

/**
 * DEBUG
 */

if (!function_exists('debug')) {

/**
 * Helper function for debugging
 * 
 * @param mixed $var Optional variable to output
 */
function debug($var = '') {
    $debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
    $debug_output = sprintf('DEBUG (line: %s) in %s', $debug[0]['line'], $debug[0]['file']);
    if ($var) {
        $debug_output .= '<br/>' . print_r($var, true);
    }
    pr($debug_output);
}

}

if (!function_exists('pr')) {

/**
 * Helper function to output data
 * 
 * @param mixed $var Variable to output
 */
function pr($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

}

if (!function_exists('prd')) {

/**
 * Helper function to output data and exit
 * 
 * @param mixed $var Variable to output
 */
function prd($var) {
    pr($var);
    exit;
}

}

/**
 * END DEBUG
 */
