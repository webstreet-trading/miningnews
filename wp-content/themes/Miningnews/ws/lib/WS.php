<?php
/**
 * Webstreet class
 */
class WS {
    
    /**
     * The string used to uniquely identify
     * 
     * @var string 
     */
	public static $textdomain = 'ws';
    
    /**
     * The current version
     * 
     * @var string 
     */
	public static $version = '1.0.0';
    
    /**
     * Classes loaded
     * 
     * @var array 
     */
	public static $classes = array();
    
    /**
     * Setup textdomain and version
     * 
     * @param string $textdomain
     * @param string $version
     */
    public static function setup($textdomain, $version) {
        self::$textdomain = $textdomain;
        self::$version = $version;
    }
    
    /**
     * Camelize a given string
     * 
     * @param string $string Camelized string
     */
    public static function camelize($string) {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', self::underscore($string))));
    }

    /**
     * Underscore a given string
     * 
     * @param string $string Underscored string
     */
    public static function underscore($string) {
        return strtolower(str_replace('-', '_', sanitize_title(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $string))));
    }
    
    /**
     * Underscore a given string
     * 
     * @param string $string Underscored string
     */
    public static function dashify($string) {
        return str_replace('_', '-', self::underscore($string));
    }
    
    /**
     * Simple humanize a given string
     * 
     * @param string $string Humanized string
     */
    public static function humanize($string) {
        return ucwords(str_replace('_', ' ', str_replace('-', ' ', $string)));
    }
    
    /**
     * Get path for view
     * 
     * @param string $view View to get path for
     * @return string View's full path
     */
    public static function get_viewpath($view) {
        
        // whether or not .php was added
        $view_file = preg_replace('/.php$/', '', $view) . '.php';	

        $theme_file = locate_template('ws-view/' . $view_file);
        if ($theme_file) {
            $file = $theme_file;
        }
        else {
            $file = WS_APP . 'View/' . $view_file;
        }		

        return apply_filters('ws_template_' . $view_file, $file);
    }
    
    /**
     * Get view
     * 
     * @param string $view The view
     * @param array $vars View arguments
     * @return string View content
     */
    public static function view($view, $vars = array()) {
        
        // set domain
        $domain = WS::$textdomain;

        // get view path
        $viewpath = self::get_viewpath($view);
        if ($viewpath && is_file($viewpath)) {
            extract($vars);
            ob_start();
            include($viewpath);
            $content = ob_get_contents();
            ob_end_clean();
        }
        // view not found
        else {
            $content = sprintf(__('Unable to locate view for: %s', $domain), $view);
        }
        return $content;
    }
    
    /**
     * Get settings key
     * 
     * @return string The settings key
     */
    public static function settings_key() {
        return 'ws_settings';
    }
    
    /**
     * Get setting(s)
     * 
     * Pass field key if specific option required otherwise all options will be retrieved
     * 
     * @param string $field_key Options array key
     * @param mixed $default Default value to return if setting key not found
     * @return mixed Option value or array of options keys and their values
     */
    public static function get_setting($field_key = '', $default = null) {

        // get options
        $options = get_option(self::settings_key());

        // get specific field key
        if ($field_key) {
            if (isset($options[$field_key])) {
                return $options[$field_key];
            }
            else {
                return $default;
            }
        }
        else {
            return $options;
        }
    }
    
    /**
     * Dispatch a controller method
     * 
     * @param string $controller Controller to use
     * @param array $method Controller method to call
     * @param array $params Controller parameters
     * @return mixed Return array of dispatch result or false on failure
     *                  array(title=>TITLE,content=>CONTENT,args=>ARGS)
     */
    public static function dispatch($controller, $method, $params = array()) {
        // set controller class
        $Controller = WS::get_class('Controller', $controller);

        // check if class exists
        if (!$Controller) {
            return false;
        }
        
        $Dispatcher = WS::get_class('Hook', 'Dispatcher');
        $result = $Dispatcher->dispatch($Controller, $method);
        if ($result) {
            return $result;
        }
        else {
            return false;
        }
    }
    
    /**
     * Directory files
     * 
     * Scan directory for files
     * 
     * @param string $directory Directory to scan
     * @param boolean $folders Return folders. Default false
     * @return array Files in directory
     */
    public static function directory_files($directory, $folders = false) {
        
        $results = scandir($directory);
        $files = array();
        foreach ($results as $file) {
            
            // filter out invalid files
            if ($file === '.' || $file === '..') {
                continue;
            }
            
            // exclude folders
            if (!$folders && is_dir($directory . '/' . $file)) {
                continue;
            }
            
            // add file
            $files[] = $file;
        }
        
        return $files;
    }
    
    /**
     * Load class
     * 
     * @param string $type Type of class
     * @param string $class Name of the class
     * @param string $scope WSApp or WS for app or lib sections
     * @return object|boolean Return class object if found, otherwise return false
     */
    public static function get_class($type, $class, $scope = 'WSApp') {
        
        $sep = '\\';
        
        // get scope
        if ('WS' != $scope) {
            $scope = 'WSApp';
        }
        
        // set class name
        $classname = $scope . $sep . $type . $sep . $class;
        $classid = $scope . '.' . $type . '.' . $class;
        
        // if class already loaded, return it
        if (isset(self::$classes[$classid])) {
            return self::$classes[$classid];
        }
        // check if class exists, if so return object
        elseif (class_exists($classname)) {
            $Object = new $classname;
        }
        // fallback if scope not lib
        elseif ('WS' !== $scope) {
            $Object = self::get_class($type, $class, 'WS');
        }
        
        // return object of valid
        if (isset($Object) && $Object) {
            self::$classes[$classid] = $Object;
            return $Object;
        }
        
        return false;
    }
}
