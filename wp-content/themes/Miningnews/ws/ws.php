<?php

/**
 * Webstreet Wordpress library
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('WS_PATH', plugin_dir_path(__FILE__));
define('WS_APP', plugin_dir_path(__FILE__) . 'app/');
define('WS_LIB', plugin_dir_path(__FILE__) . 'lib/');

// load basics
require_once WS_LIB . 'WS.php';
require_once WS_LIB . 'View/WsTemplate.php';
require_once WS_LIB . 'functions.php';

// autoloader
require_once WS_PATH . 'vendor/autoload.php';

// load alerts, since it may need session start
WS::get_class('Component', 'Alert', 'WS');

// load bootstrap
require_once WS_APP . 'bootstrap.php';

// init
WS::get_class('Loader', 'Init');
