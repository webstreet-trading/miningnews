<?php

if (!class_exists('WS')) {

/**
 * Webstreet class
 */
class WS {
    
    /**
     * The string used to uniquely identify
     * 
     * @var string 
     */
	public static $textdomain = 'ws';
    
    /**
     * The current version
     * 
     * @var string 
     */
	public static $version = '1.0.0';
    
    /**
     * Setup textdomain and version
     * 
     * @param string $textdomain
     * @param string $version
     */
    public static function setup($textdomain, $version) {
        self::$textdomain = $textdomain;
        self::$version = $version;
    }
}

}