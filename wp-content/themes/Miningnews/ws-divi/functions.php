<?php

/**
 * Theme setup
 */
function ws_theme_setup() {
    
    // setup version and textdomain
    include('includes/WS.php');
    WS::setup('ws', '1.0.0');
    
    // run includes
    include('includes/customizer.php');

    // include widgets
    require_once('widgets/shortcode-widget.php');
    
    // woocommerce sensei
    add_theme_support('sensei');
}
add_action('after_setup_theme', 'ws_theme_setup');

/**
 * Helper to get textdomain or version
 * 
 * @param string $type textdomain or version. Default textdomain
 * @return string Text domain or version string
 */
function ws($type = 'textdomain') {
    if ('version' == $type) {
        return WS::$version;
    }
    else {
        return WS::$textdomain;
    }
}

/**
 * Register Widgets
 */
function ws_widgets_init() {
	register_widget('WS_Shortcode_Widget');
}
add_action('widgets_init', 'ws_widgets_init');

/**
 * Shortcode to show a module based on id
 * 
 * @param array $atts Shortcode attributes
 * @return string Shortcode HTML
 */
function ws_divimodule_shortcode($atts) {
    extract(shortcode_atts(array('id' => '*'), $atts));
    return do_shortcode('[et_pb_section global_module="' . $id . '"][/et_pb_section]');
}
add_shortcode('divimodule', 'ws_divimodule_shortcode');

/**
 * Custom Divi modules preparation
 */
function ws_prep_divi_custom_modules() {
    global $pagenow;

    $is_admin = is_admin();
    $action_hook = $is_admin ? 'wp_loaded' : 'wp';
    $required_admin_pages = array('edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php'); // list of admin pages where we need to load builder files
    $specific_filter_pages = array('edit.php', 'admin.php', 'edit-tags.php');
    $is_edit_library_page = 'edit.php' === $pagenow && isset($_GET['post_type']) && 'et_pb_layout' === $_GET['post_type'];
    $is_role_editor_page = 'admin.php' === $pagenow && isset($_GET['page']) && 'et_divi_role_editor' === $_GET['page'];
    $is_import_page = 'admin.php' === $pagenow && isset($_GET['import']) && 'wordpress' === $_GET['import'];
    $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset($_GET['taxonomy']) && 'layout_category' === $_GET['taxonomy'];

    if (!$is_admin || ($is_admin && in_array($pagenow, $required_admin_pages) && (!in_array($pagenow, $specific_filter_pages) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page))) {
        add_action($action_hook, 'ws_divi_custom_modules', 9789);
    }
}
ws_prep_divi_custom_modules();

/**
 * Custom Divi modules
 */
//function ws_divi_custom_modules() {
//    if (class_exists('ET_Builder_Module')) {
//        include('includes/modules.php');
//    }
//}

/**
 * Set styles and javascript
 */
function ws_enqueue_css_js() {
        
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('old-style', get_stylesheet_directory_uri() . '/assets/css/prev_styles.css');
    wp_enqueue_style('animate-style', get_stylesheet_directory_uri() . '/assets/css/animate.min.css', array(), ws('version'));
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/assets/css/styles.css', array(), ws('version'));
        
    wp_enqueue_script('animateModal-js', get_stylesheet_directory_uri() . '/assets/js/animatedModal.min.js', array('jquery'), ws('version'), true);
    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), ws('version'), true);
}
add_action('wp_enqueue_scripts', 'ws_enqueue_css_js', 99);

/**
 * Hide Parent Divi theme
 * 
 * @param array $themes Available themes
 * @return array Available themes
 */
function ws_theme_hide_parent($themes) {
    unset($themes['Divi']);
    return $themes;
}
add_filter('wp_prepare_themes_for_js', 'ws_theme_hide_parent');


/**
 * Output pagination navigation
 * 
 * @param array $args @see paginate_links
 * @param WP_Query $query Optional Query object for pagination. Default to global $wp_query
 */
function ws_page_navi($args = array(), $query = false) {
    
    // use global query if query not set
    if (!$query) {
        global $wp_query;
        $query = $wp_query;
    }
    
    // don't display nav for single page
    if ($query->max_num_pages <= 1) {
        return;
    }
    
    // setup args
    $bignum = 999999999;
    $args = wp_parse_args($args, array(
        'base' => str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum))),
        'format' => '',
        'current' => max(1, get_query_var('paged')),
        'total' => $query->max_num_pages,
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
        'type' => 'list',
        'end_size' => 3,
        'mid_size' => 3
    ));
    
    // output pagination
    echo '<nav class="pagination">';
    echo paginate_links($args);
    echo '</nav>';
}

/**
 * Debug Helpers
 */
if (!function_exists('pr')) {
	
/**
 * Helper function to output data
 * 
 * @param mixed $var Variable to output
 */
function pr($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

}

if (!function_exists('prd')) {

/**
 * Helper function to output data and exit
 * 
 * @param mixed $var Variable to output
 */
function prd($var) {
    pr($var);
    exit;
}

}

/**
 * Custom Divi modules
 */
function ws_divi_custom_modules() {
    if (class_exists('ET_Builder_Module')) {
        include('modules/Blog.php');
    }
}
