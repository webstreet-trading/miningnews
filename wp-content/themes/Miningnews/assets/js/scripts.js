(function ($) {
    $(document).ready(function () {

        $('.simple-tabs .simple-tabs-nav li').click(function () {
            var $parent = $(this).parents('.simple-tabs');
            var tab_content_id = $(this).attr('data-tab');

            $parent.find('.simple-tabs-nav li').removeClass('active');
            $parent.find('.simple-tabs-content').removeClass('active');

            $(this).addClass('active');
            $("#" + tab_content_id).addClass('active');
        });
        
        $('#landing-nav .landing-nav-container li').click(function () {
            
            var $link = $('#landing-nav .landing-nav-container li');
            
            $link.removeClass('active');
            $(this).addClass('active');
            
        });

        $("#imiesa_modal").animatedModal({
            
            modalTarget: "imiesa_modal_content"
            
        });
        
        $("#wasa_modal").animatedModal({
            
            modalTarget: "wasa_modal_content"
            
        });
        
        $("#resource_modal").animatedModal({
            
            modalTarget: "resource_modal_content"
            
        });
                

    });
})(jQuery);