<?php

// run includes
include('ws-divi/functions.php');
include('ws/ws.php');

ws_setting('header-landing-pages');

/*
 * function to add login button
 *
 */
function ws_header_login_btn() {

    $myaccount_url = get_permalink(get_option('woocommerce_myaccount_page_id'));

    echo '<span class="top-login-btn">';
    if (is_user_logged_in()) {
        echo '<a href="' . $myaccount_url . '" class="my_account">My Account</a>';
    } else {
        echo '<a href="' . $myaccount_url . '" class="my_account">Login</a>';
    }
    echo '</span>';
}

/*
 * function to truncate string to specified amount of chars, but keeps words intact.
 *
 * @param string $string The string that needs to be truncated.
 * @parem int $length The amount of chars the string need to be limited to. Default 100.
 * @param string $append. Add to end of the truncated string. Default ' ...'
 *
 * @return string $string Truncated string.
 */
function ws_truncate_str($string, $length = 100, $append = ' ...') {

    $string = trim($string);

    if (strlen($string) > $length) {

        $string = wordwrap($string, $length);
        $string = explode("\n", $string, 2);
        $string = $string[0] . $append;
    }

    return $string;
}

/**
 * Function to modify category page title
 */

function ws_archive_title(){

	if(is_category()){

		$cat_title = get_the_archive_title();

		if(stripos($cat_title, 'category') !== false){

			$cat_title = str_ireplace('category: ', '', $cat_title);
			echo $cat_title;

		}

	} else {

		echo the_archive_title();

	}

}

function ws_add_admin_column($columns) {
    $columns['ID'] = 'ID';
    return $columns;
}
add_filter('manage_et_pb_layout_posts_columns', 'ws_add_admin_column');

function ws_manage_img_column($column_name, $post_id) {
    if( $column_name == 'ID' ) {
        echo $post_id;
    }
    return $column_name;
}
add_filter('manage_posts_custom_column', 'ws_manage_img_column', 10, 2);

/**
 * Ad Code Manager
 */

// set own sizes for google dfp
function ws_acm_ad_tag_ids( $ad_tag_ids = array() ) {
	
	// set add zones to use
	$ad_tag_ids = array(
		array(
			'tag' => '728x90_Leaderboard',
			'url_vars' => array(
				'tag' => '728x90_Leaderboard',
				'sz' => '728x90',
				'width' => '728',
				'height' => '90',
				'sizemapping' => array(
					// always add from biggest to smallest
					array(
						'width' => '728',
						'height' => '90',
					),
					array(
						'width' => '468',
						'height' => '60',
					),
					array(
						'width' => '320',
						'height' => '50',
					),
				),
			),
			'enable_ui_mapping' => true,
		),
		array(
			'tag' => '300x250_MPU',
			'url_vars' => array(
				'tag' => '300x250_MPU',
				'sz' => '300x250',
				'width' => '300',
				'height' => '250',
			),
			'enable_ui_mapping' => true,
		),
		array(
			'tag' => '300x250_MPU_Bottom',
			'url_vars' => array(
				'tag' => '300x250_MPU_Bottom',
				'sz' => '300x250',
				'width' => '300',
				'height' => '250',
			),
			'enable_ui_mapping' => true,
		),
        array(
			'tag' => '300x250_MPU_Bottom_3',
			'url_vars' => array(
				'tag' => '300x250_MPU_Bottom_3',
				'sz' => '300x250',
				'width' => '300',
				'height' => '250',
			),
			'enable_ui_mapping' => true,
		),
        array(
			'tag' => '300x250_MPU_3',
			'url_vars' => array(
				'tag' => '300x250_3',
				'sz' => '300x250',
				'width' => '300',
				'height' => '250',
			),
			'enable_ui_mapping' => true,
		),
        array(
			'tag' => '300x250_MPU_4',
			'url_vars' => array(
				'tag' => '300x250_4',
				'sz' => '300x250',
				'width' => '300',
				'height' => '250',
			),
			'enable_ui_mapping' => true,
		),
		array(
			'tag' => '468x60_Fullbanner',
			'url_vars' => array(
				'tag' => '468x60_Fullbanner',
				'sz' => '468x60',
				'width' => '468',
				'height' => '60',
			),
			'enable_ui_mapping' => true,
		),
        array(
			'tag' => '600x300_BLB',
			'url_vars' => array(
				'tag' => '600x300_BLB',
				'sz' => '600x300',
				'width' => '600',
				'height' => '300',
			),
			'enable_ui_mapping' => true,
		),
	);
	
	return $ad_tag_ids;
}
add_filter( 'acm_ad_tag_ids', 'ws_acm_ad_tag_ids' );

// filter ad output - setup to use responsive ads
function ws_acm_ad_filter_output_html( $output_script, $tag_id ) {
	global $ad_code_manager;

	switch ( $tag_id ) {
		case 'dfp_head':
			$ad_tags = $ad_code_manager->ad_tag_ids;
			ob_start();
?>
	<!-- Include google_services.js -->
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') +
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>
<script type='text/javascript'>
googletag.cmd.push(function() {
<?php
			foreach ( (array) $ad_tags as $key => $tag ):
				if ( $tag['tag'] == 'dfp_head' )
					continue;

			$tt = $tag['url_vars'];
			$matching_ad_code = $ad_code_manager->get_matching_ad_code( $tag['tag'] );
			if ( ! empty( $matching_ad_code ) ) {
				// @todo There might be a case when there are two tags registered with the same dimensions
				// and the same tag id ( which is just a div id ). This confuses DFP Async, so we need to make sure
				// that tags are unique
				
				// check if multiple sizes defined for ad
				$mapping = false;
				if ( isset( $matching_ad_code['url_vars']['sizemapping'] ) ) {
					$sizes = array();
					foreach ( $matching_ad_code['url_vars']['sizemapping'] as $size ) {
						$sizes[] = "[{$size['width']}, {$size['height']}]";
					}
					$ad_size = '[' . implode( ', ', $sizes ) . ']';
					
					// setup mapping
					$mapping = true;
?>
var mapping = googletag.sizeMapping()
	<?php foreach ( $sizes as $size ): ?>
	.addSize(<?php echo $size; ?>, <?php echo $size; ?>)
	<?php endforeach; ?>
	.build();
<?php
				}
				else {
					$ad_size = '[' . (int)$tt['width'] . ', ' . (int)$tt['height'] . ']';
				}
?>
var adslot_<?php echo $key; ?> = googletag.defineSlot('/<?php echo esc_attr( $matching_ad_code['url_vars']['dfp_id'] ); ?>/<?php echo esc_attr( $matching_ad_code['url_vars']['tag_name'] ); ?>', <?php echo $ad_size; ?>, "acm-ad-tag-<?php echo esc_attr( $matching_ad_code['url_vars']['tag_id'] ); ?>")
<?php if ( $mapping ): // if mapping defined ?>
	.defineSizeMapping(mapping)
<?php endif; ?>
	.addService(googletag.pubads());
<?php
			}
			endforeach;
?>
googletag.pubads().enableSingleRequest();
googletag.pubads().collapseEmptyDivs();
googletag.enableServices();
});
</script>
<?php

			$output_script = ob_get_clean();
			break;
		default:
			$output_script = "
	<div id='acm-ad-tag-%tag_id%'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('acm-ad-tag-%tag_id%'); });
</script>
	</div>
			";
			
			// wrap ad html
			$output_script = '<div class="acm-ad-tag">' . $output_script . '</div>';
	}
	return $output_script;

}
add_filter( 'acm_output_html', 'ws_acm_ad_filter_output_html', 11, 2 );

// bug fix function due to bug in ad code manager plugin for shortcode
remove_shortcode( 'acm-tag' );
add_shortcode( 'acm-tag', 'ws_acm_shortcode' );
function ws_acm_shortcode( $atts ) {
	
	$atts = shortcode_atts(
		array(
			'id' => '',
		), $atts );

	$id = sanitize_text_field( $atts['id'] );
	if ( empty( $id ) )
		return;

	ob_start();
	do_action( 'acm_tag', $atts['id'] );
	return ob_get_clean();
}