<?php
/**
 * WS Generic email wrapper
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/ws_email.php.
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

do_action('woocommerce_email_header', $email_heading);

echo $content;

do_action('woocommerce_email_footer');