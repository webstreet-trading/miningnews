<?php
/**
 * WS Generic plain email wrapper
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/plain/ws_email.php.
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if ($email_heading) {
    echo "= " . $email_heading . " =\n\n";
}

echo $plain_content . "\n\n";

echo "----------\n\n";

echo apply_filters('woocommerce_email_footer_text', get_option('woocommerce_email_footer_text'));
